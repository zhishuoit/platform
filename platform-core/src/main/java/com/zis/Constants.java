package com.zis;

/**
 * 系统配置相关常量
 * 
 * @author 赵海涛
 * 
 *         关键字类 使用 KEY_ 系统类 常量 使用 SYS_ 模板类 使用 TEMPLATE_
 * 
 * 
 */
public class Constants
{
    
    /**
     * 密码错误次数 cookies名称
     */
    public static final String COOKIE_ERROR_REMAINING = "_login_error_times_";
    
    /**
     * 密码错误次数
     */
    public static final int COOKIE_MAX_ERROR_TIMES = 20;
    
    /**
     * 错误信息名称
     */
    public static final String ERROR = "error";
    
    /**
     * 登录名称 标示
     */
    public static final String LOGIN_NAME = "loginName";
    
    /**
     * 部署路径调用名称 变量
     */
    public static final String CONTEXT_PATH = "base";
    
    /**
     * 应用系统资源路径 变量
     */
    public static final String RES_PATH = "res";
    
    /**
     * 当前应用系统信息 变量
     */
    public static final String SYS_INFO = "sysInfo";
    
    // 资源类型 - 菜单
    public static final String RES_TYPE_MENU = "MENU";
    
    // 资源类型 - 资源
    public static final String RES_TYPE_RES = "RES";
    
    public static final String SYS_CACHE_KEY = "_sys_config_key_";
    
    /**
     * 验证码保存关键字
     */
    public static final String KEY_CODE_IMAGE = "_code_image_";
    
    /**
     * 平台模板 文件存放目录
     */
    public static final String TEMPLATE_FOLDEER_PLATFORM = "page";
    
    /**
     * WEB网站模板 文件存放目录
     */
    public static final String TEMPLATE_FOLDEER_FRONT = "template";
    
    /**
     * WEB-INF
     */
    public static final String WEB_INF = "WEB-INF";
    
    /**
     * 模板页面类型后缀
     */
    public static final String WEB_SUFFIX = ".html";
    
    /**
     * 默认的分页参数：每页显示数量
     */
    public static final Integer PAGE_SIZE = 10;
    
    /**
     * 新增用户的默认密码
     */
    public static final String NEW_USER_DEFAULT_PASSWORD = "123456";
}

package com.zis.platform.common.service;

import java.util.List;

import com.zis.common.Pagination;
import com.zis.common.SearchParams;

/**
 * 
 * <b>说明：</b>公用服务类接口<br/>
 * <b><font color="#FF0000">建议所有业务接口都继承</font></b>
 * 
 * @ClassName: BaseService
 * @author zhaohaitao(2543)
 * @date 2015-7-10 上午10:14:38
 * 
 * @param <T>
 */
public interface BaseService<T>
{
    public Pagination<T> findPageByCondition(SearchParams params)
        throws Exception;
    
    public List<T> findByCondition(SearchParams params)
        throws Exception;
    
    public List<T> findAll()
        throws Exception;
}

package com.zis.platform.common.authentication.freemarker;

import java.io.IOException;

import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.jagregory.shiro.freemarker.ShiroTags;

import freemarker.template.TemplateException;

/**
 * 
 * <b>说明：</b> freemarker 配置的扩展 ；<br />
 * 加入 shiro的界面资源权限控制<br/>
 * 开发人员可以在页面使用freemarker标签对页面元素进行权限控制<br/>
 * eg: <@shiro.hasRole name=”admin”>Hello admin!</@shiro.hasRole>
 * 
 * @ClassName: ShiroFreeMarkerConfigurer
 * @author zhaohaitao(2543)
 * @date 2015-7-9 下午6:13:50
 * 
 */
public class ShiroFreeMarkerConfigurer extends FreeMarkerConfigurer
{
    @Override
    public void afterPropertiesSet()
        throws IOException, TemplateException
    {
        super.afterPropertiesSet();
        this.getConfiguration().setSharedVariable("shiro", new ShiroTags());
    }
}

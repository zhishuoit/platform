package com.zis.platform.common.authentication;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.web.filter.authc.LogoutFilter;

import com.zis.platform.common.util.CacheCoreUtil;
import com.zis.platform.core.entity.SysEntity;

/**
 * <b>说明：</b>注销拦截器<br>
 * 当用户点击注销，由该拦截器来处理注销业务
 * 
 * @ClassName: CoreLogoutFilter
 * @author zhaohaitao(2543)
 * @date 2015-7-10 上午10:05:43
 * 
 */
public class CoreLogoutFilter extends LogoutFilter
{
    
    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response)
        throws Exception
    {
        SysEntity sysInfo = CacheCoreUtil.getSysInfo((HttpServletRequest)request);
        String loginUrl = sysInfo.getLoginUrl();
        super.setRedirectUrl(loginUrl);
        return super.preHandle(request, response);
    }
    
}

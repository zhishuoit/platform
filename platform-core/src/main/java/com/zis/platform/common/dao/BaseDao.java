package com.zis.platform.common.dao;

import java.io.Serializable;

/**
 * 
 * <b>说明：</b>基础dao接口，将一些通用的数据持久方法抽象到接口 <br>
 * <font color="#FF0000"><b>建议：业务开发过程所有的dao都实现该接口</b></font>
 * 
 * @ClassName: BaseDao
 * @author zhaohaitao(2543)
 * @date 2015-7-10 上午10:10:29
 * 
 * @param <T>
 * @param <PK>
 */
public interface BaseDao<T, PK extends Serializable>
{
    int deleteByPrimaryKey(PK id);
    
    int insert(T record);
    
    int insertSelective(T record);
    
    T selectByPrimaryKey(PK id);
    
    int updateByPrimaryKeySelective(T record);
    
    int updateByPrimaryKey(T record);
}

package com.zis.platform.common.log;

import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zis.Constants;
import com.zis.platform.core.entity.LogLoginEntity;
import com.zis.util.RemoteUtil;

/**
 * 
 * <b>说明：</b>登录日志拦截器
 * 
 * @ClassName: LoginLogAspect
 * @author zhaohaitao(2543)
 * @date 2015-7-10 上午10:11:11
 * 
 */
public class LoginLogAspect
{
    private ObjectMapper objMapper = new ObjectMapper();
    
    private Logger logger = Logger.getLogger(LoginLogAspect.class);
    
    /**
     * 
     * 异常通知：当拦截到业务抛出异常的处理方法
     * 
     * @Title: doAfterThrowing
     * @param @param joinPoint
     * @param @param e 设定文件
     * @return void 返回类型
     * @throws
     */
    public void doAfterThrowing(JoinPoint joinPoint, Exception e)
    {
        ServletRequestAttributes attrs = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        if (attrs != null)
        {
            LogLoginEntity log = new LogLoginEntity();
            HttpServletRequest request = attrs.getRequest();
            String msg = (String)request.getAttribute(Constants.ERROR);
            log.setLogId(UUID.randomUUID().toString());
            log.setLoginDate(new Date());
            log.setRemark(msg);
            log.setLoginIp(RemoteUtil.getIpAddr(request));
            log.setLoginMac(RemoteUtil.getMACAddress(request));
            log.setException(e.toString());
            String loginName = (String)request.getAttribute(Constants.LOGIN_NAME);
            log.setLoginName(loginName);
            try
            {
                String logStr = objMapper.writeValueAsString(log);
                logger.warn(logStr);
            }
            catch (JsonProcessingException e1)
            {
                // e1.printStackTrace();
            }
        }
    }
    
    /**
     * 后置通知 ：在业务拦截之后的处理。
     * 
     * @Title: doAfter
     * @param @param joinPoint 设定文件
     * @return void 返回类型
     * @throws
     */
    public void doAfter(JoinPoint joinPoint)
    {
        ServletRequestAttributes attrs = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        if (attrs != null)
        {
            LogLoginEntity log = new LogLoginEntity();
            HttpServletRequest request = attrs.getRequest();
            String msg = (String)request.getAttribute(Constants.ERROR);
            String loginName = (String)request.getAttribute(Constants.LOGIN_NAME);
            log.setLogId(UUID.randomUUID().toString());
            log.setLoginDate(new Date());
            log.setRemark(msg);
            log.setLoginIp(RemoteUtil.getIpAddr(request));
            log.setLoginMac(RemoteUtil.getMACAddress(request));
            log.setLoginName(loginName);
            
            String logStr;
            try
            {
                logStr = objMapper.writeValueAsString(log);
                logger.warn(logStr);
            }
            catch (JsonProcessingException e)
            {
                // e.printStackTrace();
            }
        }
    }
    
}

package com.zis.platform.common.util;

import java.io.File;
import java.util.Calendar;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 * 附件上传
 * 
 */
public class UploadUtil
{
    
    public static String doUpload(String realPath, String modulePath, MultipartFile file)
    {
        String fileName = file.getOriginalFilename();
        if (!StringUtils.isEmpty(fileName))
        {
            String ext = fileName.substring(fileName.lastIndexOf("."));
            fileName = Calendar.getInstance().getTimeInMillis() + ext;
            
            File targetFile = new File(realPath + modulePath + fileName);
            if (!targetFile.exists())
            {
                targetFile.mkdirs();
            }
            // 保存
            try
            {
                file.transferTo(targetFile);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return modulePath + fileName;
        }
        return null;
    }
}

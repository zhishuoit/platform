package com.zis.platform.common.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zis.common.Pagination;
import com.zis.common.SearchParams;
import com.zis.platform.common.service.BaseService;

/**
 * 
 * <b>说明：</b>公用服务类抽象层 主要抽象 分页方法 以达到多数据库的通用性<br/>
 * <b><font color="#FF0000">建议所有业务类都继承</font></b>
 * 
 * @ClassName: BaseServiceImpl
 * @author zhaohaitao(2543)
 * @date 2015-7-10 上午10:14:50
 * 
 * @param <T>
 */
public abstract class BaseServiceImpl<T> implements BaseService<T>
{
    /**
     * 根据传入的 查询分页 参数进行分页查询
     * 
     * @param params
     * @return
     * @throws Exception
     */
    public Pagination<T> findPageByCondition(SearchParams params)
        throws Exception
    {
        if (params == null)
        {
            throw new Exception("com.zis.common.SearchParams 查询分页参数不能为空!");
        }
        Page<T> page = PageHelper.startPage(params.getPage(), params.getRows());
        if (params.getSearchParams() != null)
        {
            findByCondition(params);
        }
        else
        {
            findAll();
        }
        Pagination<T> pi = new Pagination<T>(page);
        return pi;
    }
    
}

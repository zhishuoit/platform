package com.zis.platform.common.web;

import java.io.Serializable;
import java.util.List;

/**
 * Tree Node
 * 
 * @author wangjun
 * 
 */
public class TreeNode implements Serializable
{
    public TreeNode(String id, String text, String parentId)
    {
        super();
        this.id = id;
        this.text = text;
        this.parentId = parentId;
    }
    
    public TreeNode()
    {
    }
    
    private String id;
    
    private String text;
    
    private String state;
    
    private String parentId;
    
    private List<TreeNode> children;
    
    public String getId()
    {
        return id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getText()
    {
        return text;
    }
    
    public void setText(String text)
    {
        this.text = text;
    }
    
    public String getState()
    {
        return state;
    }
    
    public void setState(String state)
    {
        this.state = state;
    }
    
    public List<TreeNode> getChildren()
    {
        return children;
    }
    
    public void setChildren(List<TreeNode> children)
    {
        this.children = children;
    }
    
    public String getParentId()
    {
        return parentId;
    }
    
    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }
    
}

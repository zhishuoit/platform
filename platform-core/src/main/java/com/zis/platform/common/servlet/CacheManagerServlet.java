package com.zis.platform.common.servlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.zis.platform.common.util.CacheCoreUtil;
import com.zis.platform.core.service.DataAuthStrategyService;
import com.zis.platform.core.service.DictionaryTypeService;
import com.zis.platform.core.service.SysService;

/**
 * 
 * <b>说明：</b>统一资源管理平台系统服务接口，在系统启动的时候做一些数据准备：系统数据、字典、数据策略等等放到缓存
 * 
 * @ClassName: CacheManagerServlet
 * @author zhaohaitao(2543)
 * @date 2015-7-10 上午10:16:25
 * 
 */
public class CacheManagerServlet extends HttpServlet
{
    private static final long serialVersionUID = -1055169080504850924L;
    
    protected Logger log = LoggerFactory.getLogger(CacheManagerServlet.class);
    
    public void init()
        throws ServletException
    {
        try
        {
            CacheCoreUtil.cacheSystemStartState(false);
            ServletContext servletContext = this.getServletContext();
            WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(servletContext);
            SysService sysService = (SysService)ctx.getBean("sysService");
            DictionaryTypeService dictionaryTypeService = (DictionaryTypeService)ctx.getBean("dictionaryTypeService");
            DataAuthStrategyService dataAuthStrategyService =
                (DataAuthStrategyService)ctx.getBean("dataAuthStrategyService");
            log.info("---------------------应用系统数据写入缓存   开始------------------------");
            long startNanoTime = System.currentTimeMillis();
            CacheCoreUtil.cacheSysInfo(sysService.findAll());
            long endNanoTime = System.currentTimeMillis();
            log.info("---------应用系统数据写入缓存结束     耗时：" + (endNanoTime - startNanoTime) + "ms--------");
            
            log.info("---------------------字典数据写入缓存开始------------------------");
            startNanoTime = System.currentTimeMillis();
            CacheCoreUtil.cacheDictionary(dictionaryTypeService.findAllTypeAndDics());
            endNanoTime = System.currentTimeMillis();
            log.info("---------字典数据写入缓存结束     耗时：" + (endNanoTime - startNanoTime) + "ms--------");
            
            log.info("---------------------数据权限策略  写入缓存开始------------------------");
            startNanoTime = System.currentTimeMillis();
            CacheCoreUtil.cacheDataAuthStrategy(dataAuthStrategyService.findAll());
            endNanoTime = System.currentTimeMillis();
            log.info("---------数据权限策略   写入缓存结束     耗时：" + (endNanoTime - startNanoTime) + "ms--------");
            CacheCoreUtil.cacheSystemStartState(true);
        }
        catch (Exception e)
        {
            log.error("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 记载系统缓存失败  $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            e.printStackTrace();
        }
    }
}

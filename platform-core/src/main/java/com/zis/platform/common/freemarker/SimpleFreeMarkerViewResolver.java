package com.zis.platform.common.freemarker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.AbstractTemplateViewResolver;
import org.springframework.web.servlet.view.AbstractUrlBasedView;

/**
 * 
 * <b>说明：</b>暂时未对试图解释器做扩展，留着以备后期扩展
 * 
 * @ClassName: SimpleFreeMarkerViewResolver
 * @author zhaohaitao(2543)
 * @date 2015-7-10 上午10:10:58
 * 
 */
public class SimpleFreeMarkerViewResolver extends AbstractTemplateViewResolver
{
    private Logger log = LoggerFactory.getLogger(SimpleFreeMarkerViewResolver.class);
    
    public SimpleFreeMarkerViewResolver()
    {
        setViewClass(SimpleFreeMarkerView.class);
    }
    
    /**
     * if viewName start with / , then ignore prefix.
     */
    @Override
    protected AbstractUrlBasedView buildView(String viewName)
        throws Exception
    {
        AbstractUrlBasedView view = super.buildView(viewName);
        // start with / ignore prefix
        /*
         * if (viewName.startsWith("/")) { view.setUrl(viewName + getSuffix()); }
         */
        return view;
    }
}

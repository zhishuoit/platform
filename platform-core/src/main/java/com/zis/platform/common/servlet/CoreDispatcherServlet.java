package com.zis.platform.common.servlet;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.View;

import com.zis.platform.common.util.CacheCoreUtil;
import com.zis.platform.core.entity.SysEntity;

/**
 * 
 * <b>说明：</b>springmvc核心控制器的扩展
 * 
 * @ClassName: CoreDispatcherServlet
 * @author zhaohaitao(2543)
 * @date 2015-7-10 上午10:17:47
 * 
 */
public class CoreDispatcherServlet extends DispatcherServlet
{
    /**
     * 不同的应用系统跳转到不同的视图模板路径
     */
    @Override
    protected View resolveViewName(String viewName, Map<String, Object> model, Locale locale, HttpServletRequest request)
        throws Exception
    {
        SysEntity sysInfo = CacheCoreUtil.getSysInfo(request);
        if (sysInfo != null && !viewName.startsWith("redirect:") && !viewName.startsWith("forward:"))
        {
            viewName = sysInfo.getTemplateFloder() + "/" + viewName;
        }
        return super.resolveViewName(viewName, model, locale, request);
    }
    
    @Override
    protected void doDispatch(HttpServletRequest request, HttpServletResponse response)
        throws Exception
    {
        super.doDispatch(request, response);
    }
}

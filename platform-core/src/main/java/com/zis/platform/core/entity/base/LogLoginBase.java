package com.zis.platform.core.entity.base;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 登录日志
 * 
 * @author zhaohaitao
 * 
 */
public class LogLoginBase
{
    private String logId;
    
    private String loginName;
    
    private String loginIp;
    
    private String loginMac;
    
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date loginDate;
    
    private String remark;
    
    private String exception;
    
    public LogLoginBase()
    {
        super();
    }
    
    public String getLogId()
    {
        return logId;
    }
    
    public void setLogId(String logId)
    {
        this.logId = logId;
    }
    
    public String getLoginName()
    {
        return loginName;
    }
    
    public void setLoginName(String loginName)
    {
        this.loginName = loginName;
    }
    
    public String getLoginIp()
    {
        return loginIp;
    }
    
    public void setLoginIp(String loginIp)
    {
        this.loginIp = loginIp;
    }
    
    public String getLoginMac()
    {
        return loginMac;
    }
    
    public void setLoginMac(String loginMac)
    {
        this.loginMac = loginMac;
    }
    
    public Date getLoginDate()
    {
        return loginDate;
    }
    
    public void setLoginDate(Date loginDate)
    {
        this.loginDate = loginDate;
    }
    
    public String getRemark()
    {
        return remark;
    }
    
    public void setRemark(String remark)
    {
        this.remark = remark;
    }
    
    public String getException()
    {
        return exception;
    }
    
    public void setException(String exception)
    {
        this.exception = exception;
    }
    
    @Override
    public String toString()
    {
        return "loginName=" + loginName + ", loginIp=" + loginIp + ", loginMac=" + loginMac + ", loginDate="
            + loginDate + ", remark=" + remark + ", exception=" + exception;
    }
    
}

package com.zis.platform.core.service;

import java.io.File;
import java.util.List;

import com.zis.platform.common.service.BaseService;
import com.zis.platform.core.entity.ExcelTemplateEntity;

public interface ExcelTemplateService extends BaseService<ExcelTemplateEntity>
{
    
    ExcelTemplateEntity saveOrUpdate(ExcelTemplateEntity entity);
    
    ExcelTemplateEntity getById(String etId);
    
    int deleteByEtIds(String[] etIds);
    
    ExcelTemplateEntity getByCode(String code);
    
    /**
     * 通过模板code获取模板文件
     * 
     * @param code
     * @return
     */
    File getExceltemplateFileByCode(String code);
    
    List<ExcelTemplateEntity> findByEtName(String etName);
    
    List<ExcelTemplateEntity> findByEtCode(String etCode);
}

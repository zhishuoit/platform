package com.zis.platform.core.service;

import java.util.List;
import java.util.Map;

import com.zis.platform.common.service.BaseService;
import com.zis.platform.core.entity.DataAuthRoleEntity;
import com.zis.platform.core.entity.DataAuthUserEntity;
import com.zis.platform.core.entity.UserEntity;

public interface DataAuthRoleService extends BaseService<DataAuthRoleEntity>
{
    
    boolean saveOrUpdate(DataAuthRoleEntity entity);
    
    int deleteByRoleDataAuthIds(String[] roleDataAuthIds);
    
    public Map<String, List<DataAuthUserEntity>> findByRoleId(String roleId, UserEntity user);
}

package com.zis.platform.core.entity;

import com.zis.platform.core.entity.base.DataAuthRoleBase;

public class DataAuthRoleEntity extends DataAuthRoleBase
{
    private String roleName;
    
    public String getRoleName()
    {
        return roleName;
    }
    
    public void setRoleName(String roleName)
    {
        this.roleName = roleName;
    }
    
}

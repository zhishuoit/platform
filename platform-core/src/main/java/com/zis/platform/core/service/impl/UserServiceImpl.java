package com.zis.platform.core.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zis.common.SearchParams;
import com.zis.platform.common.service.impl.BaseServiceImpl;
import com.zis.platform.core.dao.RoleDao;
import com.zis.platform.core.dao.UserDao;
import com.zis.platform.core.entity.RoleEntity;
import com.zis.platform.core.entity.UserEntity;
import com.zis.platform.core.entity.UserRoleEntity;
import com.zis.platform.core.service.UserService;
import com.zis.util.MD5Salt;
import com.zis.util.Salt;
import com.zis.util.StringUtil;

@Service
public class UserServiceImpl extends BaseServiceImpl<UserEntity> implements UserService
{
    @Autowired
    private UserDao userDao;
    
    @Autowired
    private RoleDao roleDao;
    
    @Override
    public UserEntity findByLoginName(String loginName)
    {
        if (StringUtils.isBlank(loginName))
        {
            return null;
        }
        return userDao.findByLoginName(loginName);
    }
    
    @Override
    public List<UserEntity> findByCondition(SearchParams params)
    {
        return userDao.findByCondition(params);
    }
    
    @Override
    public List<UserEntity> findAll()
    {
        return userDao.findAll();
    }
    
    @Override
    public UserEntity saveOrUpdate(UserEntity user)
    {
        if (user != null)
        {
            if (StringUtils.isBlank(user.getUserId()))
            {
                user.setUserId(UUID.randomUUID().toString());
                user.setSalt(Salt.genRandomL8());
                user.setPassword(MD5Salt.enCode(com.zis.Constants.NEW_USER_DEFAULT_PASSWORD, user.getSalt()));
                user.setLocked(false);
                int insertNum = userDao.insertSelective(user);
                if (insertNum > 0)
                {
                    return user;
                }
            }
            else
            {
                int updateNum = userDao.updateByPrimaryKeySelective(user);
                if (updateNum > 0)
                {
                    return user;
                }
            }
        }
        return null;
    }
    
    @Override
    public int deleteByUserIds(String[] userIds)
    {
        return this.userDao.deleteByUserIds(userIds);
    }
    
    @Override
    public UserEntity findById(String userId)
    {
        return this.userDao.selectByPrimaryKey(userId);
    }
    
    @Override
    public List<RoleEntity> findRolesByUserId(String userId)
    {
        if (StringUtils.isNotEmpty(userId))
        {
            List<RoleEntity> roles = roleDao.findRolesByUserId(userId);
            return roles;
        }
        return null;
    }
    
    @Override
    public boolean saveUserRoles(UserEntity user)
    {
        if (user != null && StringUtil.isNotEmpty(user.getUserId()))
        {
            this.userDao.deleteRolesByUserId(user.getUserId());
            List<RoleEntity> list = user.getRoles();
            if (list != null && list.size() > 0)
            {
                List<UserRoleEntity> dbList = new ArrayList<UserRoleEntity>();
                for (RoleEntity role : list)
                {
                    UserRoleEntity ur = new UserRoleEntity();
                    ur.setRuId(UUID.randomUUID().toString());
                    ur.setUserId(user.getUserId());
                    ur.setRoleId(role.getRoleId());
                    dbList.add(ur);
                }
                this.userDao.batchSaveUserRole(dbList);
            }
            return true;
        }
        return false;
    }
    
    @Override
    public int enableByUserIds(String[] userIds)
    {
        return this.userDao.enableByUserIds(userIds);
    }
    
    @Override
    public int disableByUserIds(String[] userIds)
    {
        return this.userDao.disableByUserIds(userIds);
    }
    
    @Override
    public void lockUser(String userId, boolean locked)
    {
        UserEntity user = new UserEntity(userId);
        user.setLocked(locked);
        userDao.lockUserByLoginName(user);
    }
    
    @Override
    public void updateUserLoginInfo(String userId, String ip, String mac)
    {
        UserEntity userTemp = userDao.selectByPrimaryKey(userId);
        UserEntity user = new UserEntity();
        user.setUserId(userTemp.getUserId());
        user.setLastLoginIp(userTemp.getNowLoginIp());
        user.setLastLoginMac(userTemp.getNowLoginMac());
        user.setLastLoginDate(userTemp.getNowLoginDate());
        user.setNowLoginDate(new Date());
        user.setNowLoginIp(ip);
        user.setNowLoginMac(mac);
        userDao.updateByPrimaryKeySelective(user);
    }
    
    @Override
    public int unLockByUserIds(String[] userIds)
    {
        return this.userDao.unLockByUserIds(userIds);
    }
    
    @Override
    public int resetPasswordByUserIds(String[] userIds)
    {
        
        if (userIds != null && userIds.length > 0)
        {
            for (String userId : userIds)
            {
                UserEntity user = userDao.selectByPrimaryKey(userId);
                user.setSalt(Salt.genRandomL8());
                user.setPassword(MD5Salt.enCode(com.zis.Constants.NEW_USER_DEFAULT_PASSWORD, user.getSalt()));
                userDao.updateByPrimaryKeySelective(user);
            }
            return userIds.length;
        }
        return 0;
    }
    
}

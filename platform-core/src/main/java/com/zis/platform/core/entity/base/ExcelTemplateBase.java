package com.zis.platform.core.entity.base;

import java.io.Serializable;

public class ExcelTemplateBase implements Serializable
{
    private String etId;
    
    private String etName;
    
    private String etPath;
    
    private String etRemarks;
    
    private String etCode;
    
    public String getEtId()
    {
        return etId;
    }
    
    public void setEtId(String etId)
    {
        this.etId = etId;
    }
    
    public String getEtName()
    {
        return etName;
    }
    
    public void setEtName(String etName)
    {
        this.etName = etName;
    }
    
    public String getEtPath()
    {
        return etPath;
    }
    
    public void setEtPath(String etPath)
    {
        this.etPath = etPath;
    }
    
    public String getEtRemarks()
    {
        return etRemarks;
    }
    
    public void setEtRemarks(String etRemarks)
    {
        this.etRemarks = etRemarks;
    }
    
    public String getEtCode()
    {
        return etCode;
    }
    
    public void setEtCode(String etCode)
    {
        this.etCode = etCode;
    }
    
}

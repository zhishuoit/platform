package com.zis.platform.core.entity.base;

import java.io.Serializable;

public class UserOrgBase implements Serializable
{
    private String uoId;
    
    private String userId;
    
    private String orgId;
    
    public String getUoId()
    {
        return uoId;
    }
    
    public void setUoId(String uoId)
    {
        this.uoId = uoId;
    }
    
    public String getUserId()
    {
        return userId;
    }
    
    public void setUserId(String userId)
    {
        this.userId = userId;
    }
    
    public String getOrgId()
    {
        return orgId;
    }
    
    public void setOrgId(String orgId)
    {
        this.orgId = orgId;
    }
    
}

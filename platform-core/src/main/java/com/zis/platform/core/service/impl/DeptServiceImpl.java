package com.zis.platform.core.service.impl;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zis.common.SearchParams;
import com.zis.platform.common.service.impl.BaseServiceImpl;
import com.zis.platform.core.dao.DeptDao;
import com.zis.platform.core.entity.DeptEntity;
import com.zis.platform.core.service.DeptService;

@Service
public class DeptServiceImpl extends BaseServiceImpl<DeptEntity> implements DeptService
{
    
    @Autowired
    DeptDao deptDao;
    
    @Override
    public List<DeptEntity> findByCondition(SearchParams params)
        throws Exception
    {
        return this.deptDao.findByCondition(params);
    }
    
    @Override
    public List<DeptEntity> findAll()
        throws Exception
    {
        return this.deptDao.findAll();
    }
    
    @Override
    public DeptEntity saveOrUpdate(DeptEntity dept)
    {
        if (dept != null)
        {
            if (StringUtils.isBlank(dept.getDeptId()))
            {
                dept.setDeptId(UUID.randomUUID().toString());
                int insertNum = deptDao.insertSelective(dept);
                if (insertNum > 0)
                {
                    return dept;
                }
            }
            else
            {
                int updateNum = deptDao.updateByPrimaryKeySelective(dept);
                if (updateNum > 0)
                {
                    return dept;
                }
            }
        }
        return null;
    }
    
    @Override
    public int deleteByDeptIds(String[] deptIds)
    {
        return deptDao.deleteByDeptIds(deptIds);
    }
    
}

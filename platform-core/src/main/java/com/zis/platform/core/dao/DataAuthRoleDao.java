package com.zis.platform.core.dao;

import java.util.List;

import com.zis.common.SearchParams;
import com.zis.platform.core.entity.DataAuthRoleEntity;

public interface DataAuthRoleDao
{
    
    List<DataAuthRoleEntity> findByCondition(SearchParams params);
    
    List<DataAuthRoleEntity> findAll();
    
    int insert(DataAuthRoleEntity entity);
    
    int updateByPrimaryKeySelective(DataAuthRoleEntity entity);
    
    int deleteByRoleDataAuthIds(String[] roleDataAuthIds);
    
    public List<DataAuthRoleEntity> findByRoleId(String roleId);
    
    int isExist(DataAuthRoleEntity entity);
}

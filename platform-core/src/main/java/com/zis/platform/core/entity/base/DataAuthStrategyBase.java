package com.zis.platform.core.entity.base;

import java.io.Serializable;

public class DataAuthStrategyBase implements Serializable
{
    private static final long serialVersionUID = 8925511115862280846L;
    
    private String strategyId;
    
    private String strategyName;
    
    private String tableName;
    
    private String columnName;
    
    private String comparisonOperator;
    
    private String logicalRelation;
    
    private String matchingRule;
    
    public String getStrategyId()
    {
        return strategyId;
    }
    
    public void setStrategyId(String strategyId)
    {
        this.strategyId = strategyId == null ? null : strategyId.trim();
    }
    
    public String getStrategyName()
    {
        return strategyName;
    }
    
    public void setStrategyName(String strategyName)
    {
        this.strategyName = strategyName == null ? null : strategyName.trim();
    }
    
    public String getTableName()
    {
        return tableName;
    }
    
    public void setTableName(String tableName)
    {
        this.tableName = tableName == null ? null : tableName.trim();
    }
    
    public String getColumnName()
    {
        return columnName;
    }
    
    public void setColumnName(String columnName)
    {
        this.columnName = columnName == null ? null : columnName.trim();
    }
    
    public String getComparisonOperator()
    {
        return comparisonOperator;
    }
    
    public void setComparisonOperator(String comparisonOperator)
    {
        this.comparisonOperator = comparisonOperator == null ? null : comparisonOperator.trim();
    }
    
    public String getLogicalRelation()
    {
        return logicalRelation;
    }
    
    public void setLogicalRelation(String logicalRelation)
    {
        this.logicalRelation = logicalRelation == null ? null : logicalRelation.trim();
    }
    
    public String getMatchingRule()
    {
        return matchingRule;
    }
    
    public void setMatchingRule(String matchingRule)
    {
        this.matchingRule = matchingRule == null ? null : matchingRule.trim();
    }
}
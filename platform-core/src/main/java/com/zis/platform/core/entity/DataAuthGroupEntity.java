package com.zis.platform.core.entity;

import com.zis.platform.core.entity.base.DataAuthGroupBase;

public class DataAuthGroupEntity extends DataAuthGroupBase
{
    private String groupName;
    
    public String getGroupName()
    {
        return groupName;
    }
    
    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }
    
}

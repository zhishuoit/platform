package com.zis.platform.core.service;

import java.util.List;

import com.zis.platform.common.service.BaseService;
import com.zis.platform.core.entity.DictionaryEntity;

public interface DictionaryService extends BaseService<DictionaryEntity>
{
    public int delete(String[] dictCode);
    
    public DictionaryEntity save(DictionaryEntity dic);
    
    public List<DictionaryEntity> findByTypeCode(String typeCode);
    
    public List<DictionaryEntity> findByDicValue(String dicValue);
}

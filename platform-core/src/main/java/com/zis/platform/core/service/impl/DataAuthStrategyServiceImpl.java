package com.zis.platform.core.service.impl;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zis.common.SearchParams;
import com.zis.platform.common.service.impl.BaseServiceImpl;
import com.zis.platform.core.dao.DataAuthStrategyDao;
import com.zis.platform.core.entity.DataAuthStrategyEntity;
import com.zis.platform.core.service.DataAuthStrategyService;

@Service("dataAuthStrategyService")
public class DataAuthStrategyServiceImpl extends BaseServiceImpl<DataAuthStrategyEntity> implements
    DataAuthStrategyService
{
    @Autowired
    private DataAuthStrategyDao dao;
    
    @Override
    public List<DataAuthStrategyEntity> findByCondition(SearchParams params)
        throws Exception
    {
        return dao.findByCondition(params);
    }
    
    @Override
    public List<DataAuthStrategyEntity> findAll()
        throws Exception
    {
        return dao.findAll();
    }
    
    @Override
    public boolean saveOrUpdate(DataAuthStrategyEntity entity)
    {
        int num = 0;
        if (StringUtils.isNotEmpty(entity.getStrategyId()))
        {
            num = dao.updateByPrimaryKeySelective(entity);
        }
        else
        {
            entity.setStrategyId(UUID.randomUUID().toString());
            num = dao.insert(entity);
        }
        if (num > 0)
        {
            return true;
        }
        return false;
    }
    
    @Override
    public int deleteByStrategyIds(String[] strategyIds)
    {
        return this.dao.deleteByStrategyIds(strategyIds);
    }
}

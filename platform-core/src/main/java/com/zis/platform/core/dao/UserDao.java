package com.zis.platform.core.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.zis.common.SearchParams;
import com.zis.platform.common.dao.BaseDao;
import com.zis.platform.core.entity.UserEntity;
import com.zis.platform.core.entity.UserRoleEntity;

@Repository
public interface UserDao extends BaseDao<UserEntity, String>
{
    public UserEntity findByLoginName(String loginName);
    
    public List<UserEntity> findByCondition(SearchParams params);
    
    public List<UserEntity> findAll();
    
    public int deleteByUserIds(String[] userIds);
    
    public void deleteRolesByUserId(String userId);
    
    public void batchSaveUserRole(List<UserRoleEntity> dbList);
    
    public int enableByUserIds(String[] userIds);
    
    public int disableByUserIds(String[] userIds);
    
    public void lockUserByLoginName(UserEntity user);
    
    public void updateUserLoginInfo(String userId, String ip, String mac);
    
    public int unLockByUserIds(String[] userIds);
    
}
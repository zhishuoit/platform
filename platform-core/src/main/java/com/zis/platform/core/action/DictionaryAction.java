package com.zis.platform.core.action;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zis.common.Pagination;
import com.zis.common.SearchParams;
import com.zis.platform.common.util.CacheCoreUtil;
import com.zis.platform.core.entity.DictionaryEntity;
import com.zis.platform.core.entity.DictionaryTypeEntity;
import com.zis.platform.core.service.DictionaryService;
import com.zis.platform.core.service.DictionaryTypeService;

@Controller
@RequestMapping(value = "/platform/dictionary/")
public class DictionaryAction
{
    @Autowired
    private DictionaryService service;
    
    @Autowired
    private DictionaryTypeService typeService;
    
    @ResponseBody
    @RequestMapping(value = "delete_dic.json")
    public Integer deleteDic(@RequestBody
    String[] dictCodes, ModelMap model)
    {
        return service.delete(dictCodes);
    }
    
    /**
     * 字典数据列表页面
     * 
     * @return
     */
    @RequestMapping(value = "edit.do")
    public String edit(String typeCode, ModelMap model)
    {
        DictionaryTypeEntity typeEntity = typeService.findById(typeCode);
        model.addAttribute("type", typeEntity);
        return "dictionary/edit";
    }
    
    /**
     * 公共接口;获取某类型下的所有字典数据
     * 
     * @param typeId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "show_dic_by_type.json")
    public List<DictionaryEntity> getDicsByDicType(String typeId, HttpServletRequest request)
    {
        if ("ICONS".equals(typeId))
        {
            String realPath = request.getSession().getServletContext().getRealPath("plug-in/icons/tabicons");
            return getFileList(realPath);
        }
        // return service.findByTypeCode(typeId);
        DictionaryTypeEntity dicType = CacheCoreUtil.getDictionaryType(typeId);
        if (dicType != null)
        {
            return dicType.getDics();
        }
        return null;
    }
    
    /**
     * 字典维护首页
     * 
     * @return
     */
    @RequestMapping(value = "list.do")
    public String list()
    {
        return "dictionary/list";
    }
    
    @ResponseBody
    @RequestMapping(value = "list_dic.json")
    public Pagination<DictionaryEntity> list(SearchParams params)
    {
        try
        {
            Pagination<DictionaryEntity> pageList = service.findPageByCondition(params);
            return pageList;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * 获取字典类型数据
     * 
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "list_dic_type.json")
    public List<DictionaryTypeEntity> listDicType()
    {
        try
        {
            return typeService.findAll();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    
    @ResponseBody
    @RequestMapping(value = "save_dic.json")
    public ModelMap saveDic(@RequestBody
    DictionaryEntity dic, ModelMap model)
    {
        DictionaryEntity entity = service.save(dic);
        if (entity != null)
        {
            model.put("success", true);
        }
        else
        {
            model.put("success", false);
        }
        return model;
    }
    
    /**
     * 新增、编辑 字典类型数据
     * 
     * @param type
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "save_dic_type.json")
    public ModelMap saveDicType(@RequestBody
    DictionaryTypeEntity type, ModelMap model)
    {
        DictionaryTypeEntity save = typeService.save(type);
        if (save != null)
        {
            model.put("success", true);
        }
        return model;
    }
    
    @ResponseBody
    @RequestMapping(value = "check_dic_value.json")
    public Boolean checkDicValue(String dictValue)
    {
        List<DictionaryEntity> dics = service.findByDicValue(dictValue);
        return CollectionUtils.isEmpty(dics);
    }
    
    @ResponseBody
    @RequestMapping(value = "check_dic_type_code.json")
    public Boolean checkDicTypeCode(String dictTypeCode)
    {
        DictionaryTypeEntity dictType = typeService.findById(dictTypeCode);
        if (dictType != null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    private List<DictionaryEntity> getFileList(String strPath)
    {
        String icon = "../../plug-in/icons/tabicons/";
        List<DictionaryEntity> list = new ArrayList<DictionaryEntity>();
        File dir = new File(strPath);
        File[] files = dir.listFiles();
        for (int i = 0; i < files.length; i++)
        {
            if (files[i].isDirectory())
            {
                getFileList(files[i].getAbsolutePath());
            }
            else
            {
                File file = files[i];
                String name = file.getName();
                String iconPath = icon + name;
                String iconName = "icon-" + name.substring(0, name.length() - 4);
                DictionaryEntity dd = new DictionaryEntity();
                dd.setDictName(iconPath);
                dd.setDictValue(iconName);
                list.add(dd);
            }
        }
        return list;
    }
}

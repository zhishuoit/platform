package com.zis.platform.core.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zis.common.SearchParams;
import com.zis.platform.common.dao.BaseDao;
import com.zis.platform.core.entity.OrgEntity;

@Repository
public interface OrgDao extends BaseDao<OrgEntity, String>
{
    
    List<OrgEntity> findByCondition(SearchParams params);
    
    List<OrgEntity> findAll();
    
    List<OrgEntity> findByParentId(@Param(value = "parentId")
    String parentId);
    
    int findCountByParentId(@Param(value = "parentId")
    String parentId);
    
    int deleteByOrgIds(String[] orgIds);
    
    List<OrgEntity> findRoot();
}
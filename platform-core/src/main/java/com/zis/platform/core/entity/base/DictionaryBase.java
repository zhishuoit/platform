package com.zis.platform.core.entity.base;

public class DictionaryBase
{
    private String dictCode;
    
    private String dictTypeCode;
    
    private String dictName;
    
    private String dictValue;
    
    private int orderNum;
    
    public String getDictCode()
    {
        return dictCode;
    }
    
    public void setDictCode(String dictCode)
    {
        this.dictCode = dictCode == null ? null : dictCode.trim();
    }
    
    public String getDictTypeCode()
    {
        return dictTypeCode;
    }
    
    public void setDictTypeCode(String dictTypeCode)
    {
        this.dictTypeCode = dictTypeCode == null ? null : dictTypeCode.trim();
    }
    
    public String getDictName()
    {
        return dictName;
    }
    
    public void setDictName(String dictName)
    {
        this.dictName = dictName == null ? null : dictName.trim();
    }
    
    public String getDictValue()
    {
        return dictValue;
    }
    
    public void setDictValue(String dictValue)
    {
        this.dictValue = dictValue == null ? null : dictValue.trim();
    }
    
    public int getOrderNum()
    {
        return orderNum;
    }
    
    public void setOrderNum(int orderNum)
    {
        this.orderNum = orderNum;
    }
    
    @Override
    public String toString()
    {
        return "DictionaryBase [dictCode=" + dictCode + ", dictTypeCode=" + dictTypeCode + ", dictName=" + dictName
            + ", dictValue=" + dictValue + ", orderNum=" + orderNum + "]";
    }
    
}
package com.zis.platform.core.action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zis.platform.core.entity.AreaEntity;
import com.zis.platform.core.service.AreaService;

@RequestMapping(value = "/platform/base/")
public class AreaAction
{
    @Autowired
    private AreaService service;
    
    @RequestMapping(value = "area.do")
    public String list()
    {
        return "base_data/area";
    }
    
    @ResponseBody
    @RequestMapping(value = "area.json")
    public List<AreaEntity> area(String areaCode)
    {
        List<AreaEntity> areas = new ArrayList<AreaEntity>();
        try
        {
            areas.add(service.findByAreaCode(areaCode));
            return areas;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    @ResponseBody
    @RequestMapping(value = "child_area.json")
    public List<AreaEntity> childArea(String parentAreaCode)
    {
        try
        {
            return service.findByParentAreaCode(parentAreaCode);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
}

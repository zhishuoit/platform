package com.zis.platform.core.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zis.common.SearchParams;
import com.zis.platform.common.service.impl.BaseServiceImpl;
import com.zis.platform.core.dao.GroupDao;
import com.zis.platform.core.dao.ResGroupDao;
import com.zis.platform.core.entity.GroupEntity;
import com.zis.platform.core.entity.ResEntity;
import com.zis.platform.core.entity.ResGroupEntity;
import com.zis.platform.core.service.GroupService;

@Service
public class GroupServiceImpl extends BaseServiceImpl<GroupEntity> implements GroupService
{
    @Autowired
    GroupDao groupDao;
    
    @Autowired
    ResGroupDao resGroupDao;
    
    @Override
    public List<GroupEntity> findByCondition(SearchParams params)
        throws Exception
    {
        return this.groupDao.findByCondition(params);
    }
    
    @Override
    public List<GroupEntity> findAll()
        throws Exception
    {
        return this.groupDao.findAll();
    }
    
    @Override
    public GroupEntity saveOrUpdate(GroupEntity group)
    {
        if (group != null)
        {
            if (StringUtils.isBlank(group.getGroupId()))
            {
                group.setGroupId(UUID.randomUUID().toString());
                int insertNum = groupDao.insertSelective(group);
                if (insertNum > 0)
                {
                    return group;
                }
            }
            else
            {
                int updateNum = groupDao.updateByPrimaryKeySelective(group);
                if (updateNum > 0)
                {
                    return group;
                }
            }
        }
        return null;
    }
    
    @Override
    public int deleteByGroupIds(String[] groupIds)
    {
        return this.groupDao.deleteByGroupIds(groupIds);
    }
    
    @Override
    public GroupEntity findGroupAndResByGroupId(String groupId)
    {
        return groupDao.findGroupAndResByGroupId(groupId);
    }
    
    @Override
    public int saveResSetting(GroupEntity group)
    {
        // 删除旧数据
        resGroupDao.deleteByGroupId(group.getGroupId());
        List<ResGroupEntity> list = new ArrayList();
        // 保存新数据
        if (group.getResList() != null && group.getResList().size() > 0)
        {
            for (ResEntity res : group.getResList())
            {
                ResGroupEntity resGroup = new ResGroupEntity();
                resGroup.setRgId(UUID.randomUUID().toString());
                resGroup.setGroupId(group.getGroupId());
                resGroup.setResId(res.getResId());
                list.add(resGroup);
            }
            return this.resGroupDao.batchSave(list);
        }
        return 1;
    }
    
}

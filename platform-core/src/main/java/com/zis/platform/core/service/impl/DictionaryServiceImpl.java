package com.zis.platform.core.service.impl;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zis.common.SearchParams;
import com.zis.platform.common.service.impl.BaseServiceImpl;
import com.zis.platform.core.dao.DictionaryDao;
import com.zis.platform.core.entity.DictionaryEntity;
import com.zis.platform.core.service.DictionaryService;

@Service
public class DictionaryServiceImpl extends BaseServiceImpl<DictionaryEntity> implements DictionaryService
{
    @Autowired
    private DictionaryDao dao;
    
    @Override
    public int delete(String[] dictCodes)
    {
        int num = 0;
        for (String id : dictCodes)
        {
            num += dao.deleteByPrimaryKey(id);
        }
        return num;
    }
    
    @Override
    public List<DictionaryEntity> findAll()
        throws Exception
    {
        return dao.findAll();
    }
    
    @Override
    public List<DictionaryEntity> findByCondition(SearchParams params)
        throws Exception
    {
        return dao.findByCondition(params);
    }
    
    @Override
    public DictionaryEntity save(DictionaryEntity dic)
    {
        if (dic == null)
        {
            return null;
        }
        int num = 0;
        if (StringUtils.isNotEmpty(dic.getDictCode()))
        {
            num = dao.updateByPrimaryKey(dic);
        }
        else
        {
            dic.setDictCode(UUID.randomUUID().toString());
            num = dao.insert(dic);
        }
        if (num > 0)
        {
            return dic;
        }
        return null;
    }
    
    @Override
    public List<DictionaryEntity> findByTypeCode(String typeCode)
    {
        if (StringUtils.isEmpty(typeCode))
        {
            return null;
        }
        return dao.findByTypeCode(typeCode);
    }
    
    @Override
    public List<DictionaryEntity> findByDicValue(String dicValue)
    {
        return dao.findByDicValue(dicValue);
    }
}

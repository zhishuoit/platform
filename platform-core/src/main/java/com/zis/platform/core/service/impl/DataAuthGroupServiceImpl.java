package com.zis.platform.core.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.zis.common.SearchParams;
import com.zis.platform.common.service.impl.BaseServiceImpl;
import com.zis.platform.core.dao.DataAuthGroupDao;
import com.zis.platform.core.entity.DataAuthGroupEntity;
import com.zis.platform.core.entity.DataAuthUserEntity;
import com.zis.platform.core.entity.UserEntity;
import com.zis.platform.core.service.DataAuthGroupService;

@Service
public class DataAuthGroupServiceImpl extends BaseServiceImpl<DataAuthGroupEntity> implements DataAuthGroupService
{
    @Autowired
    private DataAuthGroupDao dao;
    
    @Override
    public List<DataAuthGroupEntity> findByCondition(SearchParams params)
        throws Exception
    {
        return dao.findByCondition(params);
    }
    
    @Override
    public List<DataAuthGroupEntity> findAll()
        throws Exception
    {
        return dao.findAll();
    }
    
    @Override
    public boolean saveOrUpdate(DataAuthGroupEntity entity)
    {
        int num = 0;
        if (StringUtils.isEmpty(entity.getGroupDataAuthId()))
        {
            // 检查是否已存在
            if (dao.isExist(entity) > 0)
            {
                return false;
            }
            entity.setGroupDataAuthId(UUID.randomUUID().toString());
            num = dao.insert(entity);
        }
        else
        {
            num = dao.updateByPrimaryKeySelective(entity);
        }
        if (num > 0)
        {
            return true;
        }
        return false;
    }
    
    @Override
    public int deleteByGroupDataAuthIds(String[] groupDataAuthId)
    {
        return dao.deleteByGroupDataAuthIds(groupDataAuthId);
    }
    
    @Override
    public Map<String, List<DataAuthUserEntity>> findByGroupId(UserEntity user)
    {
        if (user == null)
        {
            return null;
        }
        if (user.getGroup() == null)
        {
            return null;
        }
        List<DataAuthGroupEntity> groupDataAuths = dao.findByGroupId(user.getGroup().getGroupId());
        Map<String, List<DataAuthUserEntity>> dataMaps = new HashMap<String, List<DataAuthUserEntity>>();
        for (DataAuthGroupEntity dataAuth : groupDataAuths)
        {
            String tableName = dataAuth.getTableName();
            List<DataAuthUserEntity> list = dataMaps.get(tableName);
            if (CollectionUtils.isEmpty(list))
            {
                list = new ArrayList<DataAuthUserEntity>();
            }
            DataAuthUserEntity dau = new DataAuthUserEntity();
            dau.setAuthorityScope(dataAuth.getAuthorityScope());
            dau.setTableName(tableName);
            dau.setUserId(user.getUserId());
            dau.setUserName(user.getRealName());
            list.add(dau);
            dataMaps.put(tableName.toUpperCase(), list);
        }
        return dataMaps;
    }
}

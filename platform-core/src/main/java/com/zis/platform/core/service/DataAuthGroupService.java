package com.zis.platform.core.service;

import java.util.List;
import java.util.Map;

import com.zis.platform.common.service.BaseService;
import com.zis.platform.core.entity.DataAuthGroupEntity;
import com.zis.platform.core.entity.DataAuthUserEntity;
import com.zis.platform.core.entity.UserEntity;

public interface DataAuthGroupService extends BaseService<DataAuthGroupEntity>
{
    
    boolean saveOrUpdate(DataAuthGroupEntity entity);
    
    int deleteByGroupDataAuthIds(String[] groupDataAuthId);
    
    public Map<String, List<DataAuthUserEntity>> findByGroupId(UserEntity user);
}

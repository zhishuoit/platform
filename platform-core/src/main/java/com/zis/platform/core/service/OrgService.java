package com.zis.platform.core.service;

import java.util.List;

import com.zis.platform.common.service.BaseService;
import com.zis.platform.common.web.TreeNode;
import com.zis.platform.core.entity.OrgEntity;

public interface OrgService extends BaseService<OrgEntity>
{
    
    /**
     * 获取机构的子机构信息，注意只包含一级
     * 
     * @param parentId
     * @return 一级子机构
     */
    List<OrgEntity> findByParentId(String parentId);
    
    /**
     * 获取机构的所有子机构以及子机构的子机构信息
     * 
     * @param parentId
     * @return 所有子机构
     */
    List<OrgEntity> findAllChildrenByParentId(String parentId);
    
    int deleteByOrgIds(String[] orgIds);
    
    OrgEntity saveOrUpdate(OrgEntity org);
    
    List<TreeNode> findRoot();
    
}

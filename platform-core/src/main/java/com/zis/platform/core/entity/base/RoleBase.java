package com.zis.platform.core.entity.base;

import java.util.List;

import com.zis.platform.core.entity.ResEntity;

public class RoleBase
{
    private String roleId;
    
    private String roleName;
    
    private List<ResEntity> resList;
    
    private String remarks;
    
    public String getRoleId()
    {
        return roleId;
    }
    
    public void setRoleId(String roleId)
    {
        this.roleId = roleId == null ? null : roleId.trim();
    }
    
    public String getRoleName()
    {
        return roleName;
    }
    
    public void setRoleName(String roleName)
    {
        this.roleName = roleName == null ? null : roleName.trim();
    }
    
    public String getRemarks()
    {
        return remarks;
    }
    
    public void setRemarks(String remarks)
    {
        this.remarks = remarks == null ? null : remarks.trim();
    }
    
    public List<ResEntity> getResList()
    {
        return resList;
    }
    
    public void setResList(List<ResEntity> resList)
    {
        this.resList = resList;
    }
    
    @Override
    public String toString()
    {
        return "RoleBase [roleId=" + roleId + ", roleName=" + roleName + ", resList=" + resList + ", remarks="
            + remarks + "]";
    }
    
}
package com.zis.platform.core.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zis.common.Pagination;
import com.zis.common.SearchParams;
import com.zis.platform.common.web.TreeNode;
import com.zis.platform.core.entity.DeptEntity;
import com.zis.platform.core.service.DeptService;
import com.zis.platform.core.service.OrgService;

@Controller
@RequestMapping(value = "/platform/dept/")
public class DeptAction
{
    @Autowired
    private DeptService deptService;
    
    @Autowired
    private OrgService orgService;
    
    @RequestMapping(value = "list.do")
    public String list()
    {
        return "dept/list";
    }
    
    @ResponseBody
    @RequestMapping("list.json")
    public Pagination<DeptEntity> list(SearchParams params)
        throws Exception
    {
        Pagination<DeptEntity> page = deptService.findPageByCondition(params);
        return page;
    }
    
    @ResponseBody
    @RequestMapping("listOrgRoot.json")
    public List<TreeNode> listRoot(String id)
        throws Exception
    {
        return orgService.findRoot();
    }
    
    @RequestMapping("save.json")
    @ResponseBody
    public ModelMap add(@RequestBody
    DeptEntity dept, ModelMap model)
    {
        DeptEntity deptEntity = deptService.saveOrUpdate(dept);
        if (deptEntity != null)
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
    @RequestMapping("delete.json")
    @ResponseBody
    public ModelMap add(@RequestBody
    String[] deptIds, ModelMap model)
    {
        if (deptService.deleteByDeptIds(deptIds) > 0)
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
}

package com.zis.platform.core.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.zis.platform.core.entity.ResRoleEntity;

@Repository
public interface ResRoleDao
{
    
    void deleteByRoleId(String roleId);
    
    int batchSave(List<ResRoleEntity> list);
    
}

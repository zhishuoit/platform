package com.zis.platform.core.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.util.CollectionUtils;

import com.zis.platform.core.entity.base.RoleBase;

public class RoleEntity extends RoleBase implements Serializable
{
    
    private static final long serialVersionUID = 8075794135631232147L;
    
    /**
     * 获取角色下的所有资源信息
     * 
     * @return
     */
    public Set<String> getResourcesStr()
    {
        return getChildRes(getResList());
    }
    
    private Set<String> getChildRes(List<ResEntity> resList)
    {
        Set<String> set = new HashSet<String>();
        if (!CollectionUtils.isEmpty(resList))
        {
            for (ResEntity res : resList)
            {
                String baseResUrl = res.getBaseResUrl();
                if (StringUtils.isNotEmpty(baseResUrl))
                {
                    String[] split = baseResUrl.split("@");
                    for (String url : split)
                    {
                        set.add(url);
                    }
                }
                List<ResEntity> children = res.getChildren();
                if (!CollectionUtils.isEmpty(children))
                {
                    set.addAll(getChildRes(children));
                }
                set.add(res.getResUrl());
            }
        }
        return set;
    }
}

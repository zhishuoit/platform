package com.zis.platform.core.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zis.platform.core.entity.ResEntity;
import com.zis.platform.core.entity.SysEntity;
import com.zis.platform.core.service.ResService;
import com.zis.platform.core.service.SysService;

@RequestMapping(value = "/platform/res/")
public class ResAction
{
    @Autowired
    private ResService resService;
    
    @Autowired
    private SysService sysService;
    
    @RequestMapping(value = "list.do")
    public String list()
    {
        return "res/list";
    }
    
    @ResponseBody
    @RequestMapping(value = "save.json")
    public ModelMap save(@RequestBody
    ResEntity res, ModelMap model, HttpServletRequest req)
    {
        model.put("state", resService.saveOrUpdate(res) > 0 ? true : false);
        return model;
    }
    
    @ResponseBody
    @RequestMapping(value = "delete.json")
    public ModelMap delete(@RequestBody
    String[] ids, ModelMap model)
    {
        model.put("deleteNum", resService.deleteByResIds(ids));
        return model;
    }
    
    @RequestMapping(value = "edit.do")
    public String edit(String sysId, String resId, ModelMap model)
    {
        if (!StringUtils.isBlank(sysId))
        {
            SysEntity sys = sysService.findBySysId(sysId);
            model.addAttribute("sys", sys);
            return "res/edit_sys";
        }
        
        if (!StringUtils.isBlank(resId))
        {
            ResEntity res = resService.findByResId(resId);
            model.addAttribute("resource", res);
            return "res/edit_res";
        }
        return null;
    }
    
    @RequestMapping("update.json")
    @ResponseBody
    public ModelMap updateOrderNum(@RequestBody
    ResEntity[] resources, ModelMap model)
    {
        if (resService.update(resources) > 0)
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
    /**
     * 根据父节点 以及 层级获取树结构
     * 
     * @param pId 父节点ID
     * @param pLevel
     * @param pName
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "root.json")
    public List<SysEntity> findAllSysAndRes(String pId, String pLevel, String pName)
    {
        return resService.findAllSysAndRes();
    }
    
    @ResponseBody
    @RequestMapping(value = "children.json")
    public List<ResEntity> children(String sysId, String resId)
    {
        if (!StringUtils.isBlank(sysId))
        {
            return resService.findBySysId(sysId);
        }
        
        if (!StringUtils.isBlank(resId))
        {
            return resService.findByParentId(resId);
        }
        return null;
    }
}

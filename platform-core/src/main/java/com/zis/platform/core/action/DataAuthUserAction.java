package com.zis.platform.core.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zis.common.Pagination;
import com.zis.common.SearchParams;
import com.zis.platform.core.entity.DataAuthGroupEntity;
import com.zis.platform.core.entity.DataAuthRoleEntity;
import com.zis.platform.core.entity.DataAuthUserEntity;
import com.zis.platform.core.entity.GroupEntity;
import com.zis.platform.core.entity.RoleEntity;
import com.zis.platform.core.entity.UserEntity;
import com.zis.platform.core.service.DataAuthGroupService;
import com.zis.platform.core.service.DataAuthRoleService;
import com.zis.platform.core.service.DataAuthUserService;
import com.zis.platform.core.service.GroupService;
import com.zis.platform.core.service.RoleService;
import com.zis.platform.core.service.UserService;

@Controller
@RequestMapping("/platform/data_auth_user/")
public class DataAuthUserAction
{
    @Autowired
    private DataAuthUserService service;
    
    @Autowired
    private DataAuthRoleService authRoleService;
    
    @Autowired
    private DataAuthGroupService authGroupService;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private RoleService roleService;
    
    @Autowired
    private GroupService groupService;
    
    @RequestMapping("index.do")
    public String list()
    {
        return "data_auth_user/index";
    }
    
    @RequestMapping("listUserAuth.do")
    public String listUserAuth()
    {
        return "data_auth_user/listUserAuth";
    }
    
    @RequestMapping("listRoleAuth.do")
    public String listRoleAuth()
    {
        return "data_auth_user/listRoleAuth";
    }
    
    @RequestMapping("listGroupAuth.do")
    public String listGroupAuth()
    {
        return "data_auth_user/listGroupAuth";
    }
    
    /**
     * 用户数据权限
     * 
     * @param params
     * @return
     */
    @RequestMapping("list.json")
    @ResponseBody
    public Pagination<DataAuthUserEntity> list(SearchParams params)
    {
        try
        {
            Pagination<DataAuthUserEntity> pages = service.findPageByCondition(params);
            return pages;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * 角色数据权限
     * 
     * @param params
     * @return
     */
    @RequestMapping("list_roleAuth.json")
    @ResponseBody
    public Pagination<DataAuthRoleEntity> list_roleAuth(SearchParams params)
    {
        try
        {
            Pagination<DataAuthRoleEntity> pages = authRoleService.findPageByCondition(params);
            return pages;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * 用户组数据权限
     * 
     * @param params
     * @return
     */
    @RequestMapping("list_groupAuth.json")
    @ResponseBody
    public Pagination<DataAuthGroupEntity> list_groupAuth(SearchParams params)
    {
        try
        {
            Pagination<DataAuthGroupEntity> pages = authGroupService.findPageByCondition(params);
            return pages;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * 用户信息 下拉表格
     * 
     * @param params
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping("listUser.json")
    public Pagination<UserEntity> listUser(SearchParams params)
        throws Exception
    {
        Pagination<UserEntity> page = userService.findPageByCondition(params);
        return page;
    }
    
    /**
     * 角色信息 下拉表格
     * 
     * @param params
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping("listRole.json")
    public Pagination<RoleEntity> listRole(SearchParams params)
        throws Exception
    {
        Pagination<RoleEntity> page = roleService.findPageByCondition(params);
        return page;
    }
    
    /**
     * 用户组信息 下拉表格
     * 
     * @param params
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping("listGroup.json")
    public Pagination<GroupEntity> listGroup(SearchParams params)
        throws Exception
    {
        Pagination<GroupEntity> page = groupService.findPageByCondition(params);
        return page;
    }
    
    /**
     * 保存用户 数据权限
     * 
     * @param entity
     * @param model
     * @return
     */
    @RequestMapping("save.json")
    @ResponseBody
    public ModelMap save(@RequestBody
    DataAuthUserEntity entity, ModelMap model)
    {
        if (service.saveOrUpdate(entity))
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
    /**
     * 保存角色 数据权限
     * 
     * @param entity
     * @param model
     * @return
     */
    @RequestMapping("saveRoleAuth.json")
    @ResponseBody
    public ModelMap saveRoleAuth(@RequestBody
    DataAuthRoleEntity entity, ModelMap model)
    {
        if (authRoleService.saveOrUpdate(entity))
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
    /**
     * 保存用户组 数据权限
     * 
     * @param entity
     * @param model
     * @return
     */
    @RequestMapping("saveGroupAuth.json")
    @ResponseBody
    public ModelMap saveGroupAuth(@RequestBody
    DataAuthGroupEntity entity, ModelMap model)
    {
        if (authGroupService.saveOrUpdate(entity))
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
    /**
     * 删除 用户数据权限
     * 
     * @param userDataAuthIds
     * @param model
     * @return
     */
    @RequestMapping("/delete.json")
    @ResponseBody
    public ModelMap deleteUserAuth(@RequestBody
    String[] userDataAuthIds, ModelMap model)
    {
        if (service.deleteByUserDataAuthIds(userDataAuthIds) > 0)
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
    /**
     * 删除角色 数据权限
     * 
     * @param roleDataAuthIds
     * @param model
     * @return
     */
    @RequestMapping("/deleteRoleAuth.json")
    @ResponseBody
    public ModelMap deleteRoleAuth(@RequestBody
    String[] roleDataAuthIds, ModelMap model)
    {
        if (authRoleService.deleteByRoleDataAuthIds(roleDataAuthIds) > 0)
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
    /**
     * 删除用户组 数据权限
     * 
     * @param groupDataAuthIds
     * @param model
     * @return
     */
    @RequestMapping("/deleteGroupAuth.json")
    @ResponseBody
    public ModelMap deleteGroupAuth(@RequestBody
    String[] groupDataAuthId, ModelMap model)
    {
        if (authGroupService.deleteByGroupDataAuthIds(groupDataAuthId) > 0)
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
}

package com.zis.platform.core.entity;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import com.zis.platform.common.util.CacheCoreUtil;
import com.zis.platform.core.entity.base.DataAuthStrategyBase;

public class DataAuthStrategyEntity extends DataAuthStrategyBase
{
    
    private static final long serialVersionUID = -2884272478620832268L;
    
    private static final String MATCH_KEY = "{值}";
    
    public String getComparisonOperatorName()
    {
        if (StringUtils.isNotEmpty(getComparisonOperator()))
        {
            DictionaryEntity dictionary = CacheCoreUtil.getDictionaryByValue(getComparisonOperator());
            if (dictionary != null)
            {
                return dictionary.getDictName();
            }
        }
        return null;
    }
    
    public String getLogicalRelationName()
    {
        if (StringUtils.isNotEmpty(getLogicalRelation()))
        {
            DictionaryEntity dictionary = CacheCoreUtil.getDictionaryByValue(getLogicalRelation());
            if (dictionary != null)
            {
                return dictionary.getDictName();
            }
        }
        return null;
    }
    
    public String toString(List<DataAuthUserEntity> list)
    {
        
        StringBuffer select = new StringBuffer("SELECT * FROM ");
        select.append(getTableName());
        select.append(" WHERE ");
        if (CollectionUtils.isEmpty(list))
        {
            select.append("1=0");
        }
        else
        {
            if ("=".equals(getComparisonOperator()) || "LIKE".equals(getComparisonOperator().toUpperCase()))
            {
                for (int i = 0; i < list.size(); i++)
                {
                    DataAuthUserEntity dataAuth = list.get(i);
                    select.append(getColumnName());
                    select.append(" ");
                    select.append(getComparisonOperator());
                    select.append(" ");
                    select.append("'");
                    select.append(getMatchingRule().replace(MATCH_KEY, dataAuth.getAuthorityScope()));
                    select.append("'");
                    if ((i + 1) != list.size())
                    {
                        select.append(" OR ");
                    }
                }
            }
            else if ("IN".equals(getComparisonOperator().toUpperCase()))
            {
                select.append(getColumnName());
                select.append(" IN (");
                for (DataAuthUserEntity dataAuth : list)
                {
                    select.append("'");
                    select.append(dataAuth.getAuthorityScope());
                    select.append("'");
                    select.append(",");
                }
                select.append(")");
            }
        }
        return select.toString();
    }
}

package com.zis.platform.core.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.zis.common.SearchParams;
import com.zis.platform.common.dao.BaseDao;
import com.zis.platform.core.entity.DeptEntity;

@Repository
public interface DeptDao extends BaseDao<DeptEntity, String>
{
    
    List<DeptEntity> findByCondition(SearchParams params);
    
    List<DeptEntity> findAll();
    
    int deleteByDeptIds(String[] deptIds);
}
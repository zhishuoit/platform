package com.zis.platform.core.entity.base;

import java.io.Serializable;

public class UserRoleBase implements Serializable
{
    private String ruId;
    
    private String roleId;
    
    private String userId;
    
    public String getRuId()
    {
        return ruId;
    }
    
    public void setRuId(String ruId)
    {
        this.ruId = ruId;
    }
    
    public String getRoleId()
    {
        return roleId;
    }
    
    public void setRoleId(String roleId)
    {
        this.roleId = roleId;
    }
    
    public String getUserId()
    {
        return userId;
    }
    
    public void setUserId(String userId)
    {
        this.userId = userId;
    }
    
}

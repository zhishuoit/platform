package com.zis.platform.core.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zis.common.Pagination;
import com.zis.common.SearchParams;
import com.zis.platform.core.entity.DataAuthStrategyEntity;
import com.zis.platform.core.service.DataAuthStrategyService;

@Controller
@RequestMapping("/platform/data_auth_strategy/")
public class DataAuthStrategyAction
{
    @Autowired
    private DataAuthStrategyService service;
    
    @RequestMapping("list.do")
    public String list()
    {
        return "data_auth_strategy/list";
    }
    
    @RequestMapping("list.json")
    @ResponseBody
    public Pagination<DataAuthStrategyEntity> list(SearchParams params)
    {
        try
        {
            return service.findPageByCondition(params);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    @RequestMapping("save.json")
    @ResponseBody
    public ModelMap save(@RequestBody
    DataAuthStrategyEntity entity, ModelMap model)
    {
        if (service.saveOrUpdate(entity))
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
    @RequestMapping("/delete.json")
    @ResponseBody
    public ModelMap add(@RequestBody
    String[] strategyIds, ModelMap model)
    {
        if (service.deleteByStrategyIds(strategyIds) > 0)
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
    @RequestMapping("check_table_name.json")
    @ResponseBody
    public Boolean checkTableName(String tableName)
    {
        return true;
    }
}

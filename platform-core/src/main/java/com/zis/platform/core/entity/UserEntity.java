package com.zis.platform.core.entity;

import java.io.Serializable;
import java.util.List;

import com.zis.platform.common.util.CacheCoreUtil;
import com.zis.platform.core.entity.base.UserBase;

public class UserEntity extends UserBase implements Serializable
{
    
    public UserEntity()
    {
        super();
    }
    
    public UserEntity(String loginName)
    {
        super(loginName);
    }
    
    public UserEntity(String loginName, String password)
    {
        super(loginName, password);
    }
    
    private String orgName;
    
    private String groupName;
    
    public String getEnableName()
    {
        if (isEnable() != null)
        {
            DictionaryEntity dictionary = CacheCoreUtil.getDictionaryByValue(String.valueOf(isEnable()));
            if (dictionary != null)
            {
                return dictionary.getDictName();
            }
        }
        return null;
    }
    
    public String getLockedName()
    {
        if (isLocked() != null)
        {
            DictionaryEntity dictionary = CacheCoreUtil.getDictionaryByValue(String.valueOf(isLocked()));
            if (dictionary != null)
            {
                return dictionary.getDictName();
            }
        }
        return null;
    }
    
    public String getUserTypeName()
    {
        if (getUserType() != null)
        {
            DictionaryEntity dictionary = CacheCoreUtil.getDictionaryByValue(getUserType());
            if (dictionary != null)
            {
                return dictionary.getDictName();
            }
        }
        return null;
    }
    
    public String getIsSupperManagerName()
    {
        DictionaryEntity dictionary = CacheCoreUtil.getDictionaryByValue(String.valueOf(isSupperManager()));
        if (dictionary != null)
        {
            return dictionary.getDictName();
        }
        return null;
    }
    
    public String getOrgName()
    {
        return orgName;
    }
    
    public void setOrgName(String orgName)
    {
        this.orgName = orgName;
    }
    
    public String getGroupName()
    {
        return groupName;
    }
    
    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }
    
    private List<RoleEntity> roles;
    
    private List<OrgEntity> orgs;
    
    public List<RoleEntity> getRoles()
    {
        return roles;
    }
    
    public void setRoles(List<RoleEntity> roles)
    {
        this.roles = roles;
    }
    
    public List<OrgEntity> getOrgs()
    {
        return orgs;
    }
    
    public void setOrgs(List<OrgEntity> orgs)
    {
        this.orgs = orgs;
    }
    
}

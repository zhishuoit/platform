package com.zis.platform.core.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.zis.common.SearchParams;
import com.zis.platform.common.dao.BaseDao;
import com.zis.platform.core.entity.LogOpterationEntity;

@Repository
public interface LogOpterationDao extends BaseDao<LogOpterationEntity, String>
{
    public List<LogOpterationEntity> findByCondition(SearchParams params);
    
    public List<LogOpterationEntity> findAll();
}
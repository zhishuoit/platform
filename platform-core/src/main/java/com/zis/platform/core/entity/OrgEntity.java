package com.zis.platform.core.entity;

import java.io.Serializable;
import java.util.List;

import com.zis.platform.common.util.CacheCoreUtil;
import com.zis.platform.core.entity.base.OrgBase;

/**
 * 组织机构
 * 
 * @author zhaohaitao
 * 
 */
public class OrgEntity extends OrgBase implements Serializable
{
    
    private boolean isParent;
    
    private List<OrgEntity> children;
    
    public boolean isIsParent()
    {
        return isParent;
    }
    
    public void setIsParent(boolean isParent)
    {
        this.isParent = isParent;
    }
    
    public List<OrgEntity> getChildren()
    {
        return children;
    }
    
    public void setChildren(List<OrgEntity> children)
    {
        this.children = children;
    }
    
    public String getOrgTypeName()
    {
        if (getOrgType() != null)
        {
            DictionaryEntity dictionary = CacheCoreUtil.getDictionaryByValue(getOrgType());
            if (dictionary != null)
            {
                return dictionary.getDictName();
            }
        }
        return null;
    }
}

package com.zis.platform.core.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.zis.platform.core.entity.ResGroupEntity;

@Repository
public interface ResGroupDao
{
    
    void deleteByGroupId(String roleId);
    
    int batchSave(List<ResGroupEntity> list);
    
}

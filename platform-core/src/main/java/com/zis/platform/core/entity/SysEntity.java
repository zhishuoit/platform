package com.zis.platform.core.entity;

import java.io.Serializable;

import com.zis.platform.core.entity.base.SysBase;

/**
 * 应用系统
 * 
 * @author zhaohaitao
 * 
 */
public class SysEntity extends SysBase implements Serializable
{
    
    /**
     * 
     */
    private static final long serialVersionUID = -558137898161896866L;
    
}

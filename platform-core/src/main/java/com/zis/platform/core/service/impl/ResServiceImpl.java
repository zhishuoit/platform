package com.zis.platform.core.service.impl;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zis.common.SearchParams;
import com.zis.platform.common.service.impl.BaseServiceImpl;
import com.zis.platform.core.dao.ResDao;
import com.zis.platform.core.dao.SysDao;
import com.zis.platform.core.entity.ResEntity;
import com.zis.platform.core.entity.SysEntity;
import com.zis.platform.core.service.ResService;

@Service
public class ResServiceImpl extends BaseServiceImpl<ResEntity> implements ResService
{
    @Autowired
    private ResDao dao;
    
    @Autowired
    private SysDao sysDao;
    
    public List<SysEntity> findAllSysAndRes()
    {
        return sysDao.findAllSysAndRes();
    }
    
    @Override
    public List<ResEntity> findByParentId(String parentId)
    {
        return dao.findByParentId(parentId);
    }
    
    @Override
    public List<ResEntity> findByCondition(SearchParams params)
        throws Exception
    {
        return null;
    }
    
    @Override
    public List<ResEntity> findAll()
        throws Exception
    {
        return dao.findAll();
    }
    
    @Override
    public List<ResEntity> findBySysId(String sysId)
    {
        if (StringUtils.isBlank(sysId))
        {
            return null;
        }
        return dao.findBySysId(sysId);
    }
    
    @Override
    public int saveOrUpdate(ResEntity res)
    {
        if (res == null)
        {
            return 0;
        }
        int num = 0;
        if (StringUtils.isBlank(res.getResId()))
        {
            res.setResId(UUID.randomUUID().toString());
            num = dao.insertSelective(res);
        }
        else
        {
            num = dao.updateByPrimaryKeySelective(res);
        }
        return num;
    }
    
    @Override
    public ResEntity findByResId(String resId)
    {
        if (StringUtils.isBlank(resId))
        {
            return null;
        }
        return dao.selectByPrimaryKey(resId);
    }
    
    @Override
    public int update(ResEntity[] resources)
    {
        int num = 0;
        if (resources != null && resources.length > 0)
        {
            for (ResEntity res : resources)
            {
                num += dao.updateByPrimaryKeySelective(res);
            }
        }
        return num;
    }
    
    @Override
    public int deleteByResIds(String[] resIds)
    {
        int num = 0;
        if (resIds != null && resIds.length > 0)
        {
            for (String resId : resIds)
            {
                List<ResEntity> children = dao.findByParentId(resId);
                if (children == null || children.size() == 0)
                {
                    num += dao.deleteByPrimaryKey(resId);
                }
            }
        }
        return num;
    }
    
}

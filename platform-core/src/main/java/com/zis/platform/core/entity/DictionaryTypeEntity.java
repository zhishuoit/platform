package com.zis.platform.core.entity;

import java.io.Serializable;

import com.zis.platform.core.entity.base.DictionaryTypeBase;

public class DictionaryTypeEntity extends DictionaryTypeBase implements Serializable
{
    
    
    public DictionaryTypeEntity(String dictTypeCode, String dictTypeName, String remarks)
    {
        super(dictTypeCode, dictTypeName, remarks);
    }

    public DictionaryTypeEntity()
    {
        super();
    }

    /**
     * 
     */
    private static final long serialVersionUID = -9113207468245427256L;
    
}

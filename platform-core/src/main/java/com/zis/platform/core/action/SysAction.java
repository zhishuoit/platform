package com.zis.platform.core.action;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zis.common.Pagination;
import com.zis.common.SearchParams;
import com.zis.platform.core.entity.SysEntity;
import com.zis.platform.core.service.SysService;

@Controller
@RequestMapping("/platform/system")
public class SysAction
{
    @Autowired
    private SysService service;
    
    @RequestMapping("/list.do")
    public String system(HttpServletRequest request, ModelMap model, SearchParams params)
    {
        return "system/list";
    }
    
    @ResponseBody
    @RequestMapping("/list.json")
    public Pagination<SysEntity> list(SearchParams params)
    {
        try
        {
            Pagination<SysEntity> page = service.findPageByCondition(params);
            return page;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    @RequestMapping("/save.json")
    @ResponseBody
    public ModelMap add(@RequestBody
    SysEntity sys, ModelMap model)
    {
        SysEntity sysEntity = service.saveOrUpdate(sys);
        if (sysEntity != null)
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
    @RequestMapping("/delete.json")
    @ResponseBody
    public ModelMap add(@RequestBody
    String[] sysIds, ModelMap model)
    {
        if (service.deleteBySysIds(sysIds) > 0)
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
    @RequestMapping("/update.json")
    @ResponseBody
    public ModelMap updateOrderNum(@RequestBody
    SysEntity[] systems, ModelMap model)
    {
        if (service.update(systems) > 0)
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
    @ResponseBody
    @RequestMapping(value = "check_content_path.json")
    public Boolean CheckContentPath(String contentPath)
    {
        SysEntity sys = service.findByContentPath(contentPath);
        if (sys == null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
}

package com.zis.platform.core.entity.base;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zis.platform.core.entity.GroupEntity;
import com.zis.platform.core.entity.OrgEntity;

public class UserBase
{
    private String userId;
    
    private String loginName;
    
    private String password;
    
    private String salt;
    
    private String realName;
    
    private OrgEntity org;
    
    private String orgId;
    
    private GroupEntity group;
    
    private String groupId;
    
    private String userType;
    
    private String idNumber;
    
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    private Date birthday;
    
    private String sex;
    
    private String phone;
    
    private String email;
    
    private String address;
    
    // 是否为超级管理员
    private boolean isSupperManager;
    
    // 启用、禁用
    private Boolean enable;
    
    // 锁定
    private Boolean locked;
    
    // 上次登录时间
    private Date lastLoginDate;
    
    // 上次登录IP
    private String lastLoginIp;
    
    // 上次登录mac地址
    private String lastLoginMac;
    
    // 本次登录时间
    private Date nowLoginDate;
    
    // 本次登录IP
    private String nowLoginIp;
    
    // 本次登录mac地址
    private String nowLoginMac;
    
    public UserBase()
    {
        super();
    }
    
    public UserBase(String loginName)
    {
        super();
        this.loginName = loginName;
    }
    
    public UserBase(String loginName, String password)
    {
        super();
        this.loginName = loginName;
        this.password = password;
    }
    
    public String getUserId()
    {
        return userId;
    }
    
    public void setUserId(String userId)
    {
        this.userId = userId == null ? null : userId.trim();
    }
    
    public String getLoginName()
    {
        return loginName;
    }
    
    public void setLoginName(String loginName)
    {
        this.loginName = loginName == null ? null : loginName.trim();
    }
    
    public String getPassword()
    {
        return password;
    }
    
    public void setPassword(String password)
    {
        this.password = password == null ? null : password.trim();
    }
    
    public String getSalt()
    {
        return salt;
    }
    
    public void setSalt(String salt)
    {
        this.salt = salt == null ? null : salt.trim();
    }
    
    public String getRealName()
    {
        return realName;
    }
    
    public void setRealName(String realName)
    {
        this.realName = realName == null ? null : realName.trim();
    }
    
    public OrgEntity getOrg()
    {
        return org;
    }
    
    public void setOrg(OrgEntity org)
    {
        this.org = org;
    }
    
    public String getGroupId()
    {
        return groupId;
    }
    
    public void setGroupId(String groupId)
    {
        this.groupId = groupId == null ? null : groupId.trim();
    }
    
    public String getUserType()
    {
        return userType;
    }
    
    public void setUserType(String userType)
    {
        this.userType = userType == null ? null : userType.trim();
    }
    
    public String getIdNumber()
    {
        return idNumber;
    }
    
    public void setIdNumber(String idNumber)
    {
        this.idNumber = idNumber == null ? null : idNumber.trim();
    }
    
    public Date getBirthday()
    {
        return birthday;
    }
    
    public void setBirthday(Date birthday)
    {
        this.birthday = birthday;
    }
    
    public String getSex()
    {
        return sex;
    }
    
    public void setSex(String sex)
    {
        this.sex = sex == null ? null : sex.trim();
    }
    
    public String getPhone()
    {
        return phone;
    }
    
    public void setPhone(String phone)
    {
        this.phone = phone == null ? null : phone.trim();
    }
    
    public String getEmail()
    {
        return email;
    }
    
    public void setEmail(String email)
    {
        this.email = email == null ? null : email.trim();
    }
    
    public String getAddress()
    {
        return address;
    }
    
    public void setAddress(String address)
    {
        this.address = address == null ? null : address.trim();
    }
    
    public Boolean isEnable()
    {
        return enable;
    }
    
    public void setEnable(Boolean isEnable)
    {
        this.enable = isEnable;
    }
    
    public Boolean isLocked()
    {
        return locked;
    }
    
    public void setLocked(Boolean isLocked)
    {
        this.locked = isLocked;
    }
    
    public Date getLastLoginDate()
    {
        return lastLoginDate;
    }
    
    public void setLastLoginDate(Date lastLoginDate)
    {
        this.lastLoginDate = lastLoginDate;
    }
    
    public String getLastLoginIp()
    {
        return lastLoginIp;
    }
    
    public void setLastLoginIp(String lastLoginIp)
    {
        this.lastLoginIp = lastLoginIp;
    }
    
    public String getLastLoginMac()
    {
        return lastLoginMac;
    }
    
    public void setLastLoginMac(String lastLoginMac)
    {
        this.lastLoginMac = lastLoginMac;
    }
    
    public Date getNowLoginDate()
    {
        return nowLoginDate;
    }
    
    public void setNowLoginDate(Date nowLoginDate)
    {
        this.nowLoginDate = nowLoginDate;
    }
    
    public String getNowLoginIp()
    {
        return nowLoginIp;
    }
    
    public void setNowLoginIp(String nowLoginIp)
    {
        this.nowLoginIp = nowLoginIp;
    }
    
    public String getNowLoginMac()
    {
        return nowLoginMac;
    }
    
    public void setNowLoginMac(String nowLoginMac)
    {
        this.nowLoginMac = nowLoginMac;
    }
    
    public boolean isSupperManager()
    {
        return isSupperManager;
    }
    
    public void setSupperManager(boolean isSupperManager)
    {
        this.isSupperManager = isSupperManager;
    }
    
    public String getOrgId()
    {
        return orgId;
    }
    
    public void setOrgId(String orgId)
    {
        this.orgId = orgId;
    }
    
    public GroupEntity getGroup()
    {
        return group;
    }
    
    public void setGroup(GroupEntity group)
    {
        this.group = group;
    }
    
}
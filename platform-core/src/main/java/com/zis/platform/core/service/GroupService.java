package com.zis.platform.core.service;

import com.zis.platform.common.service.BaseService;
import com.zis.platform.core.entity.GroupEntity;

public interface GroupService extends BaseService<GroupEntity>
{
    
    GroupEntity saveOrUpdate(GroupEntity group);
    
    int deleteByGroupIds(String[] groupIds);
    
    GroupEntity findGroupAndResByGroupId(String groupId);
    
    int saveResSetting(GroupEntity group);
    
}

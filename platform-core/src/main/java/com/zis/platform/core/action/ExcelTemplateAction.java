package com.zis.platform.core.action;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.zis.common.Pagination;
import com.zis.common.SearchParams;
import com.zis.platform.common.util.UploadUtil;
import com.zis.platform.core.entity.ExcelTemplateEntity;
import com.zis.platform.core.service.ExcelTemplateService;
import com.zis.util.DateUtils;

@Controller
@RequestMapping(value = "/platform/exceltemplate/")
public class ExcelTemplateAction
{
    
    @Autowired
    private ExcelTemplateService service;
    
    @RequestMapping(value = "list.do")
    public String list()
    {
        return "exceltemplate/list";
    }
    
    @RequestMapping(value = "toAdd.do")
    public String toAdd()
    {
        return "exceltemplate/add";
    }
    
    @RequestMapping(value = "toUpdate.do")
    public String toUpdate(String etId, ModelMap model)
    {
        ExcelTemplateEntity et = service.getById(etId);
        model.put("et", et);
        return "exceltemplate/update";
    }
    
    @ResponseBody
    @RequestMapping(value = "list.json")
    public Pagination<ExcelTemplateEntity> list(SearchParams params)
    {
        try
        {
            Pagination<ExcelTemplateEntity> pageList = service.findPageByCondition(params);
            return pageList;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    @RequestMapping("/delete.json")
    @ResponseBody
    public ModelMap add(@RequestBody
    String[] etIds, ModelMap model)
    {
        model.put("state", service.deleteByEtIds(etIds) > 0);
        return model;
    }
    
    @ResponseBody
    @RequestMapping(value = "check_etname.json")
    public Boolean checkEtname(String etName)
    {
        List<ExcelTemplateEntity> dics = service.findByEtName(etName);
        return CollectionUtils.isEmpty(dics);
    }
    
    @ResponseBody
    @RequestMapping(value = "check_etcode.json")
    public Boolean checkEtCode(String etCode)
    {
        List<ExcelTemplateEntity> dics = service.findByEtCode(etCode);
        return CollectionUtils.isEmpty(dics);
    }
    
    @RequestMapping(value = "save.do")
    public String doMerge(HttpServletRequest request, ExcelTemplateEntity entity,
        @RequestParam(value = "etFile", required = false)
        MultipartFile file, boolean replaceEtFile, ModelMap model)
    {
        if (replaceEtFile == true)
        {
            String realPath = request.getSession().getServletContext().getRealPath("/");
            String modelPath = "/excelTemplate/" + DateUtils.format(new Date(), "yyyyMMdd") + "/";
            if (file != null && file.getSize() > 0)
            {
                String tmpPath = UploadUtil.doUpload(realPath, modelPath, file);// 上传文件，上传文件到指定目录下
                entity.setEtPath(tmpPath);
            }
        }
        entity = service.saveOrUpdate(entity);
        model.put("result", entity != null);
        return "exceltemplate/result";
    }
    
    @RequestMapping("codeToDownExceltemplate.do")
    public ResponseEntity<byte[]> download(HttpServletRequest request, String code)
        throws Exception
    {
        ExcelTemplateEntity entity = this.service.getByCode(code);
        String realPath = request.getSession().getServletContext().getRealPath("/");
        File file = new File(realPath + entity.getEtPath());
        HttpHeaders headers = new HttpHeaders();
        String fileType = entity.getEtPath().substring(entity.getEtPath().lastIndexOf("."));
        String fileName = new String((entity.getEtName() + fileType).getBytes("UTF-8"), "iso-8859-1");// 为了解决中文名称乱码问题
        headers.setContentDispositionFormData("attachment", fileName);
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.CREATED);
    }
}

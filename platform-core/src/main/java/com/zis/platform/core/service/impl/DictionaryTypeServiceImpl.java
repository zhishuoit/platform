package com.zis.platform.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zis.common.SearchParams;
import com.zis.platform.common.service.impl.BaseServiceImpl;
import com.zis.platform.core.dao.DictionaryTypeDao;
import com.zis.platform.core.entity.DictionaryTypeEntity;
import com.zis.platform.core.service.DictionaryTypeService;

@Service("dictionaryTypeService")
public class DictionaryTypeServiceImpl extends BaseServiceImpl<DictionaryTypeEntity> implements DictionaryTypeService
{
    @Autowired
    private DictionaryTypeDao dao;
    
    @Override
    public List<DictionaryTypeEntity> findByCondition(SearchParams params)
        throws Exception
    {
        return null;
    }
    
    @Override
    public List<DictionaryTypeEntity> findAll()
        throws Exception
    {
        return dao.findAll();
    }
    
    @Override
    public DictionaryTypeEntity save(DictionaryTypeEntity type)
    {
        DictionaryTypeEntity typeInDB = dao.selectByPrimaryKey(type.getDictTypeCode());
        if (typeInDB != null)
        {
            dao.updateByPrimaryKey(type);
            return type;
        }
        if (dao.insert(type) > 0)
        {
            return type;
        }
        return null;
    }
    
    @Override
    public DictionaryTypeEntity findById(String typeCode)
    {
        return dao.selectByPrimaryKey(typeCode);
    }
    
    @Override
    public List<DictionaryTypeEntity> findAllTypeAndDics()
    {
        return dao.findAllTypeAndDics();
    }
}

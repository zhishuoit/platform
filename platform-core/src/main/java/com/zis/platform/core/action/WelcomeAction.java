package com.zis.platform.core.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zis.platform.common.authentication.UserToken;
import com.zis.platform.common.util.CacheCoreUtil;
import com.zis.platform.core.entity.SysEntity;
import com.zis.util.CacheUtil;

/**
 * 管理后台框架页面
 * 
 * @author zhaohaitao
 * 
 */
@Controller
@RequestMapping(value = "/*/")
public class WelcomeAction
{
    
    @RequestMapping(value = "index.do", method = RequestMethod.GET)
    public String index(HttpServletRequest request, ModelMap model)
    {
        List<SysEntity> allSysList = CacheCoreUtil.getSysInfos();
        UserToken loginUser = (UserToken)SecurityUtils.getSubject().getPrincipal();
        SysEntity sysInfo = CacheCoreUtil.getSysInfo(request);
        List<String> sysIds = loginUser.getSysIds();
        List<SysEntity> sysList = new ArrayList<SysEntity>();
        for (SysEntity sys : allSysList)
        {
            if (sysIds.contains(sys.getSysId()))
            {
                sysList.add(sys);
            }
        }
        String serverInfo = request.getServletContext().getServerInfo();
        // 读取系统相关信息
        Properties props = System.getProperties();
        Runtime runtime = Runtime.getRuntime();
        long freeMemoery = runtime.freeMemory();
        long totalMemory = runtime.totalMemory();
        long usedMemory = totalMemory - freeMemoery;
        long maxMemory = runtime.maxMemory();
        long useableMemory = maxMemory - totalMemory + freeMemoery;
        model.addAttribute("user", loginUser.getUser());
        model.addAttribute("redirectUrl", loginUser.getRedirectUrl());
        model.addAttribute("menus", loginUser.getMenus(sysInfo.getSysId()));
        model.addAttribute("sysInfo", CacheCoreUtil.getSysInfo(request));
        model.addAttribute("sysList", sysList);
        model.addAttribute("props", props);
        model.addAttribute("webContent", serverInfo);
        model.addAttribute("freeMemoery", freeMemoery);
        model.addAttribute("totalMemory", totalMemory);
        model.addAttribute("usedMemory", usedMemory);
        model.addAttribute("useableMemory", useableMemory);
        model.addAttribute("maxMemory", maxMemory);
        model.addAttribute("cacheObjectSize", CacheUtil.getObjectSize());
        model.addAttribute("cacheMemorySize", CacheUtil.getCacheMemorySize());
        return "index";
    }
    
    @RequestMapping(value = "app.do", method = RequestMethod.GET)
    public String appPage(){
    	return "app";
    }
    
    @RequestMapping(value = "header.do", method = RequestMethod.GET)
    public String header(){
    	return "blocks/header";
    }
    
    @RequestMapping(value = "aside.do", method = RequestMethod.GET)
    public String aside(){
    	return "blocks/aside";
    }
    
    @RequestMapping(value = "settings.do", method = RequestMethod.GET)
    public String settings(){
    	return "blocks/settings";
    }
    
    @RequestMapping(value = "nav.do", method = RequestMethod.GET)
    public String nav(){
    	return "blocks/nav";
    }
    
    @RequestMapping(value = "dashboard.do", method = RequestMethod.GET)
    public String dashboard(){
    	return "dashboard";
    }
    
}

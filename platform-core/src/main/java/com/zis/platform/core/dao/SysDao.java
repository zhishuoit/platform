package com.zis.platform.core.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zis.common.SearchParams;
import com.zis.platform.common.dao.BaseDao;
import com.zis.platform.core.entity.SysEntity;
@Repository
public interface SysDao extends BaseDao<SysEntity, String>
{
    public List<SysEntity> findAll();
    
    public List<SysEntity> findAllSysAndRes();
    
    public List<SysEntity> findByCondition(SearchParams params);
    
    public SysEntity findByContentPath(@Param(value = "contentPath") String contentPath);
}
package com.zis.platform.core.entity.base;

import java.io.Serializable;

public class DataAuthRoleBase implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 8453217109883801636L;
    
    private String roleDataAuthId;
    
    private String roleId;
    
    private String tableName;
    
    private String authorityScope;
    
    public String getRoleDataAuthId()
    {
        return roleDataAuthId;
    }
    
    public void setRoleDataAuthId(String roleDataAuthId)
    {
        this.roleDataAuthId = roleDataAuthId;
    }
    
    public String getRoleId()
    {
        return roleId;
    }
    
    public void setRoleId(String roleId)
    {
        this.roleId = roleId;
    }
    
    public String getTableName()
    {
        return tableName;
    }
    
    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }
    
    public String getAuthorityScope()
    {
        return authorityScope;
    }
    
    public void setAuthorityScope(String authorityScope)
    {
        this.authorityScope = authorityScope;
    }
    
}

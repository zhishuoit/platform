package com.zis.platform.core.dao;

import java.util.List;

import com.zis.common.SearchParams;
import com.zis.platform.common.dao.BaseDao;
import com.zis.platform.core.entity.ExcelTemplateEntity;

public interface ExcelTemplateDao extends BaseDao<ExcelTemplateEntity, String>
{
    List<ExcelTemplateEntity> findByCondition(SearchParams params);
    
    List<ExcelTemplateEntity> findAll();
    
    int deleteByEtIds(String[] orgIds);
    
    ExcelTemplateEntity selectByCode(String code);
    
    List<ExcelTemplateEntity> findByEtName(String etName);
    
    List<ExcelTemplateEntity> findByEtCode(String etCode);
    
}

package com.zis.platform.core.entity.base;

import java.io.Serializable;

public class ResRoleBase implements Serializable
{
    
    private String rrId;
    
    private String resId;
    
    private String roleId;
    
    public String getRrId()
    {
        return rrId;
    }
    
    public void setRrId(String rrId)
    {
        this.rrId = rrId;
    }
    
    public String getResId()
    {
        return resId;
    }
    
    public void setResId(String resId)
    {
        this.resId = resId;
    }
    
    public String getRoleId()
    {
        return roleId;
    }
    
    public void setRoleId(String roleId)
    {
        this.roleId = roleId;
    }
    
    @Override
    public String toString()
    {
        return "ResRoleBase [rrId=" + rrId + ", resId=" + resId + ", roleId=" + roleId + "]";
    }
    
}

package com.zis.platform.core.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zis.common.Pagination;
import com.zis.common.SearchParams;
import com.zis.platform.core.entity.ResEntity;
import com.zis.platform.core.entity.RoleEntity;
import com.zis.platform.core.entity.SysEntity;
import com.zis.platform.core.service.RoleService;
import com.zis.platform.core.service.SysService;

@Controller
@RequestMapping("/platform/role/")
public class RoleAction
{
    @Autowired
    RoleService roleService;
    
    @Autowired
    SysService sysService;
    
    @RequestMapping("list.do")
    public String toRoleList(HttpServletRequest request, ModelMap model, SearchParams params)
    {
        return "role/list";
    }
    
    @ResponseBody
    @RequestMapping("list.json")
    public Pagination<RoleEntity> list(SearchParams params)
        throws Exception
    {
        Pagination<RoleEntity> page = roleService.findPageByCondition(params);
        return page;
    }
    
    @RequestMapping("save.json")
    @ResponseBody
    public ModelMap add(@RequestBody
    RoleEntity role, ModelMap model)
    {
        RoleEntity roleEntity = roleService.saveOrUpdate(role);
        if (roleEntity != null)
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
    @RequestMapping("delete.json")
    @ResponseBody
    public ModelMap add(@RequestBody
    String[] roleIds, ModelMap model)
    {
        if (roleService.deleteByRoleIds(roleIds) > 0)
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
    @RequestMapping("res_setting.json")
    @ResponseBody
    public List<SysEntity> resSetting(String roleId, ModelMap model)
        throws Exception
    {
        List<SysEntity> list = this.sysService.findAll();
        return list;
    }
    
    @RequestMapping("save_res_setting.json")
    @ResponseBody
    public ModelMap resSetting(@RequestBody
    RoleEntity role, ModelMap model)
        throws Exception
    {
        int n = this.roleService.saveResSetting(role);
        model.put("state", n > 0);
        return model;
    }
    
    @RequestMapping("init_res_setting.json")
    @ResponseBody
    public List<ResEntity> initResSetting(@RequestBody
    String roleId, ModelMap model)
        throws Exception
    {
        RoleEntity role = this.roleService.findRoleAndResByRoleId(roleId);
        return role.getResList();
    }
}

package com.zis.platform.core.entity;

import java.io.Serializable;

import com.zis.platform.core.entity.base.DeptBase;

public class DeptEntity extends DeptBase implements Serializable
{
    private String orgName;
    
    public String getOrgName()
    {
        return orgName;
    }
    
    public void setOrgName(String orgName)
    {
        this.orgName = orgName;
    }
    
}

package com.zis.platform.core.service;

import com.zis.platform.common.service.BaseService;
import com.zis.platform.core.entity.SysEntity;

public interface SysService extends BaseService<SysEntity>
{
    public SysEntity findBySysId(String sysId);
    
    public SysEntity findByContentPath(String contentPath);
    
    public SysEntity saveOrUpdate(SysEntity sys);
    
    public int deleteBySysIds(String[] sysIds);
    
    public int update(SysEntity[] systems);
}

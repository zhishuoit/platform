package com.zis.platform.core.entity.base;

import java.io.Serializable;

public class DataAuthGroupBase implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 6853496648291600080L;
    
    private String groupDataAuthId;
    
    public String getGroupDataAuthId()
    {
        return groupDataAuthId;
    }
    
    public void setGroupDataAuthId(String groupDataAuthId)
    {
        this.groupDataAuthId = groupDataAuthId;
    }
    
    public String getGroupId()
    {
        return groupId;
    }
    
    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }
    
    private String groupId;
    
    private String tableName;
    
    private String authorityScope;
    
    public String getTableName()
    {
        return tableName;
    }
    
    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }
    
    public String getAuthorityScope()
    {
        return authorityScope;
    }
    
    public void setAuthorityScope(String authorityScope)
    {
        this.authorityScope = authorityScope;
    }
    
}

package com.zis.platform.core.service;

import com.zis.platform.common.service.BaseService;
import com.zis.platform.core.entity.RoleEntity;

public interface RoleService extends BaseService<RoleEntity>
{
    
    RoleEntity saveOrUpdate(RoleEntity role);
    
    int deleteByRoleIds(String[] roleIds);
    
    int saveResSetting(RoleEntity role)
        throws Exception;
    
    RoleEntity findRoleAndResByRoleId(String roleId);
    
}

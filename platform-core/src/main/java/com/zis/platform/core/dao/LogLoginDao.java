package com.zis.platform.core.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.zis.common.SearchParams;
import com.zis.platform.common.dao.BaseDao;
import com.zis.platform.core.entity.LogLoginEntity;

@Repository
public interface LogLoginDao extends BaseDao<LogLoginEntity, String>
{
    public List<LogLoginEntity> findAll();
    
    public List<LogLoginEntity> findByCondition(SearchParams params);
}

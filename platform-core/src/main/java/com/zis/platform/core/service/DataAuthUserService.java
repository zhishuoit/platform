package com.zis.platform.core.service;

import java.util.List;
import java.util.Map;

import com.zis.platform.common.service.BaseService;
import com.zis.platform.core.entity.DataAuthUserEntity;

/**
 * 
 * <b>说明：</b>用户数据权限分配业务接口
 * 
 * @ClassName: DataAuthUserService
 * @author zhaohaitao(2543)
 * @date 2015-7-10 上午10:36:08
 * 
 */
public interface DataAuthUserService extends BaseService<DataAuthUserEntity>
{
    /**
     * <b>说明：</b>插入用户数据权限 <br>
     * <b><font color="#FF0000">注意：</font>这个方法是提供给其他业务开发过程中来管理用户的数据权限的</b>
     * 
     * @param userId 用户ID
     * @param userName 用户名
     * @param tableName 需要鉴权的数据库表名
     * @param authCode 权限值
     * @return
     */
    public boolean saveUserDataAuth(String userId, String userName, String tableName, String authCode);
    
    /**
     * <b>说明：</b>删除用户数据权限 <br>
     * <b><font color="#FF0000">注意：</font>这个方法是提供给其他业务开发过程中来管理用户的数据权限的</b>
     * 
     * @Title: deleteUserDataAuth
     * @param @param userId
     * @param @param tableName
     * @param @param authCode
     * @return boolean 返回类型
     * @throws
     */
    public boolean deleteUserDataAuth(String userId, String tableName, String authCode);
    
    public boolean saveOrUpdate(DataAuthUserEntity record);
    
    public Map<String, List<DataAuthUserEntity>> findByUserId(String userId);
    
    public int deleteByUserDataAuthIds(String[] userDataAuthIds);
    
}

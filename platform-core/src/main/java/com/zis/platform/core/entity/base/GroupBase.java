package com.zis.platform.core.entity.base;

import java.util.List;

import com.zis.platform.core.entity.ResEntity;

public class GroupBase
{
    private String groupId;
    
    private String groupName;
    
    private String orgId;
    
    private Boolean status;
    
    private String remarks;
    
    private List<ResEntity> resList;
    
    public String getGroupId()
    {
        return groupId;
    }
    
    public void setGroupId(String groupId)
    {
        this.groupId = groupId == null ? null : groupId.trim();
    }
    
    public String getGroupName()
    {
        return groupName;
    }
    
    public void setGroupName(String groupName)
    {
        this.groupName = groupName == null ? null : groupName.trim();
    }
    
    public String getOrgId()
    {
        return orgId;
    }
    
    public void setOrgId(String orgId)
    {
        this.orgId = orgId;
    }
    
    public Boolean getStatus()
    {
        return status;
    }
    
    public void setStatus(Boolean status)
    {
        this.status = status;
    }
    
    public String getRemarks()
    {
        return remarks;
    }
    
    public void setRemarks(String remarks)
    {
        this.remarks = remarks == null ? null : remarks.trim();
    }
    
    public List<ResEntity> getResList()
    {
        return resList;
    }
    
    public void setResList(List<ResEntity> resList)
    {
        this.resList = resList;
    }
    
    @Override
    public String toString()
    {
        return "GroupBase [groupId=" + groupId + ", groupName=" + groupName + ", orgId=" + orgId + ", status=" + status
            + ", remarks=" + remarks + ", resList=" + resList + "]";
    }
    
}
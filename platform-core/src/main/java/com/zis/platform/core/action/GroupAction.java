package com.zis.platform.core.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zis.common.Pagination;
import com.zis.common.SearchParams;
import com.zis.platform.common.web.TreeNode;
import com.zis.platform.core.entity.GroupEntity;
import com.zis.platform.core.entity.ResEntity;
import com.zis.platform.core.entity.SysEntity;
import com.zis.platform.core.service.GroupService;
import com.zis.platform.core.service.OrgService;
import com.zis.platform.core.service.SysService;

/**
 * 用户组管理 action
 * 
 * @author wangjun
 * 
 */
@Controller
@RequestMapping("/platform/group")
public class GroupAction
{
    
    @Autowired
    GroupService groupService;
    
    @Autowired
    SysService sysService;
    
    @Autowired
    private OrgService orgService;
    
    /**
     * 用户组列表
     * 
     * @param request
     * @param model
     * @param params
     * @return 列表url
     */
    @RequestMapping("/list.do")
    public String toList(HttpServletRequest request, ModelMap model, SearchParams params)
    {
        return "group/list";
    }
    
    @ResponseBody
    @RequestMapping("/list.json")
    public Pagination<GroupEntity> list(SearchParams params)
        throws Exception
    {
        Pagination<GroupEntity> page = groupService.findPageByCondition(params);
        return page;
    }
    
    @RequestMapping("/save.json")
    @ResponseBody
    public ModelMap add(@RequestBody
    GroupEntity group, ModelMap model)
    {
        GroupEntity groupEntity = groupService.saveOrUpdate(group);
        if (groupEntity != null)
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
    @ResponseBody
    @RequestMapping("list_orgTree.json")
    public List<TreeNode> list_orgTree(String id)
        throws Exception
    {
        return orgService.findRoot();
    }
    
    @RequestMapping("/delete.json")
    @ResponseBody
    public ModelMap add(@RequestBody
    String[] groupIds, ModelMap model)
    {
        if (groupService.deleteByGroupIds(groupIds) > 0)
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
    @RequestMapping("res_setting.json")
    @ResponseBody
    public List<SysEntity> resSetting(String roleId, ModelMap model)
        throws Exception
    {
        List<SysEntity> list = this.sysService.findAll();
        return list;
    }
    
    @RequestMapping("init_res_setting.json")
    @ResponseBody
    public List<ResEntity> initResSetting(@RequestBody
    String groupId, ModelMap model)
        throws Exception
    {
        GroupEntity group = this.groupService.findGroupAndResByGroupId(groupId);
        return group.getResList();
    }
    
    @RequestMapping("save_res_setting.json")
    @ResponseBody
    public ModelMap resSetting(@RequestBody
    GroupEntity group, ModelMap model)
        throws Exception
    {
        int n = this.groupService.saveResSetting(group);
        model.put("state", n > 0);
        return model;
    }
}

package com.zis.platform.core.service.impl;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zis.common.SearchParams;
import com.zis.platform.common.service.impl.BaseServiceImpl;
import com.zis.platform.core.dao.SysDao;
import com.zis.platform.core.entity.SysEntity;
import com.zis.platform.core.service.SysService;

@Service(value = "sysService")
public class SysServiceImpl extends BaseServiceImpl<SysEntity> implements SysService
{
    @Autowired
    private SysDao dao;
    
    @Override
    public List<SysEntity> findAll()
    {
        return dao.findAll();
    }
    
    @Override
    public List<SysEntity> findByCondition(SearchParams params)
        throws Exception
    {
        return dao.findByCondition(params);
    }
    
    @Override
    public SysEntity findByContentPath(String sysName)
    {
        if (StringUtils.isBlank(sysName))
        {
            return null;
        }
        return dao.findByContentPath(sysName.trim());
    }
    
    @Override
    public SysEntity saveOrUpdate(SysEntity sys)
    {
        if (sys != null)
        {
            String contentPath = sys.getContentPath();
            sys.setLoginUrl("/" + contentPath + "/login.do");
            sys.setLogoutUrl("/" + contentPath + "/logout.do");
            sys.setWelcomeUrl("/" + contentPath + "/index.do");
            sys.setTemplateFloder(contentPath);
            if (StringUtils.isBlank(sys.getSysId()))
            {
                sys.setSysId(UUID.randomUUID().toString());
                int insertNum = dao.insertSelective(sys);
                if (insertNum > 0)
                {
                    return sys;
                }
            }
            else
            {
                int updateNum = dao.updateByPrimaryKeySelective(sys);
                if (updateNum > 0)
                {
                    return sys;
                }
            }
        }
        return null;
    }
    
    @Override
    public int deleteBySysIds(String[] sysIds)
    {
        int num = 0;
        if (sysIds != null && sysIds.length > 0)
        {
            for (String sysId : sysIds)
            {
                num += dao.deleteByPrimaryKey(sysId);
            }
        }
        return num;
    }
    
    @Override
    public int update(SysEntity[] systems)
    {
        int num = 0;
        if (systems != null && systems.length > 0)
        {
            for (SysEntity sys : systems)
            {
                num += dao.updateByPrimaryKeySelective(sys);
            }
        }
        return num;
    }
    
    @Override
    public SysEntity findBySysId(String sysId)
    {
        if (StringUtils.isBlank(sysId))
        {
            return null;
        }
        return dao.selectByPrimaryKey(sysId);
    }
    
}

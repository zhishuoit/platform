package com.zis.platform.core.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zis.common.Pagination;
import com.zis.common.SearchParams;
import com.zis.platform.common.web.TreeNode;
import com.zis.platform.core.entity.OrgEntity;
import com.zis.platform.core.service.OrgService;

@Controller
@RequestMapping(value = "/platform/org/")
public class OrgAction
{
    
    @Autowired
    private OrgService orgService;
    
    @RequestMapping(value = "list.do")
    public String list()
    {
        return "org/list";
    }
    
    @ResponseBody
    @RequestMapping("list.json")
    public Pagination<OrgEntity> list(SearchParams params)
        throws Exception
    {
        Pagination<OrgEntity> page = orgService.findPageByCondition(params);
        return page;
    }
    
    @ResponseBody
    @RequestMapping("listRoot.json")
    public List<TreeNode> listRootNode()
        throws Exception
    {
        return orgService.findRoot();
    }
    
    @RequestMapping("save.json")
    @ResponseBody
    public ModelMap add(@RequestBody
    OrgEntity org, ModelMap model)
    {
        OrgEntity orgEntity = orgService.saveOrUpdate(org);
        if (orgEntity != null)
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
    
    @RequestMapping("delete.json")
    @ResponseBody
    public ModelMap add(@RequestBody
    String[] orgIds, ModelMap model)
    {
        if (orgService.deleteByOrgIds(orgIds) > 0)
        {
            model.put("state", true);
        }
        else
        {
            model.put("state", false);
        }
        return model;
    }
}

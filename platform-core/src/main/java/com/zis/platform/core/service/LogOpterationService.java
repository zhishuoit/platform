package com.zis.platform.core.service;

import com.zis.platform.common.service.BaseService;
import com.zis.platform.core.entity.LogOpterationEntity;

public interface LogOpterationService extends BaseService<LogOpterationEntity>
{
    
}

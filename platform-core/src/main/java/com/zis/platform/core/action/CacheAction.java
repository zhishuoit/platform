package com.zis.platform.core.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zis.platform.common.util.CacheCoreUtil;
import com.zis.platform.core.service.DataAuthStrategyService;
import com.zis.platform.core.service.DictionaryTypeService;
import com.zis.platform.core.service.SysService;

@RequestMapping(value = "/platform/cache/")
public class CacheAction
{
    @Autowired
    private SysService sysService;
    
    @Autowired
    private DictionaryTypeService dicTypeService;
    
    @Autowired
    private DataAuthStrategyService dataAuthStratgyService;
    
    @ResponseBody
    @RequestMapping(value = "refresh_sysinfo_cache.json", method = RequestMethod.POST)
    public ModelMap refreshSysInfoCache(ModelMap model)
    {
        try
        {
            CacheCoreUtil.refreshSysInfo(sysService.findAll());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        model.put("state", true);
        return model;
    }
    
    /**
     * 刷新字典缓存
     * 
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "refresh_dictionary_cache.json", method = RequestMethod.POST)
    public ModelMap refreshDictionaryType(ModelMap model)
    {
        try
        {
            CacheCoreUtil.refreshDictionaryType(dicTypeService.findAllTypeAndDics());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        model.put("state", true);
        return model;
    }
    
    /**
     * 刷新数据权限策略缓存
     * 
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "refresh_strategy_cache.json", method = RequestMethod.POST)
    public ModelMap refreshStrategy(ModelMap model)
    {
        try
        {
            CacheCoreUtil.refreshDataAuthStrategyType(dataAuthStratgyService.findAll());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        model.put("state", true);
        return model;
    }
    
}

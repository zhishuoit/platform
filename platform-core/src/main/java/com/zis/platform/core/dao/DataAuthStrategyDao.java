package com.zis.platform.core.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.zis.common.SearchParams;
import com.zis.platform.common.dao.BaseDao;
import com.zis.platform.core.entity.DataAuthStrategyEntity;

@Repository
public interface DataAuthStrategyDao extends BaseDao<DataAuthStrategyEntity, String>
{
    public List<DataAuthStrategyEntity> findAll();
    
    public List<DataAuthStrategyEntity> findByCondition(SearchParams params);
    
    public int deleteByStrategyIds(String[] strategyIds);
    
}
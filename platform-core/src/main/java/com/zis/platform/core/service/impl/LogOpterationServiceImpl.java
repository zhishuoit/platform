package com.zis.platform.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zis.common.SearchParams;
import com.zis.platform.common.service.impl.BaseServiceImpl;
import com.zis.platform.core.dao.LogOpterationDao;
import com.zis.platform.core.entity.LogOpterationEntity;
import com.zis.platform.core.service.LogOpterationService;

@Service
public class LogOpterationServiceImpl extends BaseServiceImpl<LogOpterationEntity> implements LogOpterationService
{
    @Autowired
    private LogOpterationDao dao;
    
    @Override
    public List<LogOpterationEntity> findByCondition(SearchParams params)
        throws Exception
    {
        return dao.findByCondition(params);
    }
    
    @Override
    public List<LogOpterationEntity> findAll()
        throws Exception
    {
        return dao.findAll();
    }
    
}

package com.zis.platform.core.dao;

import java.util.List;

import com.zis.common.SearchParams;
import com.zis.platform.core.entity.DataAuthGroupEntity;

public interface DataAuthGroupDao
{
    
    List<DataAuthGroupEntity> findByCondition(SearchParams params);
    
    List<DataAuthGroupEntity> findAll();
    
    int insert(DataAuthGroupEntity entity);
    
    int updateByPrimaryKeySelective(DataAuthGroupEntity entity);
    
    int deleteByGroupDataAuthIds(String[] groupDataAuthId);
    
    public List<DataAuthGroupEntity> findByGroupId(String groupId);
    
    int isExist(DataAuthGroupEntity entity);
}

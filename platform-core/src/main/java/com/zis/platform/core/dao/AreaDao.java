package com.zis.platform.core.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.zis.platform.core.entity.AreaEntity;

@Repository
public interface AreaDao
{
    
    AreaEntity selectByPrimaryKey(String id);
    
    int updateByPrimaryKeySelective(AreaEntity record);
    
    int updateByPrimaryKey(AreaEntity record);
    
    public List<AreaEntity> findAll();
    
    public List<AreaEntity> selectByParentId(String parentCode);
}

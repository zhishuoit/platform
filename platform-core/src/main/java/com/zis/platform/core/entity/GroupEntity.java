package com.zis.platform.core.entity;

import java.io.Serializable;

import com.zis.platform.common.util.CacheCoreUtil;
import com.zis.platform.core.entity.base.GroupBase;

public class GroupEntity extends GroupBase implements Serializable
{
    
    private String orgName;
    
    public Short getStatusBit()
    {
        return (this.getStatus() != null && this.getStatus()) ? (short)1 : 0;
    }
    
    /**
     * 返回 true false的字典名称
     * 
     * @return
     */
    public String getStatusName()
    {
        if (getStatus() != null)
        {
            DictionaryEntity dictionary = CacheCoreUtil.getDictionaryByValue(String.valueOf(getStatus()));
            if (dictionary != null)
            {
                return dictionary.getDictName();
            }
        }
        return null;
    }
    
    public String getOrgName()
    {
        return orgName;
    }
    
    public void setOrgName(String orgName)
    {
        this.orgName = orgName;
    }
    
}

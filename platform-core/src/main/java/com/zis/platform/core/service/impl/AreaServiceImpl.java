package com.zis.platform.core.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zis.common.SearchParams;
import com.zis.platform.common.service.impl.BaseServiceImpl;
import com.zis.platform.core.dao.AreaDao;
import com.zis.platform.core.entity.AreaEntity;
import com.zis.platform.core.service.AreaService;

@Service
public class AreaServiceImpl extends BaseServiceImpl<AreaEntity> implements AreaService
{
    @Autowired
    private AreaDao dao;
    
    @Override
    public List<AreaEntity> findByCondition(SearchParams params)
        throws Exception
    {
        return null;
    }
    
    @Override
    public List<AreaEntity> findAll()
        throws Exception
    {
        return dao.findAll();
    }
    
    @Override
    public AreaEntity findByAreaCode(String areaCode)
    {
        areaCode = StringUtils.isEmpty(areaCode) ? "0" : areaCode;
        return dao.selectByPrimaryKey(areaCode);
    }
    
    @Override
    public List<AreaEntity> findByParentAreaCode(String parentAreaCode)
    {
        return dao.selectByParentId(parentAreaCode);
    }
}

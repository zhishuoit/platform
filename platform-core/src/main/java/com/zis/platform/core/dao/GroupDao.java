package com.zis.platform.core.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.zis.common.SearchParams;
import com.zis.platform.common.dao.BaseDao;
import com.zis.platform.core.entity.GroupEntity;

@Repository
public interface GroupDao extends BaseDao<GroupEntity, String>
{
    
    List<GroupEntity> findByCondition(SearchParams params);
    
    List<GroupEntity> findAll();
    
    int deleteByGroupIds(String[] groupIds);
    
    GroupEntity findGroupAndResByGroupId(String groupId);
}
package com.zis.platform.core.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.zis.common.SearchParams;
import com.zis.platform.common.dao.BaseDao;
import com.zis.platform.core.entity.RoleEntity;

@Repository
public interface RoleDao extends BaseDao<RoleEntity, String>
{
    
    List<RoleEntity> findByCondition(SearchParams params);
    
    List<RoleEntity> findAll();
    
    int deleteByRoleIds(String[] roleIds);
    
    RoleEntity findRoleAndResByRoleId(String roleId);
    
    public List<RoleEntity> findRolesByUserId(String userId);
}
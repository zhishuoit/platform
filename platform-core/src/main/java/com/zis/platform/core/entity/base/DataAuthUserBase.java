package com.zis.platform.core.entity.base;

import java.io.Serializable;

public class DataAuthUserBase implements Serializable
{
    private static final long serialVersionUID = 1932137940920131481L;
    
    private String userDataAuthId;
    
    private String userId;
    
    private String tableName;
    
    private String authorityScope;
    
    public String getUserDataAuthId()
    {
        return userDataAuthId;
    }
    
    public void setUserDataAuthId(String userDataAuthId)
    {
        this.userDataAuthId = userDataAuthId == null ? null : userDataAuthId.trim();
    }
    
    public String getUserId()
    {
        return userId;
    }
    
    public void setUserId(String userId)
    {
        this.userId = userId == null ? null : userId.trim();
    }
    
    public String getTableName()
    {
        return tableName;
    }
    
    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }
    
    public String getAuthorityScope()
    {
        return authorityScope;
    }
    
    public void setAuthorityScope(String authorityScope)
    {
        this.authorityScope = authorityScope == null ? null : authorityScope.trim();
    }
}
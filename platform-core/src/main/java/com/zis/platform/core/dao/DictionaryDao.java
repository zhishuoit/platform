package com.zis.platform.core.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.zis.common.SearchParams;
import com.zis.platform.common.dao.BaseDao;
import com.zis.platform.core.entity.DictionaryEntity;

@Repository
public interface DictionaryDao extends BaseDao<DictionaryEntity, String>
{
    public List<DictionaryEntity> findByTypeCode(String typeCode);
    
    public List<DictionaryEntity> findByDicValue(String dicValue);
    
    public List<DictionaryEntity> findByCondition(SearchParams params);
    
    public List<DictionaryEntity> findAll();
    
}
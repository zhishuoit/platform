package com.zis.platform.core.action;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zis.common.Pagination;
import com.zis.common.SearchParams;
import com.zis.platform.core.entity.LogLoginEntity;
import com.zis.platform.core.entity.LogOpterationEntity;
import com.zis.platform.core.service.LogLoginService;
import com.zis.platform.core.service.LogOpterationService;

@Controller
@RequestMapping("/platform/log/")
public class LogLoginAction
{
    
    @Autowired
    private LogLoginService service;
    
    @Autowired
    private LogOpterationService optService;
    
    @RequestMapping("login_list.do")
    public String loginList()
    {
        return "log/login_list";
    }
    
    @RequestMapping("opt_list.do")
    public String optList()
    {
        return "log/opt_list";
    }
    
    @ResponseBody
    @RequestMapping("login_list.json")
    public Pagination<LogLoginEntity> loginList(SearchParams params)
    {
        try
        {
            return service.findPageByCondition(params);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    @ResponseBody
    @RequestMapping("opt_list.json")
    public Pagination<LogOpterationEntity> optList(SearchParams params)
    {
        try
        {
            return optService.findPageByCondition(params);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    @RequestMapping("log_collection.do")
    public String logCollection()
    {
        return "log/log_collection";
    }
    
    @ResponseBody
    @RequestMapping("log_collection.json")
    public String logCollection(HttpServletRequest request, String type, String startDate, String endDate)
    {
        service.parseLogFromFile(type, startDate, endDate);
        return "log/log_collection";
    }
    
}

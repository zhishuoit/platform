package com.zis.platform.core.action;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.Properties;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zis.Constants;

/**
 * 生成图片验证码
 * 
 * @author zhaohaitao URL : /common/code_image.do
 * 
 */
@Controller
@RequestMapping("")
public class ImageCodeAction
{
    private String properties = "/image-code.properties";
    
    // 线条
    private String TYPE_LINE = "LINE";
    
    // 噪点
    private String TYPE_HOT = "HOT PIXEL";
    
    private int width = 70;// 定义图片的width
    
    private int height = 28;// 定义图片的height
    
    private int codeCount = 4;// 定义图片上显示验证码的个数
    
    private int backdrop = 20; // 干扰线(噪点)的数量
    
    private int xx = 10;
    
    private int fontHeight = 18;
    
    private int codeY = 22;
    
    String[] codeSequence = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
        "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    
    @RequestMapping("common/code_image.do")
    public void getCode(HttpServletRequest req, HttpServletResponse resp)
        throws Exception
    {
        InputStream in = getClass().getResourceAsStream(properties);
        Properties prop = new Properties();
        prop.load(in);
        String imageCodeType = prop.getProperty("imageCodeType");
        if (imageCodeType == null || "".equals(imageCodeType))
        {
            throw new Exception("请配置验证码噪点类型参数：【imageCodeType】");
        }
        width = Integer.parseInt(prop.getProperty("width"));
        height = Integer.parseInt(prop.getProperty("height"));
        backdrop = Integer.parseInt(prop.getProperty("backdrop"));
        fontHeight = Integer.parseInt(prop.getProperty("fontHeight"));
        // 定义图像buffer
        BufferedImage buffImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        // Graphics2D gd = buffImg.createGraphics();
        // Graphics2D gd = (Graphics2D) buffImg.getGraphics();
        Graphics gd = buffImg.getGraphics();
        // 创建一个随机数生成器类
        Random random = new Random();
        // 将图像填充为白色
        gd.setColor(Color.WHITE);
        gd.fillRect(0, 0, width, height);
        
        // 创建字体，字体的大小应该根据图片的高度来定。
        Font font = new Font("Fixedsys", Font.BOLD, fontHeight);
        // 设置字体。
        gd.setFont(font);
        
        // 画边框。
        gd.setColor(Color.BLACK);
        gd.drawRect(0, 0, width - 1, height - 1);
        
        // 随机产生40条干扰线，使图象中的认证码不易被其它程序探测到。
        gd.setColor(Color.BLACK);
        for (int i = 0; i < backdrop; i++)
        {
            if (TYPE_LINE.equals(imageCodeType.toUpperCase()))
            {
                int x = random.nextInt(width);
                int y = random.nextInt(height);
                int xl = random.nextInt(12);
                int yl = random.nextInt(12);
                gd.drawLine(x, y, x + xl, y + yl);
            }
            else
            {
                int x = (int)(Math.random() * width);
                int y = (int)(Math.random() * height);
                int red = (int)(Math.random() * 255);
                int green = (int)(Math.random() * 255);
                int blue = (int)(Math.random() * 255);
                gd.setColor(new Color(red, green, blue));
                gd.drawOval(x, y, 1, 0);
            }
        }
        
        // randomCode用于保存随机产生的验证码，以便用户登录后进行验证。
        StringBuffer randomCode = new StringBuffer();
        int red = 0, green = 0, blue = 0;
        String codeStr = prop.getProperty("code");
        if (codeStr != null && !"".equals(codeStr))
        {
            codeSequence = codeStr.split(",");
        }
        // 随机产生codeCount数字的验证码。
        for (int i = 0; i < codeCount; i++)
        {
            // 得到随机产生的验证码数字。
            String code = String.valueOf(codeSequence[random.nextInt(codeSequence.length)]);
            
            // 产生随机的颜色分量来构造颜色值，这样输出的每位数字的颜色值都将不同。
            String redStr = prop.getProperty("red" + i);
            if ("".equals(redStr) || redStr == null)
            {
                red = random.nextInt(255);
            }
            else
            {
                red = Integer.parseInt(redStr);
            }
            String greenStr = prop.getProperty("green" + i);
            if ("".equals(greenStr) || greenStr == null)
            {
                green = random.nextInt(255);
            }
            else
            {
                green = Integer.parseInt(greenStr);
            }
            String blueStr = prop.getProperty("blue" + i);
            if ("".equals(blueStr) || blueStr == null)
            {
                blue = random.nextInt(255);
            }
            else
            {
                blue = Integer.parseInt(blueStr);
            }
            // 用随机产生的颜色将验证码绘制到图像中。
            gd.setColor(new Color(red, green, blue));
            gd.drawString(code, (i + 1) * xx, codeY);
            
            // 将产生的四个随机数组合在一起。
            randomCode.append(code);
        }
        // 将四位数字的验证码保存到Session中。
        HttpSession session = req.getSession();
        session.setAttribute(Constants.KEY_CODE_IMAGE, randomCode.toString());
        
        // 禁止图像缓存。
        resp.setHeader("Pragma", "no-cache");
        resp.setHeader("Cache-Control", "no-cache");
        resp.setDateHeader("Expires", 0);
        resp.setContentType("image/jpeg");
        
        // 将图像输出到Servlet输出流中。
        ServletOutputStream sos = resp.getOutputStream();
        ImageIO.write(buffImg, "jpeg", sos);
        sos.close();
    }
}

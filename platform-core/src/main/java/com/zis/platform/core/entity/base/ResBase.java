package com.zis.platform.core.entity.base;

import java.util.List;

import com.zis.platform.core.entity.ResEntity;

public class ResBase
{
    private String resId;
    
    private String sysId;
    
    private String resName;
    
    private String remarks;
    
    private String resType;
    
    private String resUrl;
    
    private String baseResUrl;
    
    private String resIcon;
    
    private Integer orderNum;
    
    private String parentId;
    
    private Boolean isEnabled;
    
    private String resTooltip;
    
    protected List<ResEntity> children;
    
    public String getResId()
    {
        return resId;
    }
    
    public void setResId(String resId)
    {
        this.resId = resId == null ? null : resId.trim();
    }
    
    public String getSysId()
    {
        return sysId;
    }
    
    public void setSysId(String sysId)
    {
        this.sysId = sysId == null ? null : sysId.trim();
    }
    
    public String getResName()
    {
        return resName;
    }
    
    public void setResName(String resName)
    {
        this.resName = resName == null ? null : resName.trim();
    }
    
    public String getRemarks()
    {
        return remarks;
    }
    
    public void setRemarks(String remarks)
    {
        this.remarks = remarks == null ? null : remarks.trim();
    }
    
    public String getResType()
    {
        return resType;
    }
    
    public void setResType(String resType)
    {
        this.resType = resType == null ? null : resType.trim();
    }
    
    public String getResUrl()
    {
        return resUrl;
    }
    
    public void setResUrl(String resUrl)
    {
        this.resUrl = resUrl == null ? null : resUrl.trim();
    }
    
    public String getResIcon()
    {
        return resIcon;
    }
    
    public void setResIcon(String resIcon)
    {
        this.resIcon = resIcon == null ? null : resIcon.trim();
    }
    
    public Integer getOrderNum()
    {
        return orderNum;
    }
    
    public void setOrderNum(Integer orderNum)
    {
        this.orderNum = orderNum;
    }
    
    public String getParentId()
    {
        return parentId;
    }
    
    public void setParentId(String parentId)
    {
        this.parentId = parentId == null ? null : parentId.trim();
    }
    
    public Boolean getIsEnabled()
    {
        return isEnabled;
    }
    
    public void setIsEnabled(Boolean isEnabled)
    {
        this.isEnabled = isEnabled;
    }
    
    public String getResTooltip()
    {
        return resTooltip;
    }
    
    public void setResTooltip(String resTooltip)
    {
        this.resTooltip = resTooltip == null ? null : resTooltip.trim();
    }
    
    public List<ResEntity> getChildren()
    {
        return children;
    }
    
    public void setChildren(List<ResEntity> children)
    {
        this.children = children;
    }
    
    public String getBaseResUrl()
    {
        return baseResUrl;
    }
    
    public void setBaseResUrl(String baseResUrl)
    {
        this.baseResUrl = baseResUrl;
    }
    
    @Override
    public String toString()
    {
        return "ResBase [resId=" + resId + ", sysId=" + sysId + ", resName=" + resName + ", remarks=" + remarks
            + ", resType=" + resType + ", resUrl=" + resUrl + ", resIcon=" + resIcon + ", orderNum=" + orderNum
            + ", parentId=" + parentId + ", isEnabled=" + isEnabled + ", resTooltip=" + resTooltip + ", children="
            + children + "]";
    }
    
}
package com.zis.platform.core.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.zis.common.SearchParams;
import com.zis.platform.common.service.impl.BaseServiceImpl;
import com.zis.platform.core.dao.DataAuthRoleDao;
import com.zis.platform.core.entity.DataAuthRoleEntity;
import com.zis.platform.core.entity.DataAuthUserEntity;
import com.zis.platform.core.entity.UserEntity;
import com.zis.platform.core.service.DataAuthRoleService;

@Service
public class DataAuthRoleServiceImpl extends BaseServiceImpl<DataAuthRoleEntity> implements DataAuthRoleService
{
    
    @Autowired
    private DataAuthRoleDao dao;
    
    @Override
    public List<DataAuthRoleEntity> findByCondition(SearchParams params)
        throws Exception
    {
        return dao.findByCondition(params);
    }
    
    @Override
    public List<DataAuthRoleEntity> findAll()
        throws Exception
    {
        return dao.findAll();
    }
    
    @Override
    public boolean saveOrUpdate(DataAuthRoleEntity entity)
    {
        int num = 0;
        if (StringUtils.isEmpty(entity.getRoleDataAuthId()))
        {
            // 检查是否已存在
            if (dao.isExist(entity) > 0)
            {
                return false;
            }
            entity.setRoleDataAuthId(UUID.randomUUID().toString());
            num = dao.insert(entity);
        }
        else
        {
            num = dao.updateByPrimaryKeySelective(entity);
        }
        if (num > 0)
        {
            return true;
        }
        return false;
    }
    
    @Override
    public int deleteByRoleDataAuthIds(String[] roleDataAuthIds)
    {
        return dao.deleteByRoleDataAuthIds(roleDataAuthIds);
    }
    
    @Override
    public Map<String, List<DataAuthUserEntity>> findByRoleId(String roleId, UserEntity user)
    {
        List<DataAuthRoleEntity> roleDataAuths = dao.findByRoleId(roleId);
        Map<String, List<DataAuthUserEntity>> dataMaps = new HashMap<String, List<DataAuthUserEntity>>();
        for (DataAuthRoleEntity dataAuth : roleDataAuths)
        {
            String tableName = dataAuth.getTableName();
            DataAuthUserEntity dau = new DataAuthUserEntity();
            List<DataAuthUserEntity> list = dataMaps.get(tableName);
            if (CollectionUtils.isEmpty(list))
            {
                list = new ArrayList<DataAuthUserEntity>();
            }
            dau.setAuthorityScope(dataAuth.getAuthorityScope());
            dau.setTableName(tableName);
            dau.setUserId(user.getUserId());
            dau.setUserName(user.getRealName());
            list.add(dau);
            dataMaps.put(tableName.toUpperCase(), list);
        }
        return dataMaps;
    }
    
}

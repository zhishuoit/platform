package com.zis.platform.core.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.zis.platform.common.dao.BaseDao;
import com.zis.platform.core.entity.DictionaryTypeEntity;

@Repository
public interface DictionaryTypeDao extends BaseDao<DictionaryTypeEntity, String>
{
    
    public List<DictionaryTypeEntity> findAll();
    
    public List<DictionaryTypeEntity> findAllTypeAndDics();
}
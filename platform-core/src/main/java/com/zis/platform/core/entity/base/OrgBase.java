package com.zis.platform.core.entity.base;

public class OrgBase
{
    private String orgId;
    
    private String orgName;
    
    // private String phone;
    //
    // private String email;
    //
    // private String webSite;
    //
    // private String orgType;
    //
    // private String orgLevel;
    //
    // private String orgGrade;
    //
    // private String areaCode;
    
    private String remarks;
    
    private String parentOrgId;
    
    private String orgType;
    
    private String authCode;
    
    public String getOrgId()
    {
        return orgId;
    }
    
    public void setOrgId(String orgId)
    {
        this.orgId = orgId == null ? null : orgId.trim();
    }
    
    public String getOrgName()
    {
        return orgName;
    }
    
    public void setOrgName(String orgName)
    {
        this.orgName = orgName == null ? null : orgName.trim();
    }
    
    // public String getPhone()
    // {
    // return phone;
    // }
    //
    // public void setPhone(String phone)
    // {
    // this.phone = phone == null ? null : phone.trim();
    // }
    //
    // public String getEmail()
    // {
    // return email;
    // }
    //
    // public void setEmail(String email)
    // {
    // this.email = email == null ? null : email.trim();
    // }
    //
    // public String getWebSite()
    // {
    // return webSite;
    // }
    //
    // public void setWebSite(String webSite)
    // {
    // this.webSite = webSite;
    // }
    //
    // public String getOrgType()
    // {
    // return orgType;
    // }
    //
    // public void setOrgType(String orgType)
    // {
    // this.orgType = orgType == null ? null : orgType.trim();
    // }
    //
    // public String getOrgLevel()
    // {
    // return orgLevel;
    // }
    //
    // public void setOrgLevel(String orgLevel)
    // {
    // this.orgLevel = orgLevel == null ? null : orgLevel.trim();
    // }
    //
    // public String getOrgGrade()
    // {
    // return orgGrade;
    // }
    //
    // public void setOrgGrade(String orgGrade)
    // {
    // this.orgGrade = orgGrade == null ? null : orgGrade.trim();
    // }
    //
    // public String getAreaCode()
    // {
    // return areaCode;
    // }
    //
    // public void setAreaCode(String areaCode)
    // {
    // this.areaCode = areaCode == null ? null : areaCode.trim();
    // }
    
    public String getRemarks()
    {
        return remarks;
    }
    
    public void setRemarks(String remarks)
    {
        this.remarks = remarks == null ? null : remarks.trim();
    }
    
    public String getParentOrgId()
    {
        return parentOrgId;
    }
    
    public void setParentOrgId(String parentOrgId)
    {
        this.parentOrgId = parentOrgId == null ? null : parentOrgId.trim();
    }
    
    public String getOrgType()
    {
        return orgType = orgType == null ? null : orgType.trim();
    }
    
    public void setOrgType(String orgType)
    {
        this.orgType = orgType;
    }
    
    public String getAuthCode()
    {
        return authCode = authCode == null ? null : authCode.trim();
    }
    
    public void setAuthCode(String authCode)
    {
        this.authCode = authCode;
    }
    
    @Override
    public String toString()
    {
        return "OrgBase [orgId=" + orgId + ", orgName=" + orgName + ", remarks=" + remarks + ", parentOrgId="
            + parentOrgId + ", orgType=" + orgType + ",authCode=" + authCode + "]";
    }
}
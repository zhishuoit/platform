package com.zis.platform.core.service;

import java.util.List;

import com.zis.platform.common.service.BaseService;
import com.zis.platform.core.entity.RoleEntity;
import com.zis.platform.core.entity.UserEntity;

public interface UserService extends BaseService<UserEntity>
{
    /**
     * 根据 登录名和用户密码 查询用户
     * 
     * @param userName
     * @param password
     * @return
     */
    public UserEntity findByLoginName(String loginName);
    
    public UserEntity saveOrUpdate(UserEntity user);
    
    /**
     * 根据用户id进行批量删除
     * 
     * @param ids
     */
    public int deleteByUserIds(String[] userIds);
    
    public UserEntity findById(String userId);
    
    public List<RoleEntity> findRolesByUserId(String userId);
    
    public boolean saveUserRoles(UserEntity user);
    
    public int enableByUserIds(String[] userIds);
    
    public int disableByUserIds(String[] userIds);
    
    public void lockUser(String userId, boolean locked);
    
    public void updateUserLoginInfo(String userId, String ip, String mac);
    
    public int unLockByUserIds(String[] userIds);
    
    /**
     * 重置用户的密码为123456
     * 
     * @param userIds
     * @return
     */
    public int resetPasswordByUserIds(String[] userIds);
    
}

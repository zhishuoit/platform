package com.zis.platform.core.entity.base;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zis.platform.core.entity.ResEntity;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SysBase
{
    private String sysId;
    
    private String sysName;
    
    private String templateFloder;
    
    private String contentPath;
    
    private String loginUrl;
    
    private String logoutUrl;
    
    private String welcomeUrl;
    
    private String remarks;
    
    private Integer orderNum;
    
    private List<ResEntity> resources;
    
    public String getSysId()
    {
        return sysId;
    }
    
    public void setSysId(String sysId)
    {
        this.sysId = sysId == null ? null : sysId.trim();
    }
    
    public String getSysName()
    {
        return sysName;
    }
    
    public void setSysName(String sysName)
    {
        this.sysName = sysName == null ? null : sysName.trim();
    }
    
    public String getContentPath()
    {
        return contentPath;
    }
    
    public void setContentPath(String contentPath)
    {
        this.contentPath = contentPath == null ? null : contentPath.trim();
    }
    
    public String getLoginUrl()
    {
        return loginUrl;
    }
    
    public void setLoginUrl(String loginUrl)
    {
        this.loginUrl = loginUrl == null ? null : loginUrl.trim();
    }
    
    public String getLogoutUrl()
    {
        return logoutUrl;
    }
    
    public void setLogoutUrl(String logoutUrl)
    {
        this.logoutUrl = logoutUrl == null ? null : logoutUrl.trim();
    }
    
    public String getRemarks()
    {
        return remarks;
    }
    
    public void setRemarks(String remarks)
    {
        this.remarks = remarks == null ? null : remarks.trim();
    }
    
    public Integer getOrderNum()
    {
        return orderNum;
    }
    
    public void setOrderNum(Integer orderNum)
    {
        this.orderNum = orderNum;
    }
    
    public String getWelcomeUrl()
    {
        return welcomeUrl;
    }
    
    public void setWelcomeUrl(String welcomeUrl)
    {
        this.welcomeUrl = welcomeUrl == null ? null : welcomeUrl.trim();
    }
    
    public String getTemplateFloder()
    {
        return templateFloder;
    }
    
    public void setTemplateFloder(String templateFloder)
    {
        this.templateFloder = templateFloder == null ? null : templateFloder.trim();
    }
    
    public List<ResEntity> getResources()
    {
        return resources;
    }
    
    public void setResources(List<ResEntity> resources)
    {
        this.resources = resources;
    }
    
    @Override
    public String toString()
    {
        return "SysBase [sysId=" + sysId + ", sysName=" + sysName + ", templateFloder=" + templateFloder
            + ", contentPath=" + contentPath + ", loginUrl=" + loginUrl + ", logoutUrl=" + logoutUrl + ", welcomeUrl="
            + welcomeUrl + ", remarks=" + remarks + ", orderNum=" + orderNum + ", resources=" + resources + "]";
    }
}
package com.zis.platform.core.entity.base;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class LogOpterationBase
{
    // 日志ID
    private String logId;
    
    // 操作人
    private String optUser;
    
    // 操作人ID
    private String optUserId;
    
    // 操作IP
    private String ip;
    
    // 操作MAC
    private String mac;
    
    // 请求URL
    private String url;
    
    // 请求参数
    private String parameters;
    
    // 业务类
    private String serviceClassName;
    
    // 业务参数
    private String methodParameters;
    
    // 异常信息
    private String exception;
    
    // 操作时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date optDate;
    
    public String getLogId()
    {
        return logId;
    }
    
    public void setLogId(String logId)
    {
        this.logId = logId;
    }
    
    public String getOptUser()
    {
        return optUser;
    }
    
    public void setOptUser(String optUser)
    {
        this.optUser = optUser;
    }
    
    public String getOptUserId()
    {
        return optUserId;
    }
    
    public void setOptUserId(String optUserId)
    {
        this.optUserId = optUserId;
    }
    
    public String getIp()
    {
        return ip;
    }
    
    public void setIp(String ip)
    {
        this.ip = ip;
    }
    
    public String getMac()
    {
        return mac;
    }
    
    public void setMac(String mac)
    {
        this.mac = mac;
    }
    
    public String getUrl()
    {
        return url;
    }
    
    public void setUrl(String url)
    {
        this.url = url;
    }
    
    public String getParameters()
    {
        return parameters;
    }
    
    public void setParameters(String parameters)
    {
        this.parameters = parameters;
    }
    
    public String getServiceClassName()
    {
        return serviceClassName;
    }
    
    public void setServiceClassName(String serviceClassName)
    {
        this.serviceClassName = serviceClassName;
    }
    
    public String getMethodParameters()
    {
        return methodParameters;
    }
    
    public void setMethodParameters(String methodParameters)
    {
        this.methodParameters = methodParameters;
    }
    
    public String getException()
    {
        return exception;
    }
    
    public void setException(String exception)
    {
        this.exception = exception;
    }
    
    public Date getOptDate()
    {
        return optDate;
    }
    
    public void setOptDate(Date optDate)
    {
        this.optDate = optDate;
    }
    
}

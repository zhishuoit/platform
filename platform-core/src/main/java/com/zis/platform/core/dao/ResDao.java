package com.zis.platform.core.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.zis.platform.common.dao.BaseDao;
import com.zis.platform.core.entity.ResEntity;

@Repository
public interface ResDao extends BaseDao<ResEntity, String>
{
    /**
     * 根据父节点ID查找菜单
     * 
     * @param parentId
     * @return
     */
    public List<ResEntity> findByParentId(@Param(value = "parentId")
    String parentId);
    
    public List<ResEntity> findAllByParentId(@Param(value = "parentId")
    String parentId);
    
    public List<ResEntity> findBySysId(@Param(value = "sysId")
    String sysId);
    
    public List<ResEntity> findAllBySysId(@Param(value = "sysId")
    String sysId);
    
    public List<ResEntity> findAllByRoleId(@Param(value = "roleId")
    String roleId);
    
    public List<ResEntity> findAllByGroupId(@Param(value = "groupId")
    String groupId);
    
    public List<ResEntity> findAll();
    
}
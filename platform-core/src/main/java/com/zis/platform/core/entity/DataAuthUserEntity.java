package com.zis.platform.core.entity;

import com.zis.platform.core.entity.base.DataAuthUserBase;

public class DataAuthUserEntity extends DataAuthUserBase
{
    
    private static final long serialVersionUID = 6436392710323975056L;
    
    private String userName;
    
    public String getUserName()
    {
        return userName;
    }
    
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
}

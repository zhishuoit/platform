package com.zis.platform.core.service;

import java.util.List;

import com.zis.platform.common.service.BaseService;
import com.zis.platform.core.entity.DictionaryTypeEntity;

public interface DictionaryTypeService extends BaseService<DictionaryTypeEntity>
{
    
    public DictionaryTypeEntity findById(String typeCode);
    
    public DictionaryTypeEntity save(DictionaryTypeEntity type);
    
    public List<DictionaryTypeEntity> findAllTypeAndDics();
    
}

package com.zis.platform.core.entity.base;

import java.util.List;

import com.zis.platform.core.entity.AreaEntity;

public class AreaBase
{
    private String areaCode;
    
    private String areaName;
    
    private String parentAreaId;
    
    private String phoneAreaCode;
    
    private Integer level;
    
    private List<AreaEntity> children;
    
    public String getAreaCode()
    {
        return areaCode;
    }
    
    public void setAreaCode(String areaCode)
    {
        this.areaCode = areaCode == null ? null : areaCode.trim();
    }
    
    public String getAreaName()
    {
        return areaName;
    }
    
    public void setAreaName(String areaName)
    {
        this.areaName = areaName == null ? null : areaName.trim();
    }
    
    public String getParentAreaId()
    {
        return parentAreaId;
    }
    
    public void setParentAreaId(String parentAreaId)
    {
        this.parentAreaId = parentAreaId == null ? null : parentAreaId.trim();
    }
    
    public String getPhoneAreaCode()
    {
        return phoneAreaCode;
    }
    
    public void setPhoneAreaCode(String phoneAreaCode)
    {
        this.phoneAreaCode = phoneAreaCode == null ? null : phoneAreaCode.trim();
    }
    
    public List<AreaEntity> getChildren()
    {
        return children;
    }
    
    public void setChildren(List<AreaEntity> children)
    {
        this.children = children;
    }
    
    public Integer getLevel()
    {
        return level;
    }
    
    public void setLevel(Integer level)
    {
        this.level = level;
    }
    
    @Override
    public String toString()
    {
        return "AreaBase [areaCode=" + areaCode + ", areaName=" + areaName + ", parentAreaId=" + parentAreaId
            + ", phoneAreaCode=" + phoneAreaCode + ", level=" + level + ", children=" + children + "]";
    }
    
}
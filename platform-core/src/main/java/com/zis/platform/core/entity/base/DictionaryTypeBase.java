package com.zis.platform.core.entity.base;

import java.util.List;

import com.zis.platform.core.entity.DictionaryEntity;

public class DictionaryTypeBase
{
    private String dictTypeCode;
    
    private String dictTypeName;
    
    private Boolean isFixed;
    
    private String remarks;
    
    private List<DictionaryEntity> dics;
    
    public DictionaryTypeBase(String dictTypeCode, String dictTypeName, String remarks)
    {
        super();
        this.dictTypeCode = dictTypeCode;
        this.dictTypeName = dictTypeName;
        this.remarks = remarks;
    }
    
    public DictionaryTypeBase()
    {
        super();
    }
    
    public String getDictTypeCode()
    {
        return dictTypeCode;
    }
    
    public void setDictTypeCode(String dictTypeCode)
    {
        this.dictTypeCode = dictTypeCode == null ? null : dictTypeCode.trim();
    }
    
    public String getDictTypeName()
    {
        return dictTypeName;
    }
    
    public void setDictTypeName(String dictTypeName)
    {
        this.dictTypeName = dictTypeName == null ? null : dictTypeName.trim();
    }
    
    public String getRemarks()
    {
        return remarks;
    }
    
    public void setRemarks(String remarks)
    {
        this.remarks = remarks == null ? null : remarks.trim();
    }
    
    public Boolean getIsFixed()
    {
        return isFixed;
    }
    
    public void setIsFixed(Boolean isFixed)
    {
        this.isFixed = isFixed;
    }
    
    public List<DictionaryEntity> getDics()
    {
        return dics;
    }
    
    public void setDics(List<DictionaryEntity> dics)
    {
        this.dics = dics;
    }
    
    @Override
    public String toString()
    {
        return "DictionaryTypeBase [dictTypeCode=" + dictTypeCode + ", dictTypeName=" + dictTypeName + ", isFixed="
            + isFixed + ", remarks=" + remarks + "]";
    }
}
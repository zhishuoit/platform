package com.zis.platform.core.service;

import java.util.List;

import com.zis.platform.common.service.BaseService;
import com.zis.platform.core.entity.AreaEntity;

/**
 * 
 * <b>说明：</b>行政区划 业务接口
 * 
 * @ClassName: AreaService
 * @author zhaohaitao(2543)
 * @date 2015-7-10 上午10:30:04
 * 
 */
public interface AreaService extends BaseService<AreaEntity>
{
    /**
     * 
     * 根据 行政编码查询区域信息
     * 
     * @Title: findByAreaCode
     * @param @param areaCode
     * @return AreaEntity 返回类型
     * @throws
     */
    public AreaEntity findByAreaCode(String areaCode);
    
    /**
     * 根据行政编码查询下属区域信息
     * 
     * @Title: findByParentAreaCode
     * @param @param parentAreaCode
     * @param @return 设定文件
     * @return List<AreaEntity> 返回类型
     * @throws
     */
    public List<AreaEntity> findByParentAreaCode(String parentAreaCode);
    
}

package com.zis.platform.core.service;

import com.zis.platform.common.service.BaseService;
import com.zis.platform.core.entity.LogLoginEntity;

/**
 * 
 * @ClassName: LogLoginService
 * @Description: 登录日志 业务接口，采集日志
 * @author zhaohaitao(2543)
 * @date 2015-7-2 上午9:37:33
 * 
 */

public interface LogLoginService extends BaseService<LogLoginEntity>
{
    /**
     * 从日志文件中采集日志并持久化到数据库
     * 
     * @param type 日志类型:login(登录日志) null(操作日志)
     * @param startDate 日志起始时间
     * @param endDate 日志结束时间
     */
    public void parseLogFromFile(String type, String startDate, String endDate);
    
    /**
     * 调用dao层持久化方法，进行数据持久化操作
     * 
     * @param log
     */
    public void save(LogLoginEntity log);
}

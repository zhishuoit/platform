package com.zis.platform.core.service;

import java.util.List;

import com.zis.platform.common.service.BaseService;
import com.zis.platform.core.entity.ResEntity;
import com.zis.platform.core.entity.SysEntity;

public interface ResService extends BaseService<ResEntity>
{
    public int deleteByResIds(String[] resIds);
    
    public int saveOrUpdate(ResEntity res);
    
    public List<SysEntity> findAllSysAndRes();
    
    public List<ResEntity> findByParentId(String parentId);
    
    public List<ResEntity> findBySysId(String sysId);
    
    public ResEntity findByResId(String resId);
    
    public int update(ResEntity[] resources);
}

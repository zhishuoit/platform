package com.zis.platform.core.entity.base;

import java.io.Serializable;

public class ResGroupBase implements Serializable
{
    
    private String rgId;
    
    private String resId;
    
    private String groupId;
    
    public String getRgId()
    {
        return rgId;
    }
    
    public void setRgId(String rgId)
    {
        this.rgId = rgId;
    }
    
    public String getResId()
    {
        return resId;
    }
    
    public void setResId(String resId)
    {
        this.resId = resId;
    }
    
    public String getGroupId()
    {
        return groupId;
    }
    
    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }
    
    @Override
    public String toString()
    {
        return "ResGroupBase [rgId=" + rgId + ", resId=" + resId + ", groupId=" + groupId + "]";
    }
    
}

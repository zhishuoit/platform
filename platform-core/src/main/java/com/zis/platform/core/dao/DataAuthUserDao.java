package com.zis.platform.core.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.zis.common.SearchParams;
import com.zis.platform.common.dao.BaseDao;
import com.zis.platform.core.entity.DataAuthUserEntity;

@Repository
public interface DataAuthUserDao extends BaseDao<DataAuthUserEntity, String>
{
    public List<DataAuthUserEntity> findAll();
    
    public List<DataAuthUserEntity> findByCondition(SearchParams params);
    
    public List<DataAuthUserEntity> findByUserId(String userId);
    
    public int deleteByUserDataAuthIds(String[] userDataAuthIds);
    
    public int deleteUserDataAuth(String userId, String tableName, String authCode);
    
    public int isExist(DataAuthUserEntity record);
}
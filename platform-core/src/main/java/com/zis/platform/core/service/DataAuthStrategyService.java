package com.zis.platform.core.service;

import com.zis.platform.common.service.BaseService;
import com.zis.platform.core.entity.DataAuthStrategyEntity;

public interface DataAuthStrategyService extends BaseService<DataAuthStrategyEntity>
{
    public boolean saveOrUpdate(DataAuthStrategyEntity entity);
    
    public int deleteByStrategyIds(String[] strategyIds);
}

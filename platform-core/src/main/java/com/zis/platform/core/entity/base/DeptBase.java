package com.zis.platform.core.entity.base;

public class DeptBase
{
    private String deptId;
    
    private String orgId;
    
    private String deptName;
    
    private String phone;
    
    private String deptManager;
    
    private String remarks;
    
    public String getDeptId()
    {
        return deptId;
    }
    
    public void setDeptId(String deptId)
    {
        this.deptId = deptId == null ? null : deptId.trim();
    }
    
    public String getOrgId()
    {
        return orgId;
    }
    
    public void setOrgId(String orgId)
    {
        this.orgId = orgId == null ? null : orgId.trim();
    }
    
    public String getDeptName()
    {
        return deptName;
    }
    
    public void setDeptName(String deptName)
    {
        this.deptName = deptName == null ? null : deptName.trim();
    }
    
    public String getPhone()
    {
        return phone;
    }
    
    public void setPhone(String phone)
    {
        this.phone = phone == null ? null : phone.trim();
    }
    
    public String getDeptManager()
    {
        return deptManager;
    }
    
    public void setDeptManager(String deptManager)
    {
        this.deptManager = deptManager == null ? null : deptManager.trim();
    }
    
    public String getRemarks()
    {
        return remarks;
    }
    
    public void setRemarks(String remarks)
    {
        this.remarks = remarks == null ? null : remarks.trim();
    }
    
    @Override
    public String toString()
    {
        return "DeptBase [deptId=" + deptId + ", orgId=" + orgId + ", deptName=" + deptName + ", phone=" + phone
            + ", deptManager=" + deptManager + ", remarks=" + remarks + "]";
    }
}
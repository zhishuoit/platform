package com.zis.platform.core.service.impl;

import java.io.File;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.zis.common.SearchParams;
import com.zis.platform.common.service.impl.BaseServiceImpl;
import com.zis.platform.core.dao.ExcelTemplateDao;
import com.zis.platform.core.entity.ExcelTemplateEntity;
import com.zis.platform.core.service.ExcelTemplateService;

@Service
public class ExcelTemplateServiceImpl extends BaseServiceImpl<ExcelTemplateEntity> implements ExcelTemplateService
{
    @Autowired
    private ExcelTemplateDao dao;
    
    @Override
    public List<ExcelTemplateEntity> findByCondition(SearchParams params)
        throws Exception
    {
        return dao.findByCondition(params);
    }
    
    @Override
    public List<ExcelTemplateEntity> findAll()
        throws Exception
    {
        return dao.findAll();
    }
    
    @Override
    public ExcelTemplateEntity saveOrUpdate(ExcelTemplateEntity entity)
    {
        if (entity != null)
        {
            if (StringUtils.isBlank(entity.getEtId()))
            {
                entity.setEtId(UUID.randomUUID().toString());
                int insertNum = dao.insertSelective(entity);
                if (insertNum > 0)
                {
                    return entity;
                }
            }
            else
            {
                int updateNum = dao.updateByPrimaryKeySelective(entity);
                if (updateNum > 0)
                {
                    return entity;
                }
            }
        }
        return null;
    }
    
    @Override
    public ExcelTemplateEntity getById(String etId)
    {
        return dao.selectByPrimaryKey(etId);
    }
    
    @Override
    public int deleteByEtIds(String[] etIds)
    {
        return dao.deleteByEtIds(etIds);
    }
    
    @Override
    public ExcelTemplateEntity getByCode(String code)
    {
        return dao.selectByCode(code);
    }
    
    @Override
    public File getExceltemplateFileByCode(String code)
    {
        HttpServletRequest request =
            ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
        ExcelTemplateEntity entity = this.getByCode(code);
        String realPath = request.getSession().getServletContext().getRealPath("/");
        File file = new File(realPath + entity.getEtPath());
        return file;
    }
    
    @Override
    public List<ExcelTemplateEntity> findByEtName(String etName)
    {
        return this.dao.findByEtName(etName);
    }
    
    @Override
    public List<ExcelTemplateEntity> findByEtCode(String etCode)
    {
        return this.dao.findByEtCode(etCode);
    }
    
}

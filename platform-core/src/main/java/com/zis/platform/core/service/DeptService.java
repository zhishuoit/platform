package com.zis.platform.core.service;

import com.zis.platform.common.service.BaseService;
import com.zis.platform.core.entity.DeptEntity;

/**
 * 
 * <b>说明：</b>机构业务接口
 * 
 * @ClassName: DeptService
 * @author zhaohaitao(2543)
 * @date 2015-7-10 上午10:32:34
 * 
 */
public interface DeptService extends BaseService<DeptEntity>
{
    /**
     * 新增或者修改
     * 
     * @Title: saveOrUpdate
     * @param @param dept
     * @param @return 设定文件
     * @return DeptEntity 返回类型
     * @throws
     */
    DeptEntity saveOrUpdate(DeptEntity dept);
    
    /**
     * 根据主键批量删除
     * 
     * @Title: deleteByDeptIds
     * @param @param deptIds
     * @param @return 设定文件
     * @return int 返回类型
     * @throws
     */
    int deleteByDeptIds(String[] deptIds);
    
}

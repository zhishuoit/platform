package com.zis.platform.core.entity;

import java.io.Serializable;

import com.zis.platform.core.entity.base.AreaBase;

public class AreaEntity extends AreaBase implements Serializable
{
    
    private boolean isParent;
    
    public boolean isIsParent()
    {
        return isParent;
    }
    
    public void setIsParent(boolean isParent)
    {
        this.isParent = isParent;
    }
    
}

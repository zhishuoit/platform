package com.zis.platform.core.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.zis.common.SearchParams;
import com.zis.platform.common.service.impl.BaseServiceImpl;
import com.zis.platform.core.dao.DataAuthUserDao;
import com.zis.platform.core.entity.DataAuthUserEntity;
import com.zis.platform.core.service.DataAuthUserService;

@Service
public class DataAuthUserServiceImpl extends BaseServiceImpl<DataAuthUserEntity> implements DataAuthUserService
{
    @Autowired
    private DataAuthUserDao dao;
    
    @Override
    public List<DataAuthUserEntity> findByCondition(SearchParams params)
        throws Exception
    {
        return dao.findByCondition(params);
    }
    
    @Override
    public List<DataAuthUserEntity> findAll()
        throws Exception
    {
        return dao.findAll();
    }
    
    @Override
    public boolean saveOrUpdate(DataAuthUserEntity record)
    {
        int num = 0;
        if (StringUtils.isEmpty(record.getUserDataAuthId()))
        {
            // 检查是否已存在
            if (dao.isExist(record) > 0)
            {
                return false;
            }
            record.setUserDataAuthId(UUID.randomUUID().toString());
            num = dao.insert(record);
        }
        else
        {
            num = dao.updateByPrimaryKeySelective(record);
        }
        if (num > 0)
        {
            return true;
        }
        return false;
    }
    
    @Override
    public Map<String, List<DataAuthUserEntity>> findByUserId(String userId)
    {
        List<DataAuthUserEntity> userDataAuths = dao.findByUserId(userId);
        Map<String, List<DataAuthUserEntity>> dataMaps = new HashMap<String, List<DataAuthUserEntity>>();
        for (DataAuthUserEntity dataAuth : userDataAuths)
        {
            String tableName = dataAuth.getTableName();
            List<DataAuthUserEntity> list = dataMaps.get(tableName);
            if (CollectionUtils.isEmpty(list))
            {
                list = new ArrayList<DataAuthUserEntity>();
            }
            list.add(dataAuth);
            dataMaps.put(tableName.toUpperCase(), list);
        }
        return dataMaps;
    }
    
    @Override
    public int deleteByUserDataAuthIds(String[] userDataAuthIds)
    {
        return dao.deleteByUserDataAuthIds(userDataAuthIds);
    }
    
    @Override
    public boolean saveUserDataAuth(String userId, String userName, String tableName, String authCode)
    {
        DataAuthUserEntity dau = new DataAuthUserEntity();
        dau.setUserId(userId);
        dau.setUserName(userName);
        dau.setTableName(tableName);
        dau.setAuthorityScope(authCode);
        return saveOrUpdate(dau);
    }
    
    /*
     * (非 Javadoc)
     * 
     * @Title: DataAuthUserServiceImpl.java
     * 
     * @param userId
     * 
     * @param tableName
     * 
     * @param authCode
     * 
     * @return
     * 
     * @see com.skynet.platform.core.service.DataAuthUserService#deleteUserDataAuth(java.lang.String, java.lang.String,
     * java.lang.String)
     */
    
    @Override
    public boolean deleteUserDataAuth(String userId, String tableName, String authCode)
    {
        dao.deleteUserDataAuth(userId, tableName, authCode);
        return false;
    }
}

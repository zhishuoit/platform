package com.zis.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileUtil
{
    
    /** 允许上传的图片类型 **/
    public static final String IMG_TYPE = ".jpg|.gif|.png|.bmp";
    
    /** 允许上传的所有文件类型 **/
    public static final String ALL_TYPE = ".jpg|.jepg|.gif|.png|.bmp|.gz|.rar|.zip|.pdf|.txt|.swf|.mp3|.jar|.apk|.ipa";
    
    /**
     * 
     * 
     * <b>说明：</b>文件复制
     * 
     * @param srcPath 文件源
     * @param destPath 目标文件
     * @return boolean 返回类型
     */
    public static boolean copy(String srcPath, String destPath)
    {
        boolean result = false;
        try
        {
            InputStream is = new FileInputStream(srcPath);
            OutputStream os = new FileOutputStream(destPath);
            byte buffer[] = new byte[8192];
            for (int len = 0; (len = is.read(buffer)) != -1;)
            {
                os.write(buffer, 0, len);
                os.flush();
            }
            
            os.close();
            is.close();
            result = true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * 检测文件大小
     * 
     * @param file 文件
     * @param kb 限制大小
     * @return true 超过限制
     */
    public static boolean checkFileSize(File file, int kb)
    {
        long size = file.length();
        if (size > 1024 * kb)
        {
            return true;
        }
        return false;
    }
    
    /**
     * 检查文件类型
     * 
     * @param 文件名
     * @param isImg 是否检查图片
     * @return true=后缀合法
     * @throws
     */
    public static boolean checkFileType(String fileName, boolean isImg)
    {
        String fileType = getFileType(fileName);
        if (isImg)
        {
            return IMG_TYPE.indexOf(fileType.toLowerCase()) != -1;
        }
        else
        {
            return ALL_TYPE.indexOf(fileType.toLowerCase()) != -1;
        }
    }
    
    /**
     * 获取文件类型
     * 
     * @param fileName
     * @throws
     */
    public static String getFileType(String fileName)
    {
        return fileName.substring(fileName.lastIndexOf("."), fileName.length());
    }
    
    /**
     * 读取文件返回Byte数组
     * 
     * @param file
     * @return
     */
    public static byte[] readFileAsByteArray(File file)
    {
        BufferedInputStream in = null;
        byte[] content = null;
        ByteArrayOutputStream out = null;
        try
        {
            in = new BufferedInputStream(new FileInputStream(file));
            out = new ByteArrayOutputStream(1024);
            byte[] temp = new byte[1024];
            int size = 0;
            while ((size = in.read(temp)) != -1)
            {
                out.write(temp, 0, size);
            }
            content = out.toByteArray();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                out.close();
                in.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return content;
    }
    
    public static void main(String[] args)
    {
        String s = getFileType("12321.jpg");
        System.out.println(s);
    }
}

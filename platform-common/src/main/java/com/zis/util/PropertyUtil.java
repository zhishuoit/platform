package com.zis.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;

/**
 * 配置 加载工具
 * @author zhaohaitao
 *
 */
public class PropertyUtil implements BeanFactoryAware
{
    
    public List<String> getList(String prefix)
    {
        if (properties == null || prefix == null)
        {
            return Collections.emptyList();
        }
        List<String> list = new ArrayList<String>();
        Enumeration<?> en = properties.propertyNames();
        String key;
        while (en.hasMoreElements())
        {
            key = (String)en.nextElement();
            if (key.startsWith(prefix))
            {
                list.add(properties.getProperty(key));
            }
        }
        return list;
    }
    
    
    public String getString(String prefix)
    {
        if (properties == null || prefix == null)
        {
            return "";
        }
        Enumeration<?> en = properties.propertyNames();
        String key;
        while (en.hasMoreElements())
        {
            key = (String)en.nextElement();
            if (key.equals(prefix))
            {
                return properties.getProperty(key);
            }
        }
        return "";
    }
    
    
    private BeanFactory beanFactory;
    
    private Properties properties;
    
    public Properties getProperties()
    {
        return properties;
    }
    
    public void setProperties(Properties properties)
    {
        this.properties = properties;
    }
    
    public BeanFactory getBeanFactory()
    {
        return beanFactory;
    }
    
    public void setBeanFactory(BeanFactory beanFactory)
    {
        this.beanFactory = beanFactory;
    }
    
}

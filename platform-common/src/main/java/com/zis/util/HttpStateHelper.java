package com.zis.util;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

/**
 * HTTP请求状态封装类 TODO 根据开发需求进行增减
 * 
 * @author 赵海涛
 * 
 */
public class HttpStateHelper
{
    /**
     * 404 : 页面未找到
     * 
     * @param response
     * @return
     */
    public static String pageNotFound(HttpServletResponse response)
    {
        try
        {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return "temp_not_found";
    }
    
    /**
     * 403 : 没有访问权限
     * 
     * @param response
     * @return
     */
    public static String noAuthority(HttpServletResponse response)
    {
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        return "403";
    }
}

package com.zis.util;

import java.util.Random;

public class Salt
{
    /**
     * 生成随机Salt 长度8
     * 
     * @return
     */
    public static String genRandomL8()
    {
        return genRandomNum(8);
    }
    
    /**
     * 生成随机Salt 长度16
     * 
     * @return
     */
    public static String genRandomL16()
    {
        return genRandomNum(16);
    }
    
    /**
     * 生成随机Salt
     * 
     * @param len 生成的Salt的总长度
     * @return Salt的字符串
     */
    public static String genRandomNum(int len)
    {
        // 35是因为数组是从0开始的，26个字母+10个数字
        final int maxNum = 36;
        int i; // 生成的随机数
        int count = 0; // 生成的密码的长度
        char[] str =
            {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
                'v', 'w', 'x', 'y', 'z'};
        
        StringBuffer pwd = new StringBuffer("");
        Random r = new Random();
        while (count < len)
        {
            // 生成随机数，取绝对值，防止生成负数，
            
            i = Math.abs(r.nextInt(maxNum)); // 生成的数最大为36-1
            
            if (i >= 0 && i < str.length)
            {
                pwd.append(str[i]);
                count++;
            }
        }
        
        return pwd.toString();
    }
}

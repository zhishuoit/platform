package com.zis.util;

/**
 * 字符串处理工具
 * 
 * @author windy
 * 
 */
public class StringUtil
{
    
    /**
     * 判断字符串是否为空
     * 
     * @param str
     * @return str == null || str.trim().length() == 0 时为 true
     */
    public static boolean isEmpty(String str)
    {
        if (str == null || str.trim().length() == 0)
        {
            return true;
        }
        return false;
    }
    
    /**
     * 判断字符串是否不为空
     * 
     * @param str
     * @return !(str == null || str.trim().length() == 0) 时为 true
     */
    public static boolean isNotEmpty(String str)
    {
        return !isEmpty(str);
    }
}

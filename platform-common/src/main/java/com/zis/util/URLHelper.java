package com.zis.util;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.util.UrlPathHelper;

/**
 * URI帮助类
 * 
 * @author 赵海涛
 * 
 */
public class URLHelper
{
    
    /**
     * 模板文件存放目录
     */
    public static final String TEMPLATE_FOLDEER = "page";
    
    public static final String WEB_INF = "WEB-INF";
    
    /**
     * 模板页面类型后缀
     */
    public static final String WEB_SUFFIX = ".html";
    
    public static final Integer PAGE_SIZE = 10;
    
    /**
     * 平台模板 文件存放目录
     */
    public static final String TEMPLATE_FOLDEER_PLATFORM = "page";
    
    /**
     * WEB网站模板 文件存放目录
     */
    public static final String TEMPLATE_FOLDEER_FRONT = "template";
    
    private static Logger log = Logger.getLogger(URLHelper.class);
    
    /**
     * 获得页号
     * 
     * @param request
     * @return
     */
    public static int getPageNo(HttpServletRequest request)
    {
        return getPageNo(getURI(request));
    }
    
    /**
     * 获得路径信息
     * 
     * @param request
     * @return
     */
    public static String[] getPaths(HttpServletRequest request)
    {
        return getPaths(getURI(request));
    }
    
    /**
     * 获得路径参数
     * 
     * @param request
     * @return
     */
    public static String[] getParams(HttpServletRequest request)
    {
        return getParams(getURI(request));
    }
    
    public static String getURI(HttpServletRequest request)
    {
        UrlPathHelper helper = new UrlPathHelper();
        String uri = helper.getOriginatingRequestUri(request);
        String ctx = helper.getOriginatingContextPath(request);
        if (!StringUtils.isBlank(ctx))
        {
            return uri.substring(ctx.length());
        }
        else
        {
            return uri;
        }
    }
    
    /**
     * 获取模板真实路径 首先判断模板文件是否存在 ： 存在则返回路径，不存在则直接返回null
     * 
     * @param paths
     * @return
     */
    public static String getRealTemplatePath(String[] paths)
    {
        // String u = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        // String str = u.toString();
        // int num = str.indexOf(Constants.WEB_INF);
        // str = str.substring(0, num + Constants.WEB_INF.length());
        StringBuffer sb = new StringBuffer();
        if (paths != null && paths.length > 0)
        {
            // sb.append("/").append(Constants.TEMPLATE_FOLDEER_PLATFORM);
            for (int i = 1; i < paths.length; i++)
            {
                sb.append("/").append(paths[i]);
            }
        }
        else
        {
            return null;
        }
        // File f = new File(str + sb.toString() + Constants.WEB_SUFFIX);
        // // TODO 抛出异常,提示找不到模板页面
        // if (!f.exists())
        // {
        // return null;
        // }
        return sb.toString();
    }
    
    public static String getFrontRealTemplatePath(String[] paths)
    {
        String u = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String str = u.toString();
        int num = str.indexOf(WEB_INF);
        str = str.substring(0, num + WEB_INF.length());
        StringBuffer sb = new StringBuffer();
        if (paths != null && paths.length > 0)
        {
            sb.append("/").append(TEMPLATE_FOLDEER_FRONT);
            for (int i = 0; i < paths.length; i++)
            {
                sb.append("/").append(paths[i]);
            }
        }
        else
        {
            return null;
        }
        File f = new File(str + sb.toString() + WEB_SUFFIX);
        // TODO 抛出异常,提示找不到模板页面
        if (!f.exists())
        {
            return null;
        }
        return sb.toString();
    }
    
    /**
     * 获得翻页信息
     * 
     * @param request
     * @return
     */
    public static PageInfo getPageInfo(HttpServletRequest request)
    {
        UrlPathHelper helper = new UrlPathHelper();
        String uri = helper.getOriginatingRequestUri(request);
        String queryString = helper.getOriginatingQueryString(request);
        return getPageInfo(uri, queryString);
    }
    
    /**
     * 获得页号
     * 
     * @param uri URI {@link HttpServletRequest#getRequestURI()}
     * @return
     */
    public static int getPageNo(String uri)
    {
        if (uri == null)
        {
            throw new IllegalArgumentException("URI can not be null");
        }
        if (!uri.startsWith("/"))
        {
            throw new IllegalArgumentException("URI must start width '/'");
        }
        int pageNo = 1;
        int bi = uri.indexOf("_");
        int mi = uri.indexOf("-");
        int pi = uri.indexOf(".");
        if (bi != -1)
        {
            String pageNoStr;
            if (mi != -1)
            {
                pageNoStr = uri.substring(bi + 1, mi);
            }
            else
            {
                if (pi != -1)
                {
                    pageNoStr = uri.substring(bi + 1, pi);
                }
                else
                {
                    pageNoStr = uri.substring(bi + 1);
                }
            }
            try
            {
                pageNo = Integer.parseInt(pageNoStr);
            }
            catch (Exception e)
            {
            }
        }
        return pageNo;
    }
    
    /**
     * 获得路径数组
     * 
     * @param uri URI {@link HttpServletRequest#getRequestURI()}
     * @return
     */
    public static String[] getPaths(String uri)
    {
        if (uri == null)
        {
            throw new IllegalArgumentException("URI can not be null");
        }
        if (!uri.startsWith("/"))
        {
            throw new IllegalArgumentException("URI must start width '/'");
        }
        int bi = uri.indexOf("_");
        int mi = uri.indexOf("-");
        int pi = uri.indexOf(".");
        // 获得路径信息
        String pathStr;
        if (bi != -1)
        {
            pathStr = uri.substring(0, bi);
        }
        else if (mi != -1)
        {
            pathStr = uri.substring(0, mi);
        }
        else if (pi != -1)
        {
            pathStr = uri.substring(0, pi);
        }
        else
        {
            pathStr = uri;
        }
        String[] paths = StringUtils.split(pathStr, '/');
        return paths;
    }
    
    /**
     * 获得路径参数
     * 
     * @param uri URI {@link HttpServletRequest#getRequestURI()}
     * @return
     */
    public static String[] getParams(String uri)
    {
        if (uri == null)
        {
            throw new IllegalArgumentException("URI can not be null");
        }
        if (!uri.startsWith("/"))
        {
            throw new IllegalArgumentException("URI must start width '/'");
        }
        int mi = uri.indexOf("-");
        int pi = uri.indexOf(".");
        String[] params;
        if (mi != -1)
        {
            String paramStr;
            if (pi != -1)
            {
                paramStr = uri.substring(mi, pi);
            }
            else
            {
                paramStr = uri.substring(mi);
            }
            params = new String[StringUtils.countMatches(paramStr, "-")];
            int fromIndex = 1;
            int nextIndex = 0;
            int i = 0;
            while ((nextIndex = paramStr.indexOf("-", fromIndex)) != -1)
            {
                params[i++] = paramStr.substring(fromIndex, nextIndex);
                fromIndex = nextIndex + 1;
            }
            params[i++] = paramStr.substring(fromIndex);
        }
        else
        {
            params = new String[0];
        }
        return params;
    }
    
    /**
     * 获得URL信息
     * 
     * @param uri URI {@link HttpServletRequest#getRequestURI()}
     * @param queryString 查询字符串 {@link HttpServletRequest#getQueryString()}
     * @return
     */
    public static PageInfo getPageInfo(String uri, String queryString)
    {
        if (uri == null)
        {
            return null;
        }
        if (!uri.startsWith("/"))
        {
            throw new IllegalArgumentException("URI must start width '/'");
        }
        int bi = uri.indexOf("_");
        int mi = uri.indexOf("-");
        int pi = uri.indexOf(".");
        int lastSpt = uri.lastIndexOf("/") + 1;
        String url;
        if (!StringUtils.isBlank(queryString))
        {
            url = uri + "?" + queryString;
        }
        else
        {
            url = uri;
        }
        // 翻页前半部
        String urlFormer;
        if (bi != -1)
        {
            urlFormer = uri.substring(lastSpt, bi);
        }
        else if (mi != -1)
        {
            urlFormer = uri.substring(lastSpt, mi);
        }
        else if (pi != -1)
        {
            urlFormer = uri.substring(lastSpt, pi);
        }
        else
        {
            urlFormer = uri.substring(lastSpt);
        }
        // 翻页后半部
        String urlLater;
        if (mi != -1)
        {
            urlLater = url.substring(mi);
        }
        else if (pi != -1)
        {
            urlLater = url.substring(pi);
        }
        else
        {
            urlLater = url.substring(uri.length());
        }
        String href = url.substring(lastSpt);
        return new PageInfo(href, urlFormer, urlLater);
    }
    
    /**
     * 获取当前 请求 的业务应用系统路径
     * 
     * @param contextPath
     * @param requestURI
     * @return
     */
    public static String getSysContentPath(HttpServletRequest req)
    {
        String subSequence = null;
        // 得到应用路径
        String contextPath = req.getContextPath();
        // 得到请求路径
        String requestURI = req.getRequestURI();
        try
        {
            subSequence =
                requestURI.substring(contextPath.length() + 1, requestURI.indexOf("/", contextPath.length() + 1));
        }
        catch (Exception e)
        {
            log.error("请求路径：" + requestURI + "根据请求路径无法找到应用系统目录，请检查你的访问路径是否正确。");
        }
        return subSequence;
    }
    
    /**
     * URI信息
     */
    public static class PageInfo
    {
        /**
         * 页面地址
         */
        private String href;
        
        /**
         * href前半部（相对于分页）
         */
        private String hrefFormer;
        
        /**
         * href后半部（相对于分页）
         */
        private String hrefLatter;
        
        public PageInfo(String href, String hrefFormer, String hrefLatter)
        {
            this.href = href;
            this.hrefFormer = hrefFormer;
            this.hrefLatter = hrefLatter;
        }
        
        public String getHref()
        {
            return href;
        }
        
        public void setHref(String href)
        {
            this.href = href;
        }
        
        public String getHrefFormer()
        {
            return hrefFormer;
        }
        
        public void setHrefFormer(String hrefFormer)
        {
            this.hrefFormer = hrefFormer;
        }
        
        public String getHrefLatter()
        {
            return hrefLatter;
        }
        
        public void setHrefLatter(String hrefLatter)
        {
            this.hrefLatter = hrefLatter;
        }
        
    }
}

package com.zis.util;

import java.io.File;
import java.util.ArrayList;

public class WalkFile
{
    
    private static ArrayList filelist = new ArrayList();
    
    public static void main(String[] args)
    {
        refreshFileList("E:\\workspace\\zis_easyui\\platform\\platform-web\\src\\main\\webapp\\plug-in\\icons\\tabicons");
    }
    
    public static void refreshFileList(String strPath)
    {
        File dir = new File(strPath);
        File[] files = dir.listFiles();
        if (files == null)
            return;
        for (int i = 0; i < files.length; i++)
        {
            if (files[i].isDirectory())
            {
                refreshFileList(files[i].getAbsolutePath());
            }
            else
            {
                String strFileName = files[i].getAbsolutePath().toLowerCase();
                String iconPath = strFileName.substring(79, strFileName.length());
                String iconName = strFileName.substring(88, strFileName.length()-4);
                System.out.println(".icon-"+iconName+" {");
                System.out.println("    background: url('"+iconPath+"') no-repeat;");
                System.out.println("    width: 18px;");
                System.out.println("    line-height: 18px;");
                System.out.println("    display: inline-block;");
                System.out.println("}");
                System.out.println("");
                filelist.add(files[i].getAbsolutePath());
                
            }
            
        }
    }
}

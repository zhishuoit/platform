package com.zis.util;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.constructs.blocking.BlockingCache;

/**
 * 
 * <b>说明：</b>ehcache 缓存工具类 cacheName在ehcache.xml中配置
 * 
 * @ClassName: CacheUtil
 * @author zhaohaitao(2543) 日期： 2015-7-24 上午11:37:44
 * 
 */
public class CacheUtil
{
    /**
     * 系统缓存块 名称
     */
    private static final String SYS_CACHE_MANAGER_NAME = "zis.sysConfigCache";
    
    /**
     * 获得系统缓存实例
     */
    private static CacheManager MANAGER = CacheManager.getInstance();
    
    /**
     * 获取缓存对性
     * 
     * @param key
     * @return Object 返回类型
     */
    public static Object get(Object key)
    {
        Cache cache = MANAGER.getCache(SYS_CACHE_MANAGER_NAME);
        if (cache != null)
        {
            Element element = cache.get(key);
            if (element != null)
            {
                return element.getObjectValue();
            }
        }
        return null;
    }
    
    /**
     * 将数据放到缓存
     * 
     * @param key
     * @return void 返回类型
     */
    public static void put(Object key, Object value)
    {
        Cache cache = MANAGER.getCache(SYS_CACHE_MANAGER_NAME);
        if (cache != null)
        {
            cache.put(new Element(key, value));
        }
    }
    
    /**
     * 从缓存中移除数据
     * 
     * @param key
     * @return boolean 返回类型
     */
    public static boolean remove(Object key)
    {
        Cache cache = MANAGER.getCache(SYS_CACHE_MANAGER_NAME);
        if (cache != null)
        {
            return cache.remove(key);
        }
        return false;
    }
    
    /**
     * 获取缓存对象数量
     * 
     * @param 设定文件
     * @return long 返回类型
     */
    public static long getObjectSize()
    {
        BlockingCache cache = new BlockingCache(MANAGER.getEhcache(SYS_CACHE_MANAGER_NAME));
        if (cache != null)
        {
            return cache.getSize();
        }
        return 0L;
    }
    
    /**
     * 获取缓存大小
     * 
     * @return long 返回类型
     */
    public static long getCacheMemorySize()
    {
        BlockingCache cache = new BlockingCache(MANAGER.getEhcache(SYS_CACHE_MANAGER_NAME));
        if (cache != null)
        {
            return cache.getMemoryStoreSize();
        }
        return 0L;
    }
}
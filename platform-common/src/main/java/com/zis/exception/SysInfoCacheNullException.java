package com.zis.exception;

/**
 * 
 * <b>说明：</b>系统缓存为空异常
 * 
 * @ClassName: SysInfoCacheNullException
 * @author zhaohaitao(2543) 日期： 2015-7-24 上午11:36:50
 * 
 */
public class SysInfoCacheNullException extends Exception
{
    
    /**
     * @Fields serialVersionUID : 序列化号
     */
    private static final long serialVersionUID = 6067124251662259396L;
    
    public SysInfoCacheNullException()
    {
        super("系统信息缓存为空，请重新启动服务，或者联系管理员。");
    }
    
    public SysInfoCacheNullException(String message, Throwable cause)
    {
        super(message, cause);
    }
    
    public SysInfoCacheNullException(String message)
    {
        super(message);
    }
    
    public SysInfoCacheNullException(Throwable cause)
    {
        super(cause);
    }
    
}

package com.zis.exception;

/**
 * 用户被禁用异常
 * 
 * @author zhaohaitao
 * 
 */
public class UserDisableException extends Exception
{
    
    public UserDisableException()
    {
        super("用户被禁用，请联系管理员。");
    }
    
    public UserDisableException(String message, Throwable cause)
    {
        super(message, cause);
    }
    
    public UserDisableException(Throwable cause)
    {
        super(cause);
    }
    
}

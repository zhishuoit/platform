package com.zis;

/**
 * 
 * <b>说明：</b>在异步通讯中的状态定义类（后期扩展使用）
 * 
 * @ClassName: JSONState
 * @author zhaohaitao(2543) 日期： 2015-7-24 上午11:12:34
 * 
 */
public final class JSONState
{
    
    private JSONState()
    {
    }
    
    /**
     * 失败
     */
    public static final String ERROR = "error";
    
    /**
     * 成功
     */
    public static final String OK = "ok";
    
}

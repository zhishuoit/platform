package com.zis.common;

import java.util.Map;

/**
 * 
 * <b>说明：</b>适应 easyui的分页查询实体
 * 
 * @ClassName: SearchParams
 * @author zhaohaitao(2543) 日期： 2015-7-24 上午11:12:58
 * 
 */
public class SearchParams
{
	/**
	 * 排序字段
	 */
	private String sort;
	
    /**
     * 排序方式
     */
    private String order;
    
    /**
     * 每页显示数量
     */
    private Integer rows = 10;
    
    /**
     * 偏移量
     */
    private Integer page = 0;
    
    /**
     * 查询关键字
     */
    private Map<String, Object> searchParams;
    
    public SearchParams()
    {
    }
    
    public String getSort() {
    		return sort;
    }
    
    public void setSort(String sort) {
    		this.sort = sort;
    }
    
    public String getOrder()
    {
        return order;
    }
    
    public void setOrder(String order)
    {
        this.order = order;
    }
    
    public Map<String, Object> getSearchParams()
    {
        return searchParams;
    }
    
    public void setSearchParams(Map<String, Object> searchParams)
    {
        this.searchParams = searchParams;
    }
    
    public Integer getRows()
    {
        return rows;
    }
    
    public void setRows(Integer rows)
    {
        this.rows = rows;
    }
    
    public Integer getPage()
    {
        return page;
    }
    
    public void setPage(Integer page)
    {
        this.page = page;
    }
    
}

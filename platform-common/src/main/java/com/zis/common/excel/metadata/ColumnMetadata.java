/**   
 * 
 * @Title: ColumnMetadata.java
 * @Package com.skynet.common.excel.metadata
 * @author zhaohaitao(2543)
 * @date 2015-7-14 上午10:08:35
 */

package com.zis.common.excel.metadata;

/**
 * 
 * <b>说明：</b> Excel导出：列数据
 * 
 * @ClassName: ColumnMetadata
 * @author zhaohaitao(2543) 日期： 2015-7-24 上午11:27:11
 * 
 */
public class ColumnMetadata
{
    /**
     * 数据列
     */
    private String dbColumn;
    
    /**
     * 对应的excel列
     */
    private String excelColumn;
    
    /**
     * 数据类型
     */
    private String dataType;
    
    public ColumnMetadata()
    {
    }
    
    public String getDbColumn()
    {
        return dbColumn;
    }
    
    public void setDbColumn(String dbColumn)
    {
        this.dbColumn = dbColumn;
    }
    
    public String getExcelColumn()
    {
        return excelColumn;
    }
    
    public void setExcelColumn(String excelColumn)
    {
        this.excelColumn = excelColumn;
    }
    
    public String getDataType()
    {
        return dataType;
    }
    
    public void setDataType(String dataType)
    {
        this.dataType = dataType;
    }
    
}

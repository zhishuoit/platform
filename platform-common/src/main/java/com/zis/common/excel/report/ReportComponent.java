package com.zis.common.excel.report;

import java.util.List;

import com.zis.common.excel.db.DBSheet;
import com.zis.common.excel.io.ExcelWriter;
import com.zis.common.excel.metadata.ReportMetadata;

/**
 * 
 * <b>说明：</b> 报告组件
 * 
 * @ClassName: ReportComponent
 * @author zhaohaitao(2543) 日期： 2015-7-24 上午11:31:48
 * 
 */
public abstract class ReportComponent
{
    
    public ReportComponent()
    {
    }
    
    public boolean creatReport(ReportMetadata reportMetadata, String templatePath, String savePath)
    {
        ExcelWriter writer = new ExcelWriter();
        writer.setTemplatePath(templatePath);
        writer.setSavePath(savePath);
        return writer.write(initDBSheet(), reportMetadata);
    }
    
    protected abstract List<DBSheet> initDBSheet();
}

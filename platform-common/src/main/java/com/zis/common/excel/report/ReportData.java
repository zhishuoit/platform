package com.zis.common.excel.report;

import java.util.ArrayList;
import java.util.List;

import com.zis.common.excel.db.DBRecord;
import com.zis.common.excel.db.DBRecordSet;
import com.zis.common.excel.db.DBSheet;

/**
 * 
 * <b>说明：</b> 报告数据
 * 
 * @ClassName: ReportData
 * @author zhaohaitao(2543) 日期： 2015-7-24 上午11:32:43
 * 
 */
public class ReportData extends ReportComponent
{
    
    /*
     * (非 Javadoc)
     * 
     * @Title: ReportData.java
     * 
     * @return
     * 
     * @see com.skynet.common.excel.report.ReportComponent#initDBSheet()
     */
    
    public ReportData()
    {
    }
    
    protected List<DBSheet> initDBSheet()
    {
        List<DBSheet> list = new ArrayList<DBSheet>();
        DBSheet dbSheet = new DBSheet();
        dbSheet.setDisplayName("\u57FA\u672C\u4FE1\u606F");
        dbSheet.setSheetName("Sheet1");
        DBRecordSet recordSet = new DBRecordSet();
        DBRecord record = new DBRecord();
        record.put("id", Integer.valueOf(1));
        record.put("name", "\u5F20\u4E09");
        record.put("age", Integer.valueOf(20));
        recordSet.add(record);
        DBRecord record1 = new DBRecord();
        record1.put("id", Integer.valueOf(2));
        record1.put("name", "\u674E\u56DB");
        record1.put("age", Integer.valueOf(22));
        recordSet.add(record1);
        dbSheet.setRecordSet(recordSet);
        list.add(dbSheet);
        return list;
    }
    
}

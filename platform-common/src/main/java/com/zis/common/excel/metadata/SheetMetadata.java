/**   
 * 
 * @Title: SheetMetadata.java
 * @Package com.skynet.common.excel.metadata
 * @author zhaohaitao(2543)
 * @date 2015-7-14 上午10:09:18
 */

package com.zis.common.excel.metadata;

import java.util.Map;

/**
 * 
 * <b>说明：</b> sheet数据
 * 
 * @ClassName: SheetMetadata
 * @author zhaohaitao(2543) 日期： 2015-7-24 上午11:29:49
 * 
 */
public class SheetMetadata
{
    /**
     * sheet名称
     */
    private String sheetName;
    
    /**
     * 方向
     */
    private String direction;
    
    /**
     * 分隔线
     */
    private String partitionLine;
    
    /**
     * 列的元数据
     */
    private Map columnMetadatas;
    
    /**
     * X轴
     */
    private String columnX;
    
    /**
     * Y轴
     */
    private String columnY;
    
    /**
     * 数据列
     */
    private String dataColumn;
    
    /**
     * 起始下标
     */
    private String startIndex;
    
    public SheetMetadata()
    {
    }
    
    public String getSheetName()
    {
        return sheetName;
    }
    
    public void setSheetName(String sheetName)
    {
        this.sheetName = sheetName;
    }
    
    public String getDirection()
    {
        return direction;
    }
    
    public void setDirection(String direction)
    {
        this.direction = direction;
    }
    
    public String getPartitionLine()
    {
        return partitionLine;
    }
    
    public void setPartitionLine(String partitionLine)
    {
        this.partitionLine = partitionLine;
    }
    
    public Map getColumnMetadatas()
    {
        return columnMetadatas;
    }
    
    public void setColumnMetadatas(Map columnMetadatas)
    {
        this.columnMetadatas = columnMetadatas;
    }
    
    public String getColumnX()
    {
        return columnX;
    }
    
    public void setColumnX(String columnX)
    {
        this.columnX = columnX;
    }
    
    public String getColumnY()
    {
        return columnY;
    }
    
    public void setColumnY(String columnY)
    {
        this.columnY = columnY;
    }
    
    public String getDataColumn()
    {
        return dataColumn;
    }
    
    public void setDataColumn(String dataColumn)
    {
        this.dataColumn = dataColumn;
    }
    
    public String getStartIndex()
    {
        return startIndex;
    }
    
    public void setStartIndex(String startIndex)
    {
        this.startIndex = startIndex;
    }
    
}

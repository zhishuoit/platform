package com.zis.common.excel.constant;

/**
 * 
 * <b>说明：</b>excel导出：表格数据类型定义
 * 
 * @ClassName: ExcelDataType
 * @author zhaohaitao(2543) 日期： 2015-7-24 上午11:14:19
 * 
 */
public final class ExcelDataType
{
    /**
     * int 类型
     */
    public static final String INT = "int";
    
    /**
     * long 类型
     */
    public static final String LONG = "long";
    
    /**
     * double 类型
     */
    public static final String DOUBLE = "double";
    
    /**
     * string 类型
     */
    public static final String STRING = "String";
}

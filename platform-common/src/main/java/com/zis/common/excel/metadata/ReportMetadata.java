package com.zis.common.excel.metadata;

import java.util.Map;

/**
 * 
 * <b>说明：</b> excel 导出：报表数据
 * 
 * @ClassName: ReportMetadata
 * @author zhaohaitao(2543) 日期： 2015-7-24 上午11:28:23
 * 
 */
public class ReportMetadata
{
    /**
     * 报告id
     */
    private String reportId;
    
    /**
     * 报表名称
     */
    private String reportName;
    
    /**
     * 模板文件名称
     */
    private String templateFileName;
    
    /**
     * 类型名称
     */
    private String reportClsName;
    
    /**
     * sheet数据集
     */
    private Map sheetMetadatas;
    
    public ReportMetadata()
    {
    }
    
    public String getReportId()
    {
        return reportId;
    }
    
    public void setReportId(String reportId)
    {
        this.reportId = reportId;
    }
    
    public String getReportName()
    {
        return reportName;
    }
    
    public void setReportName(String reportName)
    {
        this.reportName = reportName;
    }
    
    public String getTemplateFileName()
    {
        return templateFileName;
    }
    
    public void setTemplateFileName(String templateFileName)
    {
        this.templateFileName = templateFileName;
    }
    
    public String getReportClsName()
    {
        return reportClsName;
    }
    
    public void setReportClsName(String reportClsName)
    {
        this.reportClsName = reportClsName;
    }
    
    public Map getSheetMetadatas()
    {
        return sheetMetadatas;
    }
    
    public void setSheetMetadatas(Map sheetMetadatas)
    {
        this.sheetMetadatas = sheetMetadatas;
    }
    
}

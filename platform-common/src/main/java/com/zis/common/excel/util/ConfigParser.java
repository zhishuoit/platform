package com.zis.common.excel.util;

import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.zis.common.excel.metadata.ColumnMetadata;
import com.zis.common.excel.metadata.ReportMetadata;
import com.zis.common.excel.metadata.SheetMetadata;

/**
 * 
 * <b>说明：</b> 配置解析
 * 
 * @ClassName: ConfigParser
 * @author zhaohaitao(2543) 日期： 2015-7-24 上午11:33:14
 * 
 */
public class ConfigParser
{
    
    public ConfigParser()
    {
    }
    
    /**
     * 通过输入流解析数据到map
     * 
     * @param input
     * @return Map 返回类型
     */
    public static Map parse(InputStream input)
    {
        Map reportMetadataMap = null;
        try
        {
            SAXReader reader = new SAXReader();
            Document doc = reader.read(input);
            Element root = doc.getRootElement();
            List reports = root.elements("report");
            ReportMetadata reportMetadata;
            for (Iterator iterator = reports.iterator(); iterator.hasNext(); reportMetadataMap.put(reportMetadata.getReportId(),
                reportMetadata))
            {
                Element report = (Element)iterator.next();
                reportMetadata = new ReportMetadata();
                reportMetadata.setReportId(report.attributeValue("id"));
                reportMetadata.setReportName(report.attributeValue("reportName"));
                reportMetadata.setTemplateFileName(report.attributeValue("templateFileName"));
                reportMetadata.setReportClsName(report.attributeValue("reportClsName"));
                reportMetadata.setSheetMetadatas(parseReportElement(report));
                if (reportMetadataMap == null)
                {
                    reportMetadataMap = new LinkedHashMap();
                }
            }
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return reportMetadataMap;
    }
    
    /**
     * 解析报告元素
     * 
     * @param reportElement
     * @return Map 返回类型
     */
    private static Map parseReportElement(Element reportElement)
    {
        Map sheetMetadataMap = null;
        try
        {
            List sheets = reportElement.elements("sheet");
            SheetMetadata sheetMetadata;
            for (Iterator iterator = sheets.iterator(); iterator.hasNext(); sheetMetadataMap.put(sheetMetadata.getSheetName(),
                sheetMetadata))
            {
                Element sheet = (Element)iterator.next();
                sheetMetadata = new SheetMetadata();
                sheetMetadata.setSheetName(sheet.attributeValue("name"));
                sheetMetadata.setDirection(sheet.attributeValue("direction"));
                sheetMetadata.setPartitionLine(sheet.attributeValue("partitionLine"));
                sheetMetadata.setColumnX(sheet.attributeValue("columX"));
                sheetMetadata.setColumnY(sheet.attributeValue("columnY"));
                sheetMetadata.setDataColumn(sheet.attributeValue("dataColumn"));
                sheetMetadata.setStartIndex(sheet.attributeValue("startIndex"));
                sheetMetadata.setColumnMetadatas(parseSheetElement(sheet));
                if (sheetMetadataMap == null)
                {
                    sheetMetadataMap = new LinkedHashMap();
                }
            }
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return sheetMetadataMap;
    }
    
    /**
     * 解析sheet元素
     * 
     * @param sheetElement
     * @return Map 返回类型
     */
    private static Map parseSheetElement(Element sheetElement)
    {
        Map columnMetadataMap = null;
        try
        {
            List sheets = sheetElement.elements("column");
            ColumnMetadata columnMetadata;
            for (Iterator iterator = sheets.iterator(); iterator.hasNext(); columnMetadataMap.put(columnMetadata.getDbColumn(),
                columnMetadata))
            {
                Element sheet = (Element)iterator.next();
                columnMetadata = new ColumnMetadata();
                columnMetadata.setDbColumn(sheet.attributeValue("dbColumn"));
                columnMetadata.setExcelColumn(sheet.attributeValue("excelColumn"));
                columnMetadata.setDataType(sheet.attributeValue("dataType"));
                if (columnMetadataMap == null)
                {
                    columnMetadataMap = new LinkedHashMap();
                }
            }
            
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return columnMetadataMap;
    }
}

package com.zis.common.excel.constant;

/**
 * 
 * <b>说明：</b> excel数据导出的常量定义
 * 
 * @ClassName: ReportConst
 * @author zhaohaitao(2543) 日期： 2015-7-24 上午11:15:43
 * 
 */
public final class ReportConst
{
    /**
     * report
     */
    public static final String REPORT = "report";
    
    /**
     * id
     */
    public static final String REPORTID = "id";
    
    /**
     * reportName
     */
    public static final String REPORTNAME = "reportName";
    
    /**
     * templateFileName
     */
    public static final String TEMPLATEFILENAME = "templateFileName";
    
    /**
     * reportClsName
     */
    public static final String REPORTCLSNAME = "reportClsName";
    
    /**
     * sheet
     */
    public static final String SHEET = "sheet";
    
    /**
     * name
     */
    public static final String SHEETNAME = "name";
    
    /**
     * direction
     */
    public static final String DIRECTION = "direction";
    
    /**
     * partitionLine
     */
    public static final String PARTITIONLINE = "partitionLine";
    
    /**
     * dataColumn
     */
    public static final String DATACOLUMN = "dataColumn";
    
    /**
     * startIndex
     */
    public static final String STARTINDEX = "startIndex";
    
    /**
     * columX
     */
    public static final String COLUMX = "columX";
    
    /**
     * columnY
     */
    public static final String COLUMNY = "columnY";
    
    /**
     * column
     */
    public static final String COLUMN = "column";
    
    /**
     * dbColumn
     */
    public static final String DBCOLUMN = "dbColumn";
    
    /**
     * excelColumn
     */
    public static final String EXCELCOLUMN = "excelColumn";
    
    /**
     * dataType
     */
    public static final String DATATYPE = "dataType";
}

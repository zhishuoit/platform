package com.zis.common.excel.db;

import java.util.ArrayList;

/**
 * 
 * <b>说明：</b> excel导出：多记录集合
 * 
 * @ClassName: DBRecordSet
 * @author zhaohaitao(2543) 日期： 2015-7-24 上午11:19:27
 * 
 */
public class DBRecordSet extends ArrayList<Object>
{
    /**
     * @Fields serialVersionUID : 序列化号
     */
    private static final long serialVersionUID = -1440083721789226704L;
    
    public DBRecordSet()
    {
    }
    
    public DBRecord getRow(int row)
    {
        return (DBRecord)get(row);
    }
}

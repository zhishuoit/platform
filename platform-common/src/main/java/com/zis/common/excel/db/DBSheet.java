package com.zis.common.excel.db;

/**
 * 
 * <b>说明：</b> excel导出：sheet
 * 
 * @ClassName: DBSheet
 * @author zhaohaitao(2543) 日期： 2015-7-24 上午11:20:22
 * 
 */
public class DBSheet
{
    
    /**
     * sheet名称
     */
    private String sheetName;
    
    /**
     * 显示名称
     */
    private String displayName;
    
    /**
     * 数据集合
     */
    private DBRecordSet recordSet;
    
    public DBSheet()
    {
    }
    
    public String getSheetName()
    {
        return sheetName;
    }
    
    public void setSheetName(String sheetName)
    {
        this.sheetName = sheetName;
    }
    
    public String getDisplayName()
    {
        return displayName;
    }
    
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }
    
    public DBRecordSet getRecordSet()
    {
        return recordSet;
    }
    
    public void setRecordSet(DBRecordSet recordSet)
    {
        this.recordSet = recordSet;
    }
    
}

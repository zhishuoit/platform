package com.zis.common.excel.db;

import java.util.LinkedHashMap;

/**
 * 
 * <b>说明：</b> excel导出：一行记录
 * 
 * @ClassName: DBRecord
 * @author zhaohaitao(2543) 日期： 2015-7-24 上午11:17:48
 * 
 */
public class DBRecord extends LinkedHashMap<Object, Object>
{
    
    /**
     * @Fields serialVersionUID : 序列化号
     */
    private static final long serialVersionUID = 1318007971738207749L;
    
    public DBRecord()
    {
    }
    
    public String getString(String key)
    {
        String value = null;
        Object obj = get(key);
        if (obj != null)
            value = obj.toString();
        return value;
    }
}

package com.zis.common;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;

/**
 * 
 * <b>说明：</b>适配 easyui的datatable的分页实体
 * 
 * @ClassName: Pagination
 * @author zhaohaitao(2543) 日期： 2015-7-24 上午11:12:46
 * 
 * @param <T>
 */
public class Pagination<T> implements Serializable
{
    
    /**
     * @Fields serialVersionUID : TODO
     */
    
    private static final long serialVersionUID = 7546101146443833860L;
    
    /**
     * 查询结果总数
     */
    private long total;
    
    /**
     * 结果集
     */
    private List<T> rows;
    
    /**
     * 页脚表格数据
     */
    private List<Map<String, Object>> footer;
    
    /**
     * 无参构造
     * 
     * @Title: Pagination.java
     */
    public Pagination()
    {
        super();
    }
    
    /**
     * 使用page对象进行构造
     * 
     * @Title: Pagination.java
     * @param page
     */
    public Pagination(Page<T> page)
    {
        total = page.getTotal();
        this.rows = page;
    }
    
    public long getTotal()
    {
        return total;
    }
    
    public void setTotal(long total)
    {
        this.total = total;
    }
    
    public List<T> getRows()
    {
        return rows;
    }
    
    public void setRows(List<T> rows)
    {
        this.rows = rows;
    }
    
    public List<Map<String, Object>> getFooter()
    {
        return footer;
    }
    
    public void setFooter(List<Map<String, Object>> footer)
    {
        this.footer = footer;
    }
}

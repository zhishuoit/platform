// lazyload config

angular.module('app')
    /**
   * jQuery plugin config use ui-jq directive , config the js and css files that required
   * key: function name of the jQuery plugin
   * value: array of the css js file located
   */
  .constant('JQ_CONFIG', {
      easyPieChart:   ['/plug-in/angular/vendor/jquery/charts/easypiechart/jquery.easy-pie-chart.js'],
      sparkline:      ['/plug-in/angular/vendor/jquery/charts/sparkline/jquery.sparkline.min.js'],
      plot:           ['/plug-in/angular/vendor/jquery/charts/flot/jquery.flot.min.js', 
                          '/plug-in/angular/vendor/jquery/charts/flot/jquery.flot.resize.js',
                          '/plug-in/angular/vendor/jquery/charts/flot/jquery.flot.tooltip.min.js',
                          '/plug-in/angular/vendor/jquery/charts/flot/jquery.flot.spline.js',
                          '/plug-in/angular/vendor/jquery/charts/flot/jquery.flot.orderBars.js',
                          '/plug-in/angular/vendor/jquery/charts/flot/jquery.flot.pie.min.js'],
      slimScroll:     ['/plug-in/angular/vendor/jquery/slimscroll/jquery.slimscroll.min.js'],
      sortable:       ['/plug-in/angular/vendor/jquery/sortable/jquery.sortable.js'],
      nestable:       ['/plug-in/angular/vendor/jquery/nestable/jquery.nestable.js',
                          '/plug-in/angular/vendor/jquery/nestable/nestable.css'],
      filestyle:      ['/plug-in/angular/vendor/jquery/file/bootstrap-filestyle.min.js'],
      slider:         ['/plug-in/angular/vendor/jquery/slider/bootstrap-slider.js',
                          '/plug-in/angular/vendor/jquery/slider/slider.css'],
      chosen:         ['/plug-in/angular/vendor/jquery/chosen/chosen.jquery.min.js',
                          '/plug-in/angular/vendor/jquery/chosen/chosen.css'],
      TouchSpin:      ['/plug-in/angular/vendor/jquery/spinner/jquery.bootstrap-touchspin.min.js',
                          '/plug-in/angular/vendor/jquery/spinner/jquery.bootstrap-touchspin.css'],
      wysiwyg:        ['/plug-in/angular/vendor/jquery/wysiwyg/bootstrap-wysiwyg.js',
                          '/plug-in/angular/vendor/jquery/wysiwyg/jquery.hotkeys.js'],
      dataTable:      ['/plug-in/angular/vendor/jquery/datatables/jquery.dataTables.min.js',
                          '/plug-in/angular/vendor/jquery/datatables/dataTables.bootstrap.js',
                          '/plug-in/angular/vendor/jquery/datatables/dataTables.bootstrap.css'],
      vectorMap:      ['/plug-in/angular/vendor/jquery/jvectormap/jquery-jvectormap.min.js', 
                          '/plug-in/angular/vendor/jquery/jvectormap/jquery-jvectormap-world-mill-en.js',
                          '/plug-in/angular/vendor/jquery/jvectormap/jquery-jvectormap-us-aea-en.js',
                          '/plug-in/angular/vendor/jquery/jvectormap/jquery-jvectormap.css'],
      footable:       ['/plug-in/angular/vendor/jquery/footable/footable.all.min.js',
                          '/plug-in/angular/vendor/jquery/footable/footable.core.css']
      }
  )
  // oclazyload config
  .config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
      // We configure ocLazyLoad to use the lib script.js as the async loader
      $ocLazyLoadProvider.config({
          debug:  false,
          events: true,
          modules: [
              {
                  name: 'ngGrid',
                  files: [
                      '/plug-in/angular/vendor/modules/ng-grid/ng-grid.min.js',
                      '/plug-in/angular/vendor/modules/ng-grid/ng-grid.min.css',
                      '/plug-in/angular/vendor/modules/ng-grid/theme.css'
                  ]
              },
              {
                  name: 'ui.select',
                  files: [
                      '/plug-in/angular/vendor/modules/angular-ui-select/select.min.js',
                      '/plug-in/angular/vendor/modules/angular-ui-select/select.min.css'
                  ]
              },
              {
                  name:'angularFileUpload',
                  files: [
                    '/plug-in/angular/vendor/modules/angular-file-upload/angular-file-upload.min.js'
                  ]
              },
              {
                  name:'ui.calendar',
                  files: ['/plug-in/angular/vendor/modules/angular-ui-calendar/calendar.js']
              },
              {
                  name: 'ngImgCrop',
                  files: [
                      '/plug-in/angular/vendor/modules/ngImgCrop/ng-img-crop.js',
                      '/plug-in/angular/vendor/modules/ngImgCrop/ng-img-crop.css'
                  ]
              },
              {
                  name: 'angularBootstrapNavTree',
                  files: [
                      '/plug-in/angular/vendor/modules/angular-bootstrap-nav-tree/abn_tree_directive.js',
                      '/plug-in/angular/vendor/modules/angular-bootstrap-nav-tree/abn_tree.css'
                  ]
              },
              {
                  name: 'toaster',
                  files: [
                      '/plug-in/angular/vendor/modules/angularjs-toaster/toaster.js',
                      '/plug-in/angular/vendor/modules/angularjs-toaster/toaster.css'
                  ]
              },
              {
                  name: 'textAngular',
                  files: [
                      '/plug-in/angular/vendor/modules/textAngular/textAngular-sanitize.min.js',
                      '/plug-in/angular/vendor/modules/textAngular/textAngular.min.js'
                  ]
              },
              {
                  name: 'vr.directives.slider',
                  files: [
                      '/plug-in/angular/vendor/modules/angular-slider/angular-slider.min.js',
                      '/plug-in/angular/vendor/modules/angular-slider/angular-slider.css'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular',
                  files: [
                      '/plug-in/angular/vendor/modules/videogular/videogular.min.js'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular.plugins.controls',
                  files: [
                      '/plug-in/angular/vendor/modules/videogular/plugins/controls.min.js'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular.plugins.buffering',
                  files: [
                      '/plug-in/angular/vendor/modules/videogular/plugins/buffering.min.js'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular.plugins.overlayplay',
                  files: [
                      '/plug-in/angular/vendor/modules/videogular/plugins/overlay-play.min.js'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular.plugins.poster',
                  files: [
                      '/plug-in/angular/vendor/modules/videogular/plugins/poster.min.js'
                  ]
              },
              {
                  name: 'com.2fdevs.videogular.plugins.imaads',
                  files: [
                      '/plug-in/angular/vendor/modules/videogular/plugins/ima-ads.min.js'
                  ]
              }
          ]
      });
  }])
;
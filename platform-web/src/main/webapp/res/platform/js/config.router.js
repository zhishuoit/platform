'use strict';

/**
 * Config for the router
 */
angular.module('app')
.run(
		[ '$rootScope', '$state', '$stateParams',
			function($rootScope, $state, $stateParams) {
				$rootScope.$state = $state;
				$rootScope.$stateParams = $stateParams;
				
				$rootScope.$on('$stateChangeSuccess', function(event, to, toParams, from, fromParams) {
		            $rootScope.previousState = from;
		            $rootScope.previousStateParams = fromParams;
		        });
			} 
		])
.config(
		[ '$stateProvider', '$urlRouterProvider',
			function($stateProvider, $urlRouterProvider) {
				$urlRouterProvider.otherwise('/app/dashboard');
				$stateProvider.state('app', {
					abstract : true,
					url : '/app',
					templateUrl : 'app.do',
				})
				.state('app.dashboard', {
					url : '/dashboard',
					templateUrl : 'dashboard.do',
				})
				.state('app.news', {
					abstract: true,
					url: '/news',
					template: '<div ui-view class="fade-in"></div>',
					resolve: {
					    deps: ['$ocLazyLoad',
					           function( $ocLazyLoad ){
					  	  			return $ocLazyLoad.load('/res/platform/js/controllers/grid.js');
					    		}
					    	  ]
					}
	            })
	            .state('app.news.list', {
	                url: '/list?page&search',
	                templateUrl: '/platform/user/list.do',
	            })
			} 
		]);